from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

_SET_NAMES = [
    "basic",
    "classic",
    "boomsday_project",
    # NOTE: Required for (at least) `minion_annoy-o-tron`.
    "goblins_vs_gnomes",
    "journey_to_ungoro",
    "knights_of_the_frozen_throne",
    "kobolds_and_catacombs",
    # NOTE: Required for (at least) `hero_power_the_silver_hand`.
    "the_grand_tournament",
    "witchwood",
]


def _load_card_ids(set_names):
    card_ids_set = set()
    for set_name in set_names:
        with open(os.path.join("generated/spellsource/cards", set_name)) as f:
            for l in f:
                card_ids_set.add(l.strip())

    card_ids_dict = {}
    for i, card_id in enumerate(sorted(card_ids_set)):
        card_ids_dict[card_id] = i

    return card_ids_dict


STANDARD_CARD_IDS = _load_card_ids(_SET_NAMES)
N_STANDARD_CARD_IDS = len(STANDARD_CARD_IDS)
