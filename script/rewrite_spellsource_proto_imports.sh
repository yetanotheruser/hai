#!/usr/bin/env bash
set -eou pipefail
IFS=$'\n\t'

perl \
    -p \
    -i.bak \
    -e 's/^import "(?!google)/import "spellsource\/proto\//' \
    spellsource/proto/*.proto
