#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from absl import app as absl_app
from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string("output_dir", None, "Absolute path to output directory.")
flags.DEFINE_string("spellsource_server_root",
                    None,
                    "Absolute path to root of Spellsource Server git repo.")

BUILD_FILE_TEMPLATE = """
filegroup(
    name = "cards",
    srcs = [{}],
    visibility = ["//visibility:public"],
)
"""
RELATIVE_HEARTSTONE_CARDS_DIR = "cards/src/main/resources/cards/hearthstone"


def main(argv):
    del argv

    assert os.path.isabs(FLAGS.output_dir), "Must specify ABSOLUTE path."
    assert os.path.isabs(FLAGS.spellsource_server_root), "Must specify ABSOLUTE path."

    hearthstone_cards_dir = os.path.join(FLAGS.spellsource_server_root,
                                         RELATIVE_HEARTSTONE_CARDS_DIR)

    set_names = set()
    for set_name in os.listdir(hearthstone_cards_dir):
        set_path = os.path.join(hearthstone_cards_dir, set_name)

        if os.path.isdir(set_path):
            set_names.add(set_name)
            # NOTE: We must always allow for an empty card ID (to correspond with no entity).
            set_card_ids = set([""])

            for root, dirs, files in os.walk(set_path):
                for f in files:
                    if f.endswith(".json"):
                        card_id = f[:-len(".json")]
                        set_card_ids.add(card_id)

            with open(os.path.join(FLAGS.output_dir, set_name), "w") as f:
                f.write("\n".join(sorted(list(set_card_ids))))

    with open(os.path.join(FLAGS.output_dir, "BUILD"), "w") as f:
        build_file_contents = BUILD_FILE_TEMPLATE.format(
            ", ".join(["\"{}\"".format(n) for n in sorted(set_names)])
        )
        f.write(build_file_contents)


if __name__ == "__main__":
    absl_app.run(main)
