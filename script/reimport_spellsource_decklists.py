#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import shutil

from absl import app as absl_app
from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string("output_dir",
                    None,
                    "Absolute path to `generated/decklists`.")
flags.DEFINE_string("spellsource_server_root",
                    None,
                    "Absolute path to root of Spellsource Server git repo.")

BUILD_FILE_TEMPLATE = """
filegroup(
    name = "decklists",
    srcs = [{}],
    visibility = ["//visibility:public"],
)
"""
RELATIVE_SPELLSOURCE_DECKLISTS_DIR = "net/src/main/resources/decklists/current"


def _sanitize_deck_name(deck_name):
    return deck_name.replace(" ", "_")


def main(argv):
    del argv

    assert os.path.isabs(FLAGS.spellsource_server_root), "Must specify ABSOLUTE path."

    decklists_dir = os.path.join(FLAGS.spellsource_server_root,
                                 RELATIVE_SPELLSOURCE_DECKLISTS_DIR)


    deck_names = set()
    for deck_name in os.listdir(decklists_dir):
        deck_path = os.path.join(decklists_dir, deck_name)
        if os.path.isfile(deck_path):
            deck_names.add(deck_name)

    try:
        shutil.rmtree(FLAGS.output_dir)
    except OSError: pass
    os.makedirs(FLAGS.output_dir)

    for deck_name in deck_names:
        deck_path = os.path.join(decklists_dir, deck_name)
        shutil.copy(deck_path,
                    os.path.join(FLAGS.output_dir, _sanitize_deck_name(deck_name)))

    with open(os.path.join(FLAGS.output_dir, "BUILD"), "w") as f:
        build_file_contents = BUILD_FILE_TEMPLATE.format(
            ", ".join(["\"{}\"".format(_sanitize_deck_name(n)) for n in sorted(deck_names)])
        )
        f.write(build_file_contents)


if __name__ == "__main__":
    absl_app.run(main)
