#!/usr/bin/env python
from absl import flags
from absl import logging
import tensorflow as tf

from ai.model.complex.complex import ComplexModel

FLAGS = flags.FLAGS
flags.DEFINE_string("dest_dir", "/tmp/log_dir", "Directory to dump graph into.")


def dump(dest_dir):
    with tf.Session() as sess:
        ComplexModel()
    train_writer = tf.summary.FileWriter(dest_dir)
    logging.info("Writing graph to {}.".format(dest_dir))
    train_writer.add_graph(sess.graph)


if __name__ == "__main__":
    dump(FLAGS.dest_dir)
