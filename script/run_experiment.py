from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import re
import os
import shutil
import subprocess
import tempfile
import uuid

from absl import app as absl_app
from absl import flags
from absl import logging
import absl
import jinja2
import paramiko
import scp

import base.buildinfo.buildinfo as buildinfo

FLAGS = flags.FLAGS
flags.DEFINE_integer("port", 31445, "Port to connect to SSH on on the remote host.")
flags.DEFINE_string("docker_registry", "registry.local", "Docker registry to use.")
flags.DEFINE_string("remote_base_dir", "/srv/nfs/experiments", "Base experiment storage directory.")
flags.DEFINE_string("host", "192.168.0.14", "Host to run experiment on.")
flags.DEFINE_string("train_args", "", "Arguments to supply training script.")

BAZEL_RULES_DOCKER_PUSH_DIGEST_REGEX = re.compile(r"^.* digest: (sha256:[\w]*)$")
EXPERIMENT_FOLDER_NAME_TPL = "{date}-{experiment_name}"


def parse_params(
        args,  # type: List[str]
):
    # type: (...) -> Tuple[Tuple[str], Tuple[str, str]]
    """
    Parse a series of flags into sorted tuples of boolean flags and (key, value) flags.
    """
    bool_flags = []
    kv_flags = []
    for arg in args:
        stripped_arg = arg.lstrip("-")
        if "=" in stripped_arg:
            k, v = stripped_arg.split("=", 1)
            kv_flags.append((k, v))
        else:
            bool_flags.append(stripped_arg)
    return tuple(bool_flags), tuple(kv_flags)


def main(argv):
    del argv

    if buildinfo.STABLE_BUILD_SCM_STATUS != "Clean":
        raise Exception("Cowardlily refusing to start an uncommitted experiment.")

    # Set up experiment metadata.
    experiment_uuid = uuid.uuid4()  # A random UUID.
    now = datetime.datetime.now()
    date_str = "{year}-{month}-{day}".format(year=now.year, month=now.month, day=now.day)
    experiment_name = raw_input("Experiment name: ")
    experiment_full_name = "{}-{}".format(experiment_name, experiment_uuid)
    experiment_dir = os.path.join(FLAGS.remote_base_dir, date_str, experiment_full_name)
    logging.debug("Creating experiment '{}'.".format(experiment_full_name))

    hai_git_sha = buildinfo.STABLE_BUILD_SCM_REVISION
    push_output = subprocess.check_output(["./ai/push_main_image"])
    match = False
    for l in push_output.split("\n"):
        match = BAZEL_RULES_DOCKER_PUSH_DIGEST_REGEX.match(l)
        if match:
            break
    assert match
    hai_digest = match.group(1)
    hai_image = "{}/hai@{}".format(FLAGS.docker_registry, hai_digest)
    logging.info("Built and pushed HAI image with digest '{}' for git sha '{}'.".format(
        hai_digest, hai_git_sha))
    logging.debug("Build and push output: \n" + push_output)

    spellsource_git_sha = "a2b4f191342f8ec2bc5a0d6f7f3fd74c3635b70b"
    spellsource_digest = "sha256:8c32403d2ec0dac5651c40ea896808fef710584563e33beae6f37611a2b3d876"
    spellsource_image = "{}/spellsource@{}".format(FLAGS.docker_registry, spellsource_digest)
    logging.debug("Using Spellsource image with digest '{}' for git sha '{}'.".format(
        hai_digest, hai_git_sha))

    with paramiko.SSHClient() as ssh_client:

        if FLAGS.host == "localhost":
            def exec_command(ssh_client, cmd):
                subprocess.check_call(cmd, shell=True)

            def put_dir(ssh_client, src, dst):
                subprocess.check_call(["cp", "-rT", src, dst])
        else:
            ssh_client.load_system_host_keys()
            ssh_client.connect(hostname=FLAGS.host, port=FLAGS.port)
            def exec_command(ssh_client, cmd):
                ssh_client.exec_command(cmd)

            def put_dir(ssh_client, src, dst):
                with scp.SCPClient(ssh_client.get_transport()) as scp_client:
                    scp_client.put(
                        os.path.join(src, ""),  # Append the extra `/` to avoid copying the tmpdir.
                        remote_path=dst,
                        recursive=True)

        exec_command(ssh_client, "mkdir -p {}".format(experiment_dir))

        tmux_session = "{}-{}-{}".format(date_str, experiment_name, str(experiment_uuid)[:6])
        template_args = {
            "docker_registry": FLAGS.docker_registry,
            "experiment_dir": experiment_dir,
            "experiment_name": experiment_name,
            "experiment_uuid": experiment_uuid,
            "hai_digest": hai_digest,
            "hai_git_sha": hai_git_sha,
            "hai_image": hai_image,
            "spellsource_digest": spellsource_digest,
            "spellsource_git_sha": spellsource_git_sha,
            "spellsource_image": spellsource_image,
            "tmux_session": tmux_session,
            "train_args": FLAGS.train_args,
            "train_params": parse_params(FLAGS.train_args.split()),
        }
        d = tempfile.mkdtemp()
        try:
            # Set up experiment locally.
            templates_dir = "infrastructure/experiment/"
            for maybe_template_filename in os.listdir(templates_dir):
                maybe_template_file = os.path.join(templates_dir, maybe_template_filename)

                if os.path.isfile(maybe_template_file):
                    template_file = maybe_template_file
                    template_filename = maybe_template_filename
                    dst_file = os.path.join(d, template_filename.rstrip(".tpl"))

                    with open(template_file) as f:
                        template_str = f.read()
                    with open(dst_file, "w") as f:
                        f.write(jinja2.Template(template_str).render(**template_args))

                    os.chmod(f.name, 0o755)

            # Set up remote experiment.
            put_dir(ssh_client, d, experiment_dir)

        finally:
            shutil.rmtree(d)
        logging.debug("Set up remote experiment folder at '{}'.".format(experiment_dir))

        # Start remote experiment.
        remote_run_experiment_cmd = os.path.join(experiment_dir, "run_experiment.sh")
        exec_command(ssh_client, remote_run_experiment_cmd)
        logging.info("Started experiment in tmux session '{}'.".format(tmux_session))


if __name__ == "__main__":
    absl_app.run(main)
