from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import google.protobuf.text_format as text_format
import pytest

import ai.deserialization.action
import spellsource.proto.action_pb2 as action_pb2


class TestInflateActions(object):
    def test_should_inflate(self):
        in_ = action_pb2.Action()
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 0\n"
            "}\n", in_)

        expected_out = [action_pb2.Action()]
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 0\n"
            "    target_key_to_actions {\n"
            "        target: -1\n"
            "    }\n"
            "}\n", expected_out[0])

        out = ai.deserialization.action.inflate_actions([in_])
        assert out == expected_out


class TestDeflateAction(object):
    def test_should_deflate(self):
        in_ = action_pb2.Action()
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 0\n"
            "    target_key_to_actions {\n"
            "        target: -1\n"
            "    }\n"
            "}\n", in_)

        expected_out = action_pb2.Action()
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 0\n"
            "}\n", expected_out)

        out = ai.deserialization.action.deflate_action(in_)
        assert out == expected_out

    def test_shouldnt_deflate(self):
        in_ = action_pb2.Action()
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 2\n"
            "    target_key_to_actions {\n"
            "        target: 3\n"
            "    }\n"
            "}\n", in_)

        expected_out = action_pb2.Action()
        text_format.Parse(
            "spell {\n"
            "    source_id: 1\n"
            "    action: 2\n"
            "    target_key_to_actions {\n"
            "        target: 3\n"
            "    }\n"
            "}\n", expected_out)

        out = ai.deserialization.action.deflate_action(in_)
        assert out == expected_out


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
