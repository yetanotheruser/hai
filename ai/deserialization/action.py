from __future__ import absolute_import
from __future__ import print_function

from absl import logging
import numpy as np

from ai.simulator_parameters import (
    ACTION_TYPE_BATTLECRY,
    ACTION_TYPE_DISCOVER,
    ACTION_TYPE_END_TURN,
    ACTION_TYPE_HERO,
    ACTION_TYPE_HERO_POWER,
    ACTION_TYPE_MULLIGAN,
    ACTION_TYPE_PHYSICAL_ATTACK,
    ACTION_TYPE_SPELL,
    ACTION_TYPE_SUMMON,
    ACTION_TYPE_WEAPON,
)
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.target_action_pair_pb2 as target_action_pair_pb2

_SPELL_ACTION_TYPES = {
    ACTION_TYPE_BATTLECRY,
    ACTION_TYPE_HERO,
    ACTION_TYPE_HERO_POWER,
    ACTION_TYPE_SPELL,
}

####################################################################################################
# Inflate action proto into several singular action protos.
####################################################################################################

def _inflate_physical_attack(action_pb):
    physical_attack_action_pb = getattr(action_pb, action_pb.WhichOneof("action_oneof"))
    inflated_actions = []
    for defender in physical_attack_action_pb.defenders:
        inflated_action = action_pb2.Action()
        inflated_physical_attack = getattr(inflated_action, action_pb.WhichOneof("action_oneof"))

        inflated_physical_attack.source_id = physical_attack_action_pb.source_id
        inflated_physical_attack.defenders.extend([defender])

        inflated_actions.append(inflated_action)
    return inflated_actions


def _inflate_spell(action_pb):
    spell_action_pb = getattr(action_pb, action_pb.WhichOneof("action_oneof"))
    # If the spell has no target we insert a "nontarget" with entity id `-1`
    if spell_action_pb.HasField("action"):
        ret = action_pb2.Action()
        ret.CopyFrom(action_pb)
        ret_spell_action_pb = getattr(ret, action_pb.WhichOneof("action_oneof"))
        ret_spell_action_pb.target_key_to_actions.extend([target_action_pair_pb2.TargetActionPair(
            target=-1,
        )])
        return [ret]

    inflated_actions = []
    for target_key_to_action in spell_action_pb.target_key_to_actions:
        inflated_action = action_pb2.Action()
        inflated_spell = getattr(inflated_action, action_pb.WhichOneof("action_oneof"))

        inflated_spell.source_id = spell_action_pb.source_id
        inflated_spell.action = spell_action_pb.source_id
        inflated_spell.target_key_to_actions.extend([target_key_to_action])

        inflated_actions.append(inflated_action)
    return inflated_actions


def _inflate_summon(action_pb):
    summon_action_pb = getattr(action_pb, action_pb.WhichOneof("action_oneof"))
    inflated_actions = []

    if len(summon_action_pb.index_to_actions) < 1:
        raise Exception("Expected every summon to have an index;"
                        + "summons with 'default' index should have the board size as index.")

    for index_to_action in summon_action_pb.index_to_actions:
        inflated_action = action_pb2.Action()
        inflated_summon = getattr(inflated_action, action_pb.WhichOneof("action_oneof"))

        inflated_summon.source_id = summon_action_pb.source_id
        inflated_summon.index_to_actions.extend([index_to_action])

        inflated_actions.append(inflated_action)
    return inflated_actions


def inflate_actions(
        action_pbs,  # type: List[action_pb2.Action]
):
    # type: (...) -> List[action_pb2.Action] ("inflated" aka "singular")
    inflated_actions = []
    for action_pb in action_pbs:
        if (action_pb.WhichOneof("action_oneof") in _SPELL_ACTION_TYPES):
            inflated_actions.extend(_inflate_spell(action_pb))

        elif (action_pb.WhichOneof("action_oneof") == ACTION_TYPE_SUMMON or
              action_pb.WhichOneof("action_oneof") == ACTION_TYPE_WEAPON):

            inflated_actions.extend(_inflate_summon(action_pb))

        elif action_pb.WhichOneof("action_oneof") == ACTION_TYPE_DISCOVER:

            inflated_actions.append(action_pb)

        elif (action_pb.WhichOneof("action_oneof") == ACTION_TYPE_MULLIGAN or
              action_pb.WhichOneof("action_oneof") == ACTION_TYPE_END_TURN):

            inflated_actions.append(action_pb)

        elif action_pb.WhichOneof("action_oneof") == ACTION_TYPE_PHYSICAL_ATTACK:

            inflated_actions.extend(_inflate_physical_attack(action_pb))

        else:
            raise Exception("Cannot inflate unsupported action type '{}'.".format(
                action_pb.WhichOneof("action_oneof")))
    return inflated_actions


####################################################################################################
# Undo any Spellsource-incompatible changes made during inflation.
####################################################################################################

def deflate_action(
        in_pb,  # type: action_pb2.Action
):
    out_pb = action_pb2.Action()
    out_pb.CopyFrom(in_pb)

    if out_pb.WhichOneof("action_oneof") in _SPELL_ACTION_TYPES:
        spell_action_pb = getattr(out_pb, out_pb.WhichOneof("action_oneof"))
        if spell_action_pb.HasField("action"):
            if len(spell_action_pb.target_key_to_actions) != 1:
                raise Exception("Spell action to be deflated must have exactly one target.")
            target = spell_action_pb.target_key_to_actions[0].target
            if target == -1:
                spell_action_pb.target_key_to_actions.pop()

    return out_pb
