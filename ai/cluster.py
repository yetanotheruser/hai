from __future__ import absolute_import
from __future__ import print_function

import tensorflow as tf


def definition(
    num_workers,  # type: int
    num_param_servers,  # type: int
    localhost,  # type: bool
):
    # type: (...) -> tf.train.ClusterDef
    """
    More tensorflow setup for data parallelism
    """
    PS_HOST_TEMPLATE = "ps{}"
    WORKER_HOST_TEMPLATE = "worker{}"
    spec = {}
    PS_PORT = 10000
    WORKER_PORT = 20000

    param_servers = []
    for i in range(num_param_servers):
        if localhost:
            ps_host = "localhost"
        else:
            ps_host = PS_HOST_TEMPLATE.format(i)
        param_servers.append("{}:{}".format(ps_host, PS_PORT))
    spec['ps'] = param_servers

    workers = []
    for i in range(num_workers):
        if localhost:
            worker_host = "localhost"
        else:
            worker_host = WORKER_HOST_TEMPLATE.format(i)
        workers.append("{}:{}".format(worker_host, WORKER_PORT))
    spec['worker'] = workers
    return tf.train.ClusterSpec(spec).as_cluster_def()
