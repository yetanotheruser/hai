from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import (
    datetime,
    timedelta,
    tzinfo,
)

from absl import flags
from absl import logging
from katib.proto import (
    api_pb2_grpc,
)
import grpc

import katib.proto.api_pb2 as api_pb2

FLAGS = flags.FLAGS
flags.DEFINE_string("katib_study_id", None, "")
flags.DEFINE_string("katib_worker_id", None, "")
flags.DEFINE_string("katib_uri", "", "")


ZERO = timedelta(0)
class UTC(tzinfo):
    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO
_UTC = UTC()


def _client(uri):
    channel = grpc.insecure_channel(uri)
    return api_pb2_grpc.ManagerStub(channel)


def _now_str_rfc3339():
    now = datetime.now(_UTC)
    return now.isoformat("T")


def log_error():
    if FLAGS.katib_uri != "":
        flags.mark_flag_as_required("katib_study_id")
        flags.mark_flag_as_required("katib_worker_id")

        grpc_client = _client(FLAGS.katib_uri)
        grpc_client.ReportMetricsLogs(
            api_pb2.ReportMetricsLogsRequest(
                study_id=FLAGS.katib_study_id,
                metrics_log_sets=[api_pb2.MetricsLogSet(
                    worker_id=FLAGS.katib_worker_id,
                    worker_status=api_pb2.ERROR,
                )],
            ),
        )

    else:
        logging.info("Didn't report error to Katib since no uri was provided.")


def log_metric(name, value):
    if FLAGS.katib_uri != "":
        flags.mark_flag_as_required("katib_study_id")
        flags.mark_flag_as_required("katib_worker_id")

        grpc_client = _client(FLAGS.katib_uri)
        grpc_client.ReportMetricsLogs(
            api_pb2.ReportMetricsLogsRequest(
                study_id=FLAGS.katib_study_id,
                metrics_log_sets=[api_pb2.MetricsLogSet(
                    metrics_logs=[api_pb2.MetricsLog(
                        name=name,
                        values=[api_pb2.MetricsValueTime(
                            time=_now_str_rfc3339(),
                            value=str(value),
                        )],
                    )],
                    worker_id=FLAGS.katib_worker_id,
                    worker_status=api_pb2.COMPLETED,
                )],
            ),
        )

    else:
        logging.info(
            "Didn't send metric '{}' (value '{}') to Katib since no uri was provided.".format(
                name, value))
