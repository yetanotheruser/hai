from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags
import numpy as np
import tensorflow as tf
import tensorflow.contrib.rnn as rnn

from ai.model.complex.action_encoder import (
    ActionEncoder,
)
from ai.model.complex.game_state_encoder import (
    GameStateEncoder,
)
from ai.tf_util import (
    assert_tensor_has_static_shape,
)
import ai.policy.optimizer.common  # for the flags
import ai.simulator_parameters as sp
import ai.tf_util as tf_util

FLAGS = flags.FLAGS
flags.DEFINE_float("action_prob_epsilon", 1e-9, "")
flags.DEFINE_integer("gru_n_units", 256, "Size GRU hidden state.")


def _flatten(x):
    return tf.reshape(x, [-1, np.prod(x.get_shape().as_list()[1:])])


def _logits_with_dummies_to_probs(
        logits_with_dummies,
        n_real_logits,
        # For testing only.
        action_prob_epsilon=None,  # type: Optional[float]
):
    """
    NOTE: `log_action_probs` entries past `n_real_logits` will have arbitrary values.
    """
    if action_prob_epsilon == None:
        action_prob_epsilon = FLAGS.action_prob_epsilon

    batch_dim = logits_with_dummies.shape.as_list()[0]
    assert_tensor_has_static_shape(logits_with_dummies, (batch_dim, sp.GET_MAX_N_ACTIONS()))
    assert_tensor_has_static_shape(n_real_logits, (batch_dim, 1))

    # NOTE: We add a small number to the probabilities to avoid underflow. The later normalization
    # step will (as a side effect) make sure all relevant proabilities sum to 1 so that won't be an
    # issue.
    action_probs_with_dummies = tf.maximum(tf.nn.softmax(logits_with_dummies),
                                           action_prob_epsilon)
    assert_tensor_has_static_shape(action_probs_with_dummies, (batch_dim, sp.GET_MAX_N_ACTIONS()))
    action_mask = tf.sequence_mask(tf.squeeze(n_real_logits, axis=1),
                                   maxlen=sp.GET_MAX_N_ACTIONS(),
                                   dtype=tf.float32)
    assert_tensor_has_static_shape(action_mask, (batch_dim, sp.GET_MAX_N_ACTIONS()))
    total_non_dummy_action_probs = tf.reduce_sum(action_probs_with_dummies * action_mask,
                                                 axis=1, keepdims=True)
    assert_tensor_has_static_shape(total_non_dummy_action_probs, (batch_dim, 1))
    normalized_action_probs_with_dummies = tf.divide(action_probs_with_dummies,
                                                     total_non_dummy_action_probs)
    action_probs = normalized_action_probs_with_dummies * action_mask

    # NOTE: We must be careful to manually ignore the "dummy" entries in `log_action_probs`.
    log_action_probs = tf.log(normalized_action_probs_with_dummies)
    assert_tensor_has_static_shape(log_action_probs, (batch_dim, sp.GET_MAX_N_ACTIONS()))

    return action_probs, log_action_probs


def _lstm(
        x,  # type: tf.Tensor[None, input_size]
):
    # Dead code kept around in case I want to perturb it into a usable LSTM replacement for `_Gru`.
    with tf.variable_scope("lstm"):
        x = tf.expand_dims(_flatten(x), [0])

        lstm = rnn.BasicLSTMCell(LSTM_SIZE, state_is_tuple=True)
        step_size = tf.shape(x)[:1]

        c_init = np.zeros((1, LSTM_SIZE), np.float32)
        h_init = np.zeros((1, LSTM_SIZE), np.float32)
        state_init = [c_init, h_init]

        c_in = tf.placeholder(tf.float32, [1, lstm.state_size.c])
        h_in = tf.placeholder(tf.float32, [1, lstm.state_size.h])
        state_in = [c_in, h_in]

        state_in = rnn.LSTMStateTuple(c_in, h_in)
        lstm_outputs, lstm_state = tf.nn.dynamic_rnn(
            lstm, x, initial_state=state_in, sequence_length=step_size,
            time_major=False)
        lstm_c, lstm_h = lstm_state
        x = tf.reshape(lstm_outputs, [-1, LSTM_SIZE])

        state_out = [lstm_c[:1, :], lstm_h[:1, :]]

        return x, state_in, state_out, state_init


class _Gru(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        with tf.variable_scope("gru_initial_state"):
            self.initial_state = np.zeros((1, FLAGS.gru_n_units), np.float32)
        super(_Gru, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._gru_cell = rnn.GRUCell(FLAGS.gru_n_units)
        super(_Gru, self).build(input_shape)

    def call(self, x):
        assert isinstance(x, list)
        x, input_state = x
        batch_dim = x.shape.as_list()[0]

        assert_tensor_has_static_shape(x, (batch_dim, tf_util.ANY))
        assert_tensor_has_static_shape(input_state, (batch_dim, FLAGS.gru_n_units))
        # _dynamic_ batch size. May be smaller than `FLAGS.minibatch_size` in some cases (when two
        # gRPC agents are being driven at the same time).
        batch_size = tf.shape(x)[0]

        # Insert a time dimension (of 1), since we process one timestep at a time.
        x = tf.expand_dims(x, axis=1)
        assert_tensor_has_static_shape(x, (batch_dim, 1, tf_util.ANY))

        gru_outputs, state_out = tf.nn.dynamic_rnn(
            self._gru_cell,
            x,
            initial_state=input_state,
            parallel_iterations=FLAGS.minibatch_size,
            sequence_length=tf.ones([batch_size]),
        )
        x = tf.squeeze(gru_outputs, axis=1)
        assert_tensor_has_static_shape(x, (batch_dim, FLAGS.gru_n_units))
        assert_tensor_has_static_shape(state_out, (batch_dim, FLAGS.gru_n_units))

        return x, state_out

    @classmethod
    def get_state_placeholder(cls):
        return tf.placeholder(tf.float32, [None, FLAGS.gru_n_units])


class _OutputActionDecoder(tf.keras.layers.Layer):
    def __init__(
            self,
            encoded_action_size,  # type: int
            *args, **kwargs):
        self._encoded_action_size = encoded_action_size
        super(_OutputActionDecoder, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._dense = tf.keras.layers.Dense(
            self._encoded_action_size,
            activation=tf.nn.relu,
            name="output_action_dense",
        )
        super(_OutputActionDecoder, self).build(input_shape)

    def call(self, x):
        assert isinstance(x, list)
        encoded_actions, state_representation = x
        batch_dim = encoded_actions.shape.as_list()[0]

        assert_tensor_has_static_shape(state_representation, (batch_dim, tf_util.ANY))
        assert_tensor_has_static_shape(encoded_actions, (batch_dim, sp.GET_MAX_N_ACTIONS(),
                                                         self._encoded_action_size))

        dense = self._dense(state_representation)
        logits = tf.reduce_mean(tf.multiply(encoded_actions, tf.expand_dims(dense, 1)), axis=-1)
        assert_tensor_has_static_shape(logits, (batch_dim, sp.GET_MAX_N_ACTIONS()))

        return logits


class _ValueDecoder(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_ValueDecoder, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._dense = tf.keras.layers.Dense(1, activation=tf.nn.tanh, name="dense")
        super(_ValueDecoder, self).build(input_shape)

    def call(self, x):
        return tf.reshape(self._dense(x), [-1])


class ComplexModel(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(ComplexModel, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._action_encoder = ActionEncoder(name="actions_encoder")
        self._action_set_max_pool = tf.keras.layers.MaxPool1D(pool_size=sp.GET_MAX_N_ACTIONS(),
                                                              name="action_set_max_pool")
        self._encoded_action_size = self._action_encoder.get_encoded_action_size()
        self._game_state_encoder = GameStateEncoder(name="game_state_encoder")
        self._gru = _Gru(name="gru")
        self._output_action_decoder = _OutputActionDecoder(self._encoded_action_size,
                                                           name="output_action_decoder")
        self._value_decoder = _ValueDecoder(name="value_decoder")

        self.initial_state = self._gru.initial_state
        super(ComplexModel, self).build(input_shape)

    def call(self, x):
        assert isinstance(x, list)
        addressable_entities, actions, input_temporal_state, n_actions = x
        batch_dim = input_temporal_state.shape.as_list()[0]

        assert_tensor_has_static_shape(input_temporal_state, (batch_dim, FLAGS.gru_n_units))

        encoded_game_state, encoded_addressable_entities, encoded_indices = \
            self._game_state_encoder(addressable_entities)
        encoded_actions = []
        for action in actions:
            encoded_actions.append(
                self._action_encoder([encoded_addressable_entities, encoded_indices, action]))
        stacked_encoded_actions = tf.stack(encoded_actions, axis=1)
        assert_tensor_has_static_shape(stacked_encoded_actions, (batch_dim, sp.GET_MAX_N_ACTIONS(),
                                                                 self._encoded_action_size))

        encoded_actions_set = tf.squeeze(self._action_set_max_pool(stacked_encoded_actions), axis=1)
        assert_tensor_has_static_shape(encoded_actions_set, (batch_dim, self._encoded_action_size))

        encoded_inputs = tf.concat([encoded_game_state, encoded_actions_set], axis=1)
        assert_tensor_has_static_shape(encoded_inputs, (batch_dim, tf_util.ANY))

        (
            state_representation,
            output_temporal_state,
        ) = self._gru([encoded_inputs, input_temporal_state])
        state_representation = encoded_inputs
        output_temporal_state = input_temporal_state + 1

        action_logits_with_dummies = self._output_action_decoder([
            stacked_encoded_actions,
            state_representation,
        ])
        assert_tensor_has_static_shape(action_logits_with_dummies, (batch_dim, sp.GET_MAX_N_ACTIONS()))

        action_probs, log_action_probs = _logits_with_dummies_to_probs(action_logits_with_dummies,
                                                                       n_actions)
        value = self._value_decoder(state_representation)
        assert_tensor_has_static_shape(value, (batch_dim,))

        return action_probs, log_action_probs, value, output_temporal_state

    @classmethod
    def get_state_placeholder(cls):
        return _Gru.get_state_placeholder()
