from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math

from absl import flags
import google.protobuf.text_format as text_format
import numpy as np
import pytest
import sys
import tensorflow as tf
tf.enable_eager_execution()

from ai.model.complex.complex import (
    _logits_with_dummies_to_probs,
    ComplexModel,
)
from ai.policy.input.input import (
    _generate_action_deserializers,
    _generate_game_state_deserializers,
)
from ai.policy.policy import (
    _deserialize_and_pad_entities,
)
import ai.simulator_parameters as sp
import ai.test_util as test_util
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.game_state_pb2 as game_state_pb2
import spellsource.proto.player_pb2 as player_pb2

FLAGS = flags.FLAGS


class Test_LogitsWithDummiesToProbs(object):
    @pytest.mark.parametrize(
        "logits_with_dummies,n_real_logits,action_prob_epsilon,expected_action_probs,expected_log_action_probs",
        [(
            tf.constant([[1, 1, 1]], dtype=tf.float32),
            tf.constant([[2]], dtype=tf.float32),
            1e-9,
            np.array([[0.5, 0.5, 0]], np.float32),
            np.array([[math.log(0.5), math.log(0.5), 1]], np.float32),
        ), (
            tf.constant([[1, 1, 10]], dtype=tf.float32),
            tf.constant([[2]], dtype=tf.float32),
            1e-9,
            np.array([[0.5, 0.5, 0]], np.float32),
            np.array([[math.log(0.5), math.log(0.5), 1]], np.float32),
        )],
        ids=[
            "basic",
            "dummy_logits_dont_change_probs",
        ],
    )
    def test_template(
            self,
            monkeypatch,
            logits_with_dummies,
            n_real_logits,
            action_prob_epsilon,
            expected_action_probs,
            expected_log_action_probs,
    ):
        monkeypatch.setattr(sp, "GET_MAX_N_ACTIONS", lambda:
                            logits_with_dummies.shape[1])
        actual_action_probs, actual_log_action_probs = _logits_with_dummies_to_probs(
            logits_with_dummies,
            n_real_logits,
            action_prob_epsilon,
        )
        actual_action_probs = actual_action_probs.numpy()
        actual_log_action_probs = actual_log_action_probs.numpy()

        assert np.isclose(actual_action_probs, expected_action_probs).all()
        for i in range(logits_with_dummies.shape[0]):
            assert np.isclose(
                actual_log_action_probs[:, :n_real_logits[i][0]],
                expected_log_action_probs[:, :n_real_logits[i][0]]).all()


@pytest.fixture
def action_pbs(monkeypatch):
    monkeypatch.setattr(sp, "GET_MAX_N_ACTIONS", lambda: 2)
    ret = []
    action1_pb = action_pb2.Action()
    text_format.Parse(
        """
        spell {
            source_id: 0
        }
        """,
        action1_pb)
    ret.append(action1_pb)
    action2_pb = action_pb2.Action()
    text_format.Parse(
        """
        physical_attack {
            source_id: 1
            defenders {
                target: 1
            }
        }
        """,
        action2_pb)
    ret.append(action2_pb)
    return ret


@pytest.fixture
def game_state_pb():
    ret = game_state_pb2.GameState()
    text_format.Parse(
        """
        entities {
            card_id: "hero_anduin"
            entity_type: HERO
            state {
                location {
                    player: 0
                    zone: HERO
                }
                hp: 10
                base_hp: 30
            }
        }
        entities {
            card_id: "hero_guldan"
            entity_type: HERO
            state {
                location {
                    player: 1
                    zone: HERO
                }
                hp: 18
                base_hp: 30
            }
        }
        entities {
            card_id: "minion_bloodfen_raptor"
            entity_type: MINION
            state {
                location {
                    player: 1
                    zone: BATTLEFIELD
                }
                hp: 1
                base_hp: 2
            }
        }
        entities {
            card_id: "spell_hex"
            entity_type: CARD
            state {
                location {
                    player: 0
                    zone: HAND
                }
                playable: true
            }
        }
        """,
        ret)
    return ret


class TestComplexEncoder(object):
    def Atest_action_reordering_has_no_effect(self, action_pbs, game_state_pb, monkeypatch):
        test_util.fix_random_seeds()

        complex_model = ComplexModel()
        action_packer, _, action_parser = _generate_action_deserializers(np.float32)
        entity_packer, _, entity_parser = _generate_game_state_deserializers(np.float32)

        model_state = tf.ones([1, 256], dtype=tf.float32)
        n_actions = tf.constant([[2]], dtype=tf.int32)

        deserialized_entities = _deserialize_and_pad_entities(game_state_pb.entities, entity_packer,
                                                              player_pb2.Player_1)
        entities = entity_parser(deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)
        actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]

        (
            action_probs,
            log_action_probs,
            value,
            output_temporal_state,
        ) = complex_model([
            entities, actions, model_state, n_actions,
        ])

        (
            reversed_action_probs,
            reversed_log_action_probs,
            reversed_value,
            reversed_output_temporal_state,
        ) = complex_model([
            entities, reversed(actions), model_state, n_actions,
        ])

        assert np.isclose(action_probs, np.flip(reversed_action_probs, axis=1)).all()
        assert np.isclose(log_action_probs, np.flip(reversed_log_action_probs, axis=1)).all()
        assert np.isclose(value, reversed_value)
        assert np.isclose(output_temporal_state, reversed_output_temporal_state).all()

    def Atest_action_different_causes_changes(self, action_pbs, game_state_pb, monkeypatch):
        test_util.fix_random_seeds()

        complex_model = ComplexModel()
        action_packer, _, action_parser = _generate_action_deserializers(np.float32)
        entity_packer, _, entity_parser = _generate_game_state_deserializers(np.float32)

        model_state = tf.ones([1, 256], dtype=tf.float32)
        n_actions = tf.constant([[2]], dtype=tf.int32)

        deserialized_entities = _deserialize_and_pad_entities(game_state_pb.entities, entity_packer,
                                                              player_pb2.Player_1)
        entities = entity_parser(deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)

        before_actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]

        action_pbs[0].spell.source_id += 1
        action_pbs[1].physical_attack.defenders[0].target += 1
        after_actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]

        (
            before_action_probs,
            before_log_action_probs,
            before_value,
            before_output_temporal_state,
        ) = complex_model([
            entities, before_actions, model_state, n_actions,
        ])

        (
            after_action_probs,
            after_log_action_probs,
            after_value,
            after_output_temporal_state,
        ) = complex_model([
            entities, after_actions, model_state, n_actions,
        ])

        assert not np.isclose(before_action_probs, after_action_probs).any()
        assert not np.isclose(before_log_action_probs, after_log_action_probs).any()
        assert not np.isclose(before_value, after_value)
        # NOTE: Disable this assertion until we re-enable the GRU.
        #assert not np.isclose(before_output_temporal_state, after_output_temporal_state).any()

    def Atest_actions_past_n_actions_have_no_effect(self, action_pbs, game_state_pb, monkeypatch):
        test_util.fix_random_seeds()

        complex_model = ComplexModel()
        action_packer, _, action_parser = _generate_action_deserializers(np.float32)
        entity_packer, _, entity_parser = _generate_game_state_deserializers(np.float32)

        model_state = tf.ones([1, 256], dtype=tf.float32)
        n_actions = tf.constant([[1]], dtype=tf.int32)

        deserialized_entities = _deserialize_and_pad_entities(game_state_pb.entities, entity_packer,
                                                              player_pb2.Player_1)
        entities = entity_parser(deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)

        before_actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]

        action_pbs[1].physical_attack.defenders[0].target += 1
        after_actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]

        (
            before_action_probs,
            before_log_action_probs,
            before_value,
            before_output_temporal_state,
        ) = complex_model([
            entities, before_actions, model_state, n_actions,
        ])

        (
            after_action_probs,
            after_log_action_probs,
            after_value,
            after_output_temporal_state,
        ) = complex_model([
            entities, after_actions, model_state, n_actions,
        ])

        assert np.isclose(before_action_probs, after_action_probs).all()
        assert np.isclose(before_log_action_probs, after_log_action_probs).all()
        assert np.isclose(before_value, after_value)
        # NOTE: Disable this assertion until we re-enable the GRU.
        #assert np.isclose(before_output_temporal_state, after_output_temporal_state).all()

    def test_entity_different_causes_changes(self, action_pbs, game_state_pb, monkeypatch):
        test_util.fix_random_seeds()

        complex_model = ComplexModel()
        action_packer, _, action_parser = _generate_action_deserializers(np.float32)
        entity_packer, _, entity_parser = _generate_game_state_deserializers(np.float32)

        model_state = tf.ones([1, 256], dtype=tf.float32)
        n_actions = tf.constant([[2]], dtype=tf.int32)

        actions = [action_parser(action_packer(apb), 1)[0] for apb in action_pbs]
        before_deserialized_entities = _deserialize_and_pad_entities(game_state_pb.entities,
                                                                     entity_packer,
                                                                     player_pb2.Player_1)
        before_entities = entity_parser(before_deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)

        (
            before_action_probs,
            before_log_action_probs,
            before_value,
            before_output_temporal_state,
        ) = complex_model([
            before_entities, actions, model_state, n_actions,
        ])
        action_probs = [before_action_probs]
        log_action_probs = [before_log_action_probs]
        values = [before_value]
        output_temporal_states = [before_output_temporal_state]

        for i in range(len(game_state_pb.entities)):
            game_state_pb.entities[i].state.hp += 10
            game_state_pb.entities[i].state.base_hp -= 10
            after_deserialized_entities = _deserialize_and_pad_entities(game_state_pb.entities,
                                                                        entity_packer,
                                                                        player_pb2.Player_1)
            after_entities = entity_parser(after_deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)

            (
                after_action_probs,
                after_log_action_probs,
                after_value,
                after_output_temporal_state,
            ) = complex_model([
                after_entities, actions, model_state, n_actions,
            ])
            action_probs.append(after_action_probs)
            log_action_probs.append(after_log_action_probs)
            values.append(after_value)
            output_temporal_states.append(after_output_temporal_state)

        test_util.assert_ndarrays_not_close(*action_probs)
        test_util.assert_ndarrays_not_close(*log_action_probs)
        test_util.assert_ndarrays_not_close(*values)
        # NOTE: Disable this assertion until we re-enable the GRU.
        #test_util.assert_ndarrays_not_close(*output_temporal_states)


if __name__ == "__main__":
    FLAGS(["asdf"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assertions.
        __file__,
    ]))
