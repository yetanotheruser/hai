from __future__ import absolute_import
from __future__ import print_function

from absl import flags
import tensorflow as tf

from ai.agent.util import (
    get_addressable_entities_in_zone,
)
from ai.tf_util import (
    assert_tensor_has_static_shape,
)
import ai.simulator_parameters as sp
import ai.tf_util as tf_util
import spellsource.cards
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.entity_state_pb2 as entity_state_pb2

FLAGS = flags.FLAGS
flags.DEFINE_integer("entity_conv_depth", 1, "Number of 'entity conv' to stack.")
flags.DEFINE_integer("entity_conv_n_filters", 32, "Number of filters per entity conv.")
flags.DEFINE_integer("entity_embedding_size", 32, "Number of floats to embed cards with.")

def GET_ENTITY_ENCODING_SIZE():
    return FLAGS.entity_conv_n_filters + FLAGS.entity_embedding_size


def _create_addressable_entities(
        hand,
        battlefields,
        hero_powers,
        heros,
        weapons,
        set_aside_zone,
        discovers,
):
    assert_tensor_has_static_shape(hand, (tf_util.ANY, sp.MAX_HAND_SIZE,
                                          GET_ENTITY_ENCODING_SIZE()))
    for battlefield in battlefields:
        assert_tensor_has_static_shape(battlefield, (tf_util.ANY, sp.MAX_BOARD_SIZE,
                                                     GET_ENTITY_ENCODING_SIZE()))
    for hero_power in hero_powers:
        assert_tensor_has_static_shape(hero_power, (tf_util.ANY, 1, GET_ENTITY_ENCODING_SIZE()))
    for hero in heros:
        assert_tensor_has_static_shape(hero, (tf_util.ANY, 1, GET_ENTITY_ENCODING_SIZE()))
    for weapon in weapons:
        assert_tensor_has_static_shape(weapon, (tf_util.ANY, 1, GET_ENTITY_ENCODING_SIZE()))
    assert_tensor_has_static_shape(discovers, (tf_util.ANY, sp.MAX_DISCOVERS, GET_ENTITY_ENCODING_SIZE()))

    addressable_entities = tf.concat([
        hand,
        battlefields[0],
        battlefields[1],
        hero_powers[0],
        hero_powers[1],
        heros[0],
        heros[1],
        weapons[0],
        weapons[1],
        set_aside_zone,
        discovers,
    ], axis=1)
    assert_tensor_has_static_shape(addressable_entities, (tf_util.ANY, sp.N_ADDRESSABLE_ENTITIES,
                                                          GET_ENTITY_ENCODING_SIZE()))
    return addressable_entities


class _Board(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Board, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._conv = tf.keras.layers.Conv1D(
            filters=GET_ENTITY_ENCODING_SIZE(),
            kernel_size=3,
            activation=tf.nn.relu,
            padding="same",
            name="conv1d",
        )
        super(_Board, self).build(input_shape)

    def call(
            self,
            minions,  # type: tf.Tensor[None x MAX_BOARD_SIZE x <minion encoding size>]
    ):
        # type: (...) -> tf.Tensor[None x MAX_BOARD_SIZE x ENTITY_ENCODING_SIZE]
        batch_dim = minions.shape.as_list()[0]
        assert_tensor_has_static_shape(minions, (batch_dim, sp.MAX_BOARD_SIZE, tf_util.ANY))
        return self._conv(minions)


class _DenseStack(tf.keras.layers.Layer):
    """
    Repeated fully connected layers over inputs (probably a card or a minion).
    """

    def __init__(
            self,
            depth,  # type: int
            n_filters,  # type: int
            *args, **kwargs
    ):
        self._depth = depth
        self._n_filters = n_filters
        super(_DenseStack, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._layers = []
        for layer_num in range(self._depth):
            self._layers.append(
                tf.keras.layers.Dense(self._n_filters, activation=tf.nn.relu,
                                      name="dense_{}".format(layer_num))
            )
        super(_DenseStack, self).build(input_shape)

    def call(
            self,
            x,  # type: tf.Tensor
    ):
        # type: (...) -> tf.Tensor[None x n_filters]
        batch_dim = x.shape.as_list()[0]
        for layer in self._layers:
            x = layer(x)
        assert_tensor_has_static_shape(x, (batch_dim, self._n_filters))
        return x


def _flatten_entity(
        entity,  # type: Dict[tf.Tensor]
):
    # type: (...) -> tf.Tensor[None x ?]
    #return entity
    return tf.concat(entity.values(), axis=1)


class _Entities(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Entities, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        # NOTE: We initialize this "normally" because we'd prefer for things to be concentrated
        # around zero initially. In the case where we encounter a never-before-seen card during
        # evaluation we want to have "no preconceptions" instead of "random preconceptions".
        self._card_id_embeddings = tf.Variable(
            tf.initializers.random_normal()(
                [spellsource.cards.N_STANDARD_CARD_IDS, FLAGS.entity_embedding_size],
                dtype=tf.float32),
            #tf.zeros([spellsource.cards.N_STANDARD_CARD_IDS, FLAGS.entity_embedding_size],
            #         dtype=tf.float32),
            name="card_id_embeddings")
        self._card_conv_stack = _DenseStack(
            FLAGS.entity_conv_depth,
            FLAGS.entity_conv_n_filters,
            name="card_conv_stack",
        )
        super(_Entities, self).build(input_shape)

    def call(
            self,
            entities,  # type: List[Dict[tf.Tensor]]
    ):
        # type: (...) -> tf.Tensor[None x N_ADDRESSABLE_ENTITIES x ENTITY_ENCODING_SIZE]
        batch_dim = entities[0]["card_id"].shape.as_list()[0]

        card_ids_list = []
        entity_convolutions_list = []
        for entity in entities:
            card_ids_list.append(entity["card_id"])
            entity_convolutions_list.append(self._card_conv_stack(_flatten_entity(entity)))

        # Compute embeddings.
        for card_id in card_ids_list:
            assert_tensor_has_static_shape(card_id, (batch_dim,
                                                     spellsource.cards.N_STANDARD_CARD_IDS))
        one_hot_ids = tf.stack(card_ids_list, axis=1)
        assert_tensor_has_static_shape(one_hot_ids, (batch_dim, sp.N_ADDRESSABLE_ENTITIES,
                                                     spellsource.cards.N_STANDARD_CARD_IDS))

        # NOTE: `tf.tensordot` because `tf.matmul` doesn't broadcast...
        card_embeddings = tf.tensordot(one_hot_ids, self._card_id_embeddings, axes=[[2], [0]])
        assert_tensor_has_static_shape(card_embeddings, (batch_dim, sp.N_ADDRESSABLE_ENTITIES,
                                                         FLAGS.entity_embedding_size))

        # Concat with entity convolutions and return.
        entity_convolutions = tf.stack(entity_convolutions_list, axis=1)
        assert_tensor_has_static_shape(entity_convolutions, (batch_dim, sp.N_ADDRESSABLE_ENTITIES,
                                                             FLAGS.entity_conv_n_filters))
        return tf.concat([card_embeddings, entity_convolutions], axis=-1)


class _MinionIndices(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_MinionIndices, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._conv = tf.keras.layers.Conv1D(
            filters=GET_ENTITY_ENCODING_SIZE(),
            kernel_size=2,
            activation=tf.nn.relu,
            padding="valid",
            name="conv1d",
        )
        self._empty_minion_pad = tf.zeros([1, 1, GET_ENTITY_ENCODING_SIZE()], dtype=tf.float32)

        super(_MinionIndices, self).build(input_shape)

    def call(
            self,
            board,  # type: tf.Tensor[None x MAX_BOARD_SIZE x <board minion encoding size>]
    ):
        # type: (...) -> tf.Tensor[None x MAX_BOARD_SIZE x ENTITY_ENCODING_SIZE]
        batch_dim = board.shape.as_list()[0]

        assert_tensor_has_static_shape(board, (batch_dim, sp.MAX_BOARD_SIZE, tf_util.ANY))
        empty_minion_pad = tf.tile(self._empty_minion_pad, [tf.shape(board)[0], 1, 1])
        padded_board = tf.concat([empty_minion_pad, board, empty_minion_pad], axis=1)
        assert_tensor_has_static_shape(padded_board, (batch_dim, sp.MAX_BOARD_SIZE + 2, tf_util.ANY))
        indices = self._conv(padded_board)
        assert_tensor_has_static_shape(indices, (batch_dim, sp.N_BOARD_POSITIONS, tf_util.ANY))
        return indices


class GameStateEncoder(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(GameStateEncoder, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._board_layer = _Board(name="board")
        self._entities_layer = _Entities(name="entities")
        self._hand_set_max_pool = tf.keras.layers.MaxPool1D(pool_size=sp.MAX_HAND_SIZE,
                                                            name="hand_set_max_pool")
        self._discover_set_max_pool = tf.keras.layers.MaxPool1D(pool_size=sp.MAX_DISCOVERS,
                                                                name="discover_set_max_pool")
        self._hero_layer = tf.keras.layers.Dense(GET_ENTITY_ENCODING_SIZE(),
                                                 activation=tf.nn.relu, name="hero")
        self._minion_indices_layer = _MinionIndices(name="minion_indices")
        super(GameStateEncoder, self).build(input_shape)

    def call(
            self,
            addressable_entities,  # type: List[Dict[tf.Tensor]]
    ):
        # type: (...) -> Tuple[
        #   tf.Tensor[None x ?],
        #   tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES],
        # ]
        batch_dim = addressable_entities[0]["card_id"].shape.as_list()[0]

        encoded_entities = self._entities_layer(addressable_entities)
        assert_tensor_has_static_shape(encoded_entities, (batch_dim, sp.N_ADDRESSABLE_ENTITIES,
                                                          GET_ENTITY_ENCODING_SIZE()))

        # Encode hand.
        hand = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.HAND, "protagonist")
        assert_tensor_has_static_shape(hand, (batch_dim, sp.MAX_HAND_SIZE,
                                              GET_ENTITY_ENCODING_SIZE()))
        hand_set = tf.squeeze(self._hand_set_max_pool(hand), axis=1)
        assert_tensor_has_static_shape(hand_set, (batch_dim, GET_ENTITY_ENCODING_SIZE()))
        minions = get_addressable_entities_in_zone(encoded_entities,
                                                   entity_location_pb2.EntityLocation.BATTLEFIELD,
                                                   "protagonist")
        assert_tensor_has_static_shape(minions, (batch_dim, sp.MAX_BOARD_SIZE,
                                                 GET_ENTITY_ENCODING_SIZE()))
        board = self._board_layer(minions)
        assert_tensor_has_static_shape(board, (batch_dim, sp.MAX_BOARD_SIZE,
                                               GET_ENTITY_ENCODING_SIZE()))
        # Encode hero power.
        hero_power = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.HERO_POWER, "protagonist")
        assert_tensor_has_static_shape(hero_power, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))
        # Encode hero.
        hero = self._hero_layer(get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.HERO, "protagonist"))
        assert_tensor_has_static_shape(hero, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))
        # Encode weapon.
        weapon = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.WEAPON, "protagonist")
        assert_tensor_has_static_shape(weapon, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))

        # Encode enemy board.
        enemy_minions = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.BATTLEFIELD, "antagonist")
        assert_tensor_has_static_shape(enemy_minions, (batch_dim, sp.MAX_BOARD_SIZE,
                                                       GET_ENTITY_ENCODING_SIZE()))
        enemy_board = self._board_layer(enemy_minions)
        assert_tensor_has_static_shape(enemy_board, (batch_dim, sp.MAX_BOARD_SIZE,
                                                     GET_ENTITY_ENCODING_SIZE()))
        # Encode enemy hero power.
        enemy_hero_power = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.HERO_POWER, "antagonist")
        assert_tensor_has_static_shape(enemy_hero_power, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))
        # Encode enemy hero.
        enemy_hero = self._hero_layer(get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.HERO, "antagonist"))
        assert_tensor_has_static_shape(enemy_hero, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))
        # Encode enemy weapon.
        enemy_weapon = get_addressable_entities_in_zone(
            encoded_entities, entity_location_pb2.EntityLocation.WEAPON, "antagonist")
        assert_tensor_has_static_shape(enemy_weapon, (batch_dim, 1, GET_ENTITY_ENCODING_SIZE()))

        # Encode set aside zone (only used for mulligans afaik).
        set_aside_zone = get_addressable_entities_in_zone(
            encoded_entities,
            entity_location_pb2.EntityLocation.SET_ASIDE_ZONE,
            "protagonist")

        # Encode discovers.
        discovers = get_addressable_entities_in_zone(encoded_entities,
                                                     entity_location_pb2.EntityLocation.DISCOVER,
                                                     "protagonist")
        assert_tensor_has_static_shape(discovers, (batch_dim, sp.MAX_DISCOVERS,
                                                   GET_ENTITY_ENCODING_SIZE()))
        discovers_set = tf.squeeze(self._hand_set_max_pool(hand), axis=1)
        assert_tensor_has_static_shape(discovers_set, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

        encoded_game_state = tf.concat([
            hand_set,
            tf.keras.layers.Flatten()(board),
            tf.squeeze(hero, axis=1),
            tf.squeeze(weapon, axis=1),
            tf.keras.layers.Flatten()(enemy_board),
            tf.squeeze(enemy_hero, axis=1),
            tf.squeeze(enemy_weapon, axis=1),
            discovers_set,
        ], axis=1)
        assert_tensor_has_static_shape(encoded_game_state, (batch_dim, tf_util.ANY))

        encoded_addressable_entities = tf.transpose(
            _create_addressable_entities(
                hand=hand,
                battlefields=[board, enemy_board],
                hero_powers=[hero_power, enemy_hero_power],
                heros=[hero, enemy_hero],
                weapons=[weapon, enemy_weapon],
                set_aside_zone=set_aside_zone,
                discovers=discovers,
            ),
            perm=[0, 2, 1],
        )
        assert_tensor_has_static_shape(encoded_addressable_entities,
                                       (batch_dim, GET_ENTITY_ENCODING_SIZE(),
                                        len(addressable_entities)))

        # Encode features for minion summon indices.
        encoded_indices = tf.transpose(self._minion_indices_layer(board), perm=[0, 2, 1])
        assert_tensor_has_static_shape(encoded_indices, (batch_dim, tf_util.ANY,
                                                         sp.N_BOARD_POSITIONS))

        return encoded_game_state, encoded_addressable_entities, encoded_indices
