from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags
import tensorflow as tf

from ai.model.complex.game_state_encoder import (
    GET_ENTITY_ENCODING_SIZE,
)
from ai.tf_util import (
    assert_tensor_has_static_shape,
)
import ai.simulator_parameters as sp
import ai.tf_util as tf_util

FLAGS = flags.FLAGS
flags.DEFINE_bool("action_type_embedding_enabled", False, "If true use action type embedding.")
flags.DEFINE_bool("action_condition_physical_attack", False, "")
flags.DEFINE_bool("action_condition_spell", False, "")
flags.DEFINE_bool("action_condition_summon", False, "")
flags.DEFINE_integer("action_type_embedding_size",
                     8,
                     "Only matters if `action_type_embedding_enabled`.")
flags.DEFINE_integer("action_to_entity_size_multiplier", 2, "")

# Want to at least be able to encode (1) entity action "originates" from and (2) entity action is
# targeting. We assume the other variances in an action can be represented within this many bytes.
def GET_VALID_SUBACTION_ENCODING_SIZE():
    return FLAGS.action_to_entity_size_multiplier * GET_ENTITY_ENCODING_SIZE()
def GET_VALID_ACTION_ENCODING_SIZE(action_type_embedding_enabled):
    if action_type_embedding_enabled:
        return FLAGS.action_type_embedding_size + GET_VALID_SUBACTION_ENCODING_SIZE()
    else:
        return GET_VALID_SUBACTION_ENCODING_SIZE()
ACTION_ENTITY_ID_FIELDS = {
    "battlecry": (
        ("source_id",),
        ("target_key_to_actions", "elems", 0, "target"),
    ),
    "discover": (
        ("card_id",),
    ),
    "end_turn": (),  # End turn requires no id processing.
    "hero": (
        ("source_id",),
        ("target_key_to_actions", "elems", 0, "target"),
    ),
    "hero_power": (
        ("source_id",),
        ("target_key_to_actions", "elems", 0, "target"),
    ),
    "physical_attack": (
        ("source_id",),
        ("defenders", "elems", 0, "target"),
    ),
    "spell": (
        ("source_id",),
        ("target_key_to_actions", "elems", 0, "target"),
    ),
    "summon": (
        ("source_id",),
    ),
    "weapon": (
        ("source_id",),
    ),

    "mulligan": (
        ("discard_entity_ids", "elems", 0),
        ("discard_entity_ids", "elems", 1),
        ("discard_entity_ids", "elems", 2),
        ("discard_entity_ids", "elems", 3),
        ("keep_entity_ids", "elems", 0),
        ("keep_entity_ids", "elems", 1),
        ("keep_entity_ids", "elems", 2),
        ("keep_entity_ids", "elems", 3),
    ),
}


_SQRT_EPSILON = 1e-8


def _dereference(
        action,  # type: Dict[str, <this type> | tf.Tensor]
        action_attr_path,  # type: Tuple[str | int ...]
        elems,  # type: tf.Tensor[None x <elem encoding size> x <# of elems>]
        verify_entity_dereference=True,  # type: bool
        offset=0,  # type: int
):
    # type: (...) -> tf.Tensor[None x <elem encoding size>]
    """
    Element dereference implementation using `tf.matmul` under the hood.
    """
    batch_dim, elem_encoding_size, n_elems = elems.shape.as_list()

    assert_tensor_has_static_shape(elems, (batch_dim, tf_util.ANY, tf_util.ANY))
    if verify_entity_dereference:
        # This assertion prevents us from "dereferencing" any entity id that was not decoded.
        assert action_attr_path[0] in ACTION_ENTITY_ID_FIELDS
        assert action_attr_path[1:] in ACTION_ENTITY_ID_FIELDS[action_attr_path[0]]

    id_ = action
    for path_elem in action_attr_path:
        id_ = id_[path_elem]
    assert_tensor_has_static_shape(id_, (batch_dim, 1))
    id_ += offset

    one_hot_ids = tf.one_hot(tf.cast(id_, tf.int32), n_elems, axis=1)
    assert_tensor_has_static_shape(one_hot_ids, (batch_dim, n_elems, 1))

    entity = tf.matmul(elems, one_hot_ids)
    assert_tensor_has_static_shape(entity, (batch_dim, elem_encoding_size, 1))
    squeezed_entity = tf.squeeze(entity, axis=-1)
    return squeezed_entity


class _Discover(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Discover, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                            activation=tf.nn.relu,
                                            name="dense")
        super(_Discover, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        _,
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_dim = entities.shape.as_list()[0]
        assert_tensor_has_static_shape(entities, (batch_dim, GET_ENTITY_ENCODING_SIZE(),
                                                  sp.N_ADDRESSABLE_ENTITIES))
        card_entity = _dereference(action, ("discover", "card_id"), entities)
        assert_tensor_has_static_shape(card_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))
        encoded_subaction = self._dense(card_entity)
        assert_tensor_has_static_shape(encoded_subaction, (batch_dim,
                                                           GET_VALID_SUBACTION_ENCODING_SIZE()))
        return encoded_subaction


class _EndTurn(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_EndTurn, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._end_turn_embedding = tf.Variable(
            tf.random_uniform([1, GET_VALID_SUBACTION_ENCODING_SIZE()], -1.0, 1.0),
            name="end_turn_embedding")
        super(_EndTurn, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        _,
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_size = tf.shape(entities)[0]
        return tf.tile(self._end_turn_embedding, [batch_size, 1])


class _Mulligan(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Mulligan, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self._dense = tf.keras.layers.Dense(
            GET_VALID_SUBACTION_ENCODING_SIZE(), activation=tf.nn.relu, name="final_dense")
        self._discard_entity_layer = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                           name="discard_layer")
        self._keep_entity_layer = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                        name="keep_layer")
        self._discard_max_pool = tf.keras.layers.MaxPool1D(pool_size=sp.MAX_STARTING_HAND_SIZE,
                                                           name="discard_max_pool")
        self._keep_max_pool = tf.keras.layers.MaxPool1D(pool_size=sp.MAX_STARTING_HAND_SIZE,
                                                        name="keep_max_pool")
        super(_Mulligan, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        _,
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_dim = entities.shape.as_list()[0]
        assert_tensor_has_static_shape(entities, (batch_dim, GET_ENTITY_ENCODING_SIZE(),
                                                  sp.N_ADDRESSABLE_ENTITIES))

        discard_entities = []
        for i in range(sp.MAX_STARTING_HAND_SIZE):
            selected_entity = _dereference(action, ("mulligan", "discard_entity_ids", "elems", i),
                                           entities)
            assert_tensor_has_static_shape(selected_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))
            discard_entities.append(self._discard_entity_layer(selected_entity))

        stacked_discard_entities = tf.stack(discard_entities, axis=1)
        assert_tensor_has_static_shape(stacked_discard_entities,
                                       (batch_dim, sp.MAX_STARTING_HAND_SIZE,
                                        GET_VALID_SUBACTION_ENCODING_SIZE()))

        discard_entities_mask = tf.cast(
            tf.sequence_mask(action["mulligan"]["discard_entity_ids"]["size"],
                             maxlen=sp.MAX_STARTING_HAND_SIZE), tf.float32)
        discard_entities_mask = tf.transpose(discard_entities_mask, perm=[0, 2, 1])
        assert_tensor_has_static_shape(discard_entities_mask, (batch_dim, sp.MAX_STARTING_HAND_SIZE, 1))

        masked_discard_entities = tf.multiply(discard_entities_mask, stacked_discard_entities)
        pooled_discard_entities_fat = self._discard_max_pool(masked_discard_entities)
        pooled_discard_entities = tf.squeeze(pooled_discard_entities_fat, axis=1)
        assert_tensor_has_static_shape(pooled_discard_entities,
                                       (batch_dim, GET_VALID_SUBACTION_ENCODING_SIZE()))

        keep_entities = []
        for i in range(sp.MAX_STARTING_HAND_SIZE):
            selected_entity = _dereference(action, ("mulligan", "keep_entity_ids", "elems", i),
                                           entities)
            assert_tensor_has_static_shape(selected_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))
            keep_entities.append(self._keep_entity_layer(selected_entity))

        stacked_keep_entities = tf.stack(keep_entities, axis=1)
        assert_tensor_has_static_shape(stacked_keep_entities,
                                       (batch_dim, sp.MAX_STARTING_HAND_SIZE,
                                        GET_VALID_SUBACTION_ENCODING_SIZE()))

        keep_entities_mask = tf.cast(
            tf.sequence_mask(action["mulligan"]["keep_entity_ids"]["size"],
                             maxlen=sp.MAX_STARTING_HAND_SIZE), tf.float32)
        keep_entities_mask = tf.transpose(keep_entities_mask, perm=[0, 2, 1])
        assert_tensor_has_static_shape(keep_entities_mask, (batch_dim, sp.MAX_STARTING_HAND_SIZE, 1))

        masked_keep_entities = tf.multiply(keep_entities_mask, stacked_keep_entities)
        pooled_keep_entities_fat = self._keep_max_pool(masked_keep_entities)
        pooled_keep_entities = tf.squeeze(pooled_keep_entities_fat, axis=1)
        assert_tensor_has_static_shape(pooled_keep_entities,
                                       (batch_dim, GET_VALID_SUBACTION_ENCODING_SIZE()))

        encoded_subaction = self._dense(tf.concat([pooled_discard_entities, pooled_keep_entities],
                                                  axis=1))
        assert_tensor_has_static_shape(encoded_subaction, (batch_dim, GET_VALID_SUBACTION_ENCODING_SIZE()))
        return encoded_subaction


class _PhysicalAttack(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_PhysicalAttack, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        if FLAGS.action_condition_physical_attack:
            self._source_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="source_dense")
            self._target_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="target_dense")
        self._final_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                  activation=tf.nn.relu,
                                                  name="final_dense")
        super(_PhysicalAttack, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        _,
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_dim = entities.shape.as_list()[0]
        assert_tensor_has_static_shape(entities, (batch_dim, GET_ENTITY_ENCODING_SIZE(),
                                                  sp.N_ADDRESSABLE_ENTITIES))

        source_entity = _dereference(action, ("physical_attack", "source_id"), entities)
        assert_tensor_has_static_shape(source_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

        assert len(action["physical_attack"]["defenders"]["elems"]) == 1
        target_entity = _dereference(action,
                                     ("physical_attack", "defenders", "elems", 0, "target"),
                                     entities)
        assert_tensor_has_static_shape(target_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

        if FLAGS.action_condition_physical_attack:
            source_dense = tf.sqrt(self._source_dense(source_entity) + _SQRT_EPSILON)
            target_dense = tf.sqrt(self._target_dense(target_entity) + _SQRT_EPSILON)
            combined_source_target = tf.multiply(source_dense, target_dense)
        else:
            combined_source_target = tf.concat([source_entity, target_entity], axis=1)

        encoded_subaction = self._final_dense(combined_source_target)
        assert_tensor_has_static_shape(encoded_subaction, (batch_dim,
                                                           GET_VALID_SUBACTION_ENCODING_SIZE()))
        return encoded_subaction


class _Spell(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Spell, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        if FLAGS.action_condition_spell:
            self._source_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="source_dense")
            self._target_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="target_dense")

        self._final_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                  activation=tf.nn.relu,
                                                  name="final_dense")
        self._empty_target_embedding = tf.Variable(
            tf.random_uniform([1, GET_ENTITY_ENCODING_SIZE(), 1], -1.0, 1.0),
            name="empty_target_embedding")
        super(_Spell, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        action_type,  # type: str (one of ACTION_TYPE_KEYS)
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_dim = entities.shape.as_list()[0]

        with tf.variable_scope(action_type):
            batch_size = tf.shape(entities)[0]
            empty_target_embeddings = tf.tile(
                self._empty_target_embedding,
                (batch_size, 1, 1),
            )

            # Since spells can have empty target (which is represented by target -1, shifting over
            # everything else by 1), we prepend a learned "empty entity embedding" to the end of
            # entities.
            augmented_entities = tf.concat([empty_target_embeddings, entities], axis=2)
            assert_tensor_has_static_shape(augmented_entities, (batch_dim,
                                                                GET_ENTITY_ENCODING_SIZE(),
                                                                sp.N_ADDRESSABLE_ENTITIES + 1))

            source_entity = _dereference(action, (action_type, "source_id"), augmented_entities)
            assert_tensor_has_static_shape(source_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

            assert len(action[action_type]["target_key_to_actions"]["elems"]) == 1
            target_entity = _dereference(action,
                                         (action_type, "target_key_to_actions", "elems", 0, "target"),
                                         augmented_entities,
                                         offset=1,  # for the inserted empty target
                                         )
            assert_tensor_has_static_shape(target_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

            if FLAGS.action_condition_spell:
                source_dense = tf.sqrt(self._source_dense(source_entity) + _SQRT_EPSILON)
                target_dense = tf.sqrt(self._target_dense(target_entity) + _SQRT_EPSILON)
                combined_source_target = tf.multiply(source_dense, target_dense)
            else:
                combined_source_target = tf.concat([source_entity, target_entity], axis=1)

            encoded_subaction = self._final_dense(combined_source_target)
            assert_tensor_has_static_shape(encoded_subaction, (batch_dim, GET_VALID_SUBACTION_ENCODING_SIZE()))
            return encoded_subaction


class _Summon(tf.keras.layers.Layer):
    def __init__(self, *args, **kwargs):
        super(_Summon, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        if FLAGS.action_condition_summon:
            self._source_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="source_dense")
            self._index_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                       activation=tf.nn.relu,
                                                       name="index_dense")

        self._final_dense = tf.keras.layers.Dense(GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                  activation=tf.nn.relu,
                                                  name="final_dense")
        super(_Summon, self).build(input_shape)

    def call(
        self,
        action,  # type: Dict[str, <this type> | tf.Tensor]
        entities,  # type: tf.Tensor[None x ENTITY_ENCODING_SIZE x N_ADDRESSABLE_ENTITIES]
        indices,  # type: tf.Tensor[None x ? x N_BOARD_POSITIONS]
        action_type,  # type: str (one of ACTION_TYPE_KEYS)
    ):
        # type: (...) -> tf.Tensor[None x VALID_SUBACTION_ENCODING_SIZE]
        batch_dim = entities.shape.as_list()[0]

        assert_tensor_has_static_shape(entities, (batch_dim, GET_ENTITY_ENCODING_SIZE(),
                                                  sp.N_ADDRESSABLE_ENTITIES))
        assert_tensor_has_static_shape(indices, (batch_dim, tf_util.ANY, sp.N_BOARD_POSITIONS))

        with tf.variable_scope(action_type):
            source_entity = _dereference(action, (action_type, "source_id"), entities)
            assert_tensor_has_static_shape(source_entity, (batch_dim, GET_ENTITY_ENCODING_SIZE()))

            assert len(action[action_type]["index_to_actions"]["elems"]) == 1
            index = action[action_type]["index_to_actions"]["elems"][0]["index"]
            index_feature = _dereference(index, (), indices, verify_entity_dereference=False)
            assert_tensor_has_static_shape(index_feature, (batch_dim, tf_util.ANY))

            if FLAGS.action_condition_summon:
                source_dense = tf.sqrt(self._source_dense(source_entity) + _SQRT_EPSILON)
                index_dense = tf.sqrt(self._index_dense(index_feature) + _SQRT_EPSILON)
                combined_source_index = tf.multiply(source_dense, index_dense)
            else:
                combined_source_index = tf.concat([source_entity, index_feature], axis=1)

            encoded_subaction = self._final_dense(combined_source_index)
            assert_tensor_has_static_shape(encoded_subaction, (batch_dim,
                                                               GET_VALID_SUBACTION_ENCODING_SIZE()))
            return encoded_subaction


class ActionEncoder(tf.keras.layers.Layer):

    def __init__(self, action_type_embedding_enabled=None, *args, **kwargs):
        if action_type_embedding_enabled == None:
            action_type_embedding_enabled = FLAGS.action_type_embedding_enabled
        self._action_type_embedding_enabled = action_type_embedding_enabled
        super(ActionEncoder, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        if self._action_type_embedding_enabled:
            self._action_type_embeddings = self.add_variable(
                "action_type_embeddings",
                [
                    sp.NUM_ACTION_TYPES + 1,  # + 1 since we reserve 0 to indicate empty.
                    FLAGS.action_type_embedding_size,
                ],
                initializer="random_uniform",
            )

        self._subaction_encoders = {}
        self._dense = tf.keras.layers.Dense(
            GET_VALID_ACTION_ENCODING_SIZE(self._action_type_embedding_enabled),
            activation=tf.nn.relu,
            name="dense",
        )

        self._subaction_encoders[sp.ACTION_TYPE_DISCOVER] = _Discover(name="discover_encoder")
        self._subaction_encoders[sp.ACTION_TYPE_END_TURN] = _EndTurn(name="end_turn_encoder")
        self._subaction_encoders[sp.ACTION_TYPE_MULLIGAN] = _Mulligan(name="mulligan_encoder")
        self._subaction_encoders[sp.ACTION_TYPE_PHYSICAL_ATTACK] = _PhysicalAttack(
            name="physical_attack_encoder")

        spell_encoder = _Spell(name="spell_encoder")
        self._subaction_encoders[sp.ACTION_TYPE_BATTLECRY] = \
            lambda a, es, idcs: spell_encoder(a, es, sp.ACTION_TYPE_BATTLECRY)
        self._subaction_encoders[sp.ACTION_TYPE_HERO] = \
            lambda a, es, idcs: spell_encoder(a, es, sp.ACTION_TYPE_HERO)
        self._subaction_encoders[sp.ACTION_TYPE_HERO_POWER] = \
            lambda a, es, idcs: spell_encoder(a, es, sp.ACTION_TYPE_HERO_POWER)
        self._subaction_encoders[sp.ACTION_TYPE_SPELL] = \
            lambda a, es, idcs: spell_encoder(a, es, sp.ACTION_TYPE_SPELL)

        summon_encoder = _Summon(name="summon_encoder")
        self._subaction_encoders[sp.ACTION_TYPE_SUMMON] = \
            lambda a, es, idcs: summon_encoder(a, es, idcs, sp.ACTION_TYPE_SUMMON)
        self._subaction_encoders[sp.ACTION_TYPE_WEAPON] = \
            lambda a, es, idcs: summon_encoder(a, es, idcs, sp.ACTION_TYPE_WEAPON)

        super(ActionEncoder, self).build(input_shape)

    def call(self, x):
        # type: (...) -> tf.Tensor[None x VALID_ACTION_ENCODING_SIZE]
        assert isinstance(x, list)
        entities, indices, action = x
        batch_dim = entities.shape.as_list()[0]
        batch_size = tf.shape(entities)[0]

        assert_tensor_has_static_shape(entities, (batch_dim, tf_util.ANY, sp.N_ADDRESSABLE_ENTITIES))
        assert_tensor_has_static_shape(indices, (batch_dim, tf_util.ANY, sp.N_BOARD_POSITIONS))
        assert_tensor_has_static_shape(action["action_oneof"], (batch_dim, 1))

        if self._action_type_embedding_enabled:
            type_as_index = tf.reshape(tf.cast(action["action_oneof"], tf.int32), (-1,))
            action_type = tf.gather(self._action_type_embeddings, type_as_index)
            assert_tensor_has_static_shape(action_type, (batch_dim, FLAGS.action_type_embedding_size))

        dummy_no_actiononeof = tf.zeros(
            [batch_size, GET_VALID_SUBACTION_ENCODING_SIZE()], name="dummy_no_actiononeof")
        action_payloads = tf.stack(
            [dummy_no_actiononeof] +
            [
                self._subaction_encoders[t](action, entities, indices)
                for t in sp.ACTION_TYPE_KEYS
            ],
            axis=2,
        )
        assert_tensor_has_static_shape(action_payloads, (batch_dim,
                                                         GET_VALID_SUBACTION_ENCODING_SIZE(),
                                                         sp.NUM_ACTION_TYPES + 1))


        action_payload = _dereference(action,
                                      ("action_oneof",),
                                      action_payloads,
                                      verify_entity_dereference=False)
        assert_tensor_has_static_shape(action_payload, (batch_dim, GET_VALID_SUBACTION_ENCODING_SIZE()))

        if self._action_type_embedding_enabled:
            encoded_action = self._dense(tf.concat([action_type, action_payload], axis=1))
        else:
            encoded_action = action_payload
        assert_tensor_has_static_shape(
            encoded_action, (batch_dim,
                             GET_VALID_ACTION_ENCODING_SIZE(self._action_type_embedding_enabled)))
        return encoded_action

    def get_encoded_action_size(self):
        return GET_VALID_ACTION_ENCODING_SIZE(self._action_type_embedding_enabled)
