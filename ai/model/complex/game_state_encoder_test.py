from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

from absl import flags
import pytest
import tensorflow as tf
tf.enable_eager_execution()

from ai.agent.util import (
    entity_address,
)
from ai.model.complex.game_state_encoder import (
    _create_addressable_entities,
)
import ai.model.complex.game_state_encoder
import ai.simulator_parameters as sp
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.player_pb2 as player_pb2

FAKE_ENTITY_CONV_N_FILTERS = 3
FLAGS = flags.FLAGS


class Test_CreateAddressableEntities(object):
    def test_compatible_with_entity_address(self, monkeypatch):
        monkeypatch.setattr(
            ai.model.complex.game_state_encoder,
            "GET_ENTITY_ENCODING_SIZE",
            lambda: FAKE_ENTITY_CONV_N_FILTERS
        )
        counter = 0

        hand_tensors = []
        hand_begin_counter = counter
        for _ in range(sp.MAX_HAND_SIZE):
            hand_tensors.append(counter * tf.ones([1, FAKE_ENTITY_CONV_N_FILTERS]))
            counter += 1
        hand = tf.stack(hand_tensors, axis=1)

        battlefield1_tensors = []
        battlefield_begin_counter = counter
        for _ in range(sp.MAX_BOARD_SIZE):
            battlefield1_tensors.append(counter * tf.ones([1, FAKE_ENTITY_CONV_N_FILTERS]))
            counter += 1
        battlefield1 = tf.stack(battlefield1_tensors, axis=1)
        battlefield2_tensors = []
        for _ in range(sp.MAX_BOARD_SIZE):
            battlefield2_tensors.append(counter * tf.ones([1, FAKE_ENTITY_CONV_N_FILTERS]))
            counter += 1
        battlefield2 = tf.stack(battlefield2_tensors, axis=1)
        battlefields = [battlefield1, battlefield2]

        hero_power_begin_counter = counter
        hero_powers = [
            counter * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
            (counter + 1) * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
        ]
        counter += 2

        hero_begin_counter = counter
        heros = [
            counter * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
            (counter + 1) * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
        ]
        counter += 2

        weapon_begin_counter = counter
        weapons = [
            counter * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
            (counter + 1) * tf.ones([1, 1, FAKE_ENTITY_CONV_N_FILTERS]),
        ]
        counter += 2

        mulligans_begin_counter = counter
        mulligans_tensors = []
        for _ in range(sp.MAX_SET_ASIDE_ZONE_SIZE):
            mulligans_tensors.append(counter * tf.ones([1, FAKE_ENTITY_CONV_N_FILTERS]))
            counter += 1
        mulligans = tf.stack(mulligans_tensors, axis=1)

        discovers_begin_counter = counter
        discovers_tensors = []
        for _ in range(sp.MAX_DISCOVERS):
            discovers_tensors.append(counter * tf.ones([1, FAKE_ENTITY_CONV_N_FILTERS]))
            counter += 1
        discovers = tf.stack(discovers_tensors, axis=1)

        # Verify we (probably) remembered all addressable entities.
        assert counter == sp.N_ADDRESSABLE_ENTITIES

        addressable_entities = _create_addressable_entities(
            hand=hand,
            battlefields=battlefields,
            hero_powers=hero_powers,
            heros=heros,
            weapons=weapons,
            set_aside_zone=mulligans,
            discovers=discovers,
        )

        # Check hand.
        hand_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.HAND,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][hand_address].numpy() == hand_begin_counter).all()

        # Check battlefield.
        battlefield_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.BATTLEFIELD,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][battlefield_address].numpy() == battlefield_begin_counter).all()

        # Check hero_power.
        hero_power_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.HERO_POWER,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][hero_power_address].numpy() == hero_power_begin_counter).all()

        # Check hero.
        hero_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.HERO,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][hero_address].numpy() == hero_begin_counter).all()

        # Check weapon.
        weapon_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.WEAPON,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][weapon_address].numpy() == weapon_begin_counter).all()

        # Check mulligans.
        mulligans_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.SET_ASIDE_ZONE,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][mulligans_address].numpy() == mulligans_begin_counter).all()

        # Check discovers.
        discovers_address = entity_address(
            entity_location_pb2.EntityLocation(
                player=0,
                index=0,
                zone=entity_location_pb2.EntityLocation.DISCOVER,
            ),
            player_pb2.Player_1,
        )
        assert (addressable_entities[0][discovers_address].numpy() == discovers_begin_counter).all()


if __name__ == "__main__":
    FLAGS(["fake_program_name"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
