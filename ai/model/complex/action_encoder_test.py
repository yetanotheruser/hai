from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

from absl import flags
import google.protobuf.text_format as text_format
import numpy as np
import pytest
import tensorflow as tf
tf.enable_eager_execution()

from ai.model.complex.action_encoder import (
    ActionEncoder,
)
from ai.policy.input.input import (
    _generate_action_deserializers,
)
import ai.model.complex.action_encoder
import ai.simulator_parameters
import ai.test_util as test_util
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.spell_action_pb2 as spell_action_pb2

FLAGS = flags.FLAGS


@pytest.fixture
def entities():
    return tf.transpose(
        tf.constant([
            [
                [1, 1, 1, 1],  # first entity
                [-1, -1, -1, -1],  # second entity
            ],
        ], dtype=tf.float32),
        perm=[0, 2, 1],
    )


@pytest.fixture
def monkeypatch_action_encoder(monkeypatch):
    monkeypatch.setattr(ai.model.complex.action_encoder, "GET_ENTITY_ENCODING_SIZE", lambda: 4)
    monkeypatch.setattr(ai.simulator_parameters, "N_ADDRESSABLE_ENTITIES", 2)
    monkeypatch.setattr(ai.simulator_parameters, "N_BOARD_POSITIONS", 2)
    FLAGS.action_type_embedding_enabled = False


class TestActionEncoder(object):
    @pytest.mark.parametrize("action_type_embedding_enabled", [
        (False,),
        (True,),
    ], ids=[
        "without_action_type_embedding",
        "with_action_type_embedding",
    ])
    def test_uniqueness(self,
                        action_type_embedding_enabled,
                        entities,
                        monkeypatch_action_encoder):
        test_util.fix_random_seeds()

        action_encoder = ActionEncoder(action_type_embedding_enabled=action_type_embedding_enabled)
        packer, _, parser = _generate_action_deserializers(np.float32)
        indices = tf.transpose(
            tf.constant([
                [[2, 2, 2, 2], [-2, -2, -2, -2]],
            ], dtype=tf.float32),
            perm=[0, 2, 1],
        )

        encoded_actions = []

        ## Discover.
        # Card 0.
        discover_card_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            discover {
                card_id: 0
            }
            """,
            discover_card_0_pb)
        discover_card_0 = parser(packer(discover_card_0_pb), 1)[0]
        encoded_discover_card_0 = action_encoder([entities, indices, discover_card_0])
        encoded_actions.append(encoded_discover_card_0)
        # Card 1.
        discover_card_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            discover {
                card_id: 1
            }
            """,
            discover_card_1_pb)
        discover_card_1 = parser(packer(discover_card_1_pb), 1)[0]
        encoded_discover_card_1 = action_encoder([entities, indices, discover_card_1])
        encoded_actions.append(encoded_discover_card_1)

        ## End turn.
        end_turn_pb = action_pb2.Action()
        text_format.Parse(
            """
            end_turn: 0
            """,
            end_turn_pb)
        end_turn = parser(packer(end_turn_pb), 1)[0]
        encoded_end_turn = action_encoder([entities, indices, end_turn])
        encoded_actions.append(encoded_end_turn)

        ## Physical attacks.
        # Source 0.
        physical_attack_source_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            physical_attack {
                source_id: 0
                defenders {
                    target: 0
                }
            }
            """,
            physical_attack_source_0_pb)
        physical_attack_source_0 = parser(packer(physical_attack_source_0_pb), 1)[0]
        encoded_physical_attack_source_0 = action_encoder([entities, indices, physical_attack_source_0])
        encoded_actions.append(encoded_physical_attack_source_0)
        # Source 1.
        physical_attack_source_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            physical_attack {
                source_id: 1
                defenders {
                    target: 0
                }
            }
            """,
            physical_attack_source_1_pb)
        physical_attack_source_1 = parser(packer(physical_attack_source_1_pb), 1)[0]
        encoded_physical_attack_source_1 = action_encoder([entities, indices, physical_attack_source_1])
        encoded_actions.append(encoded_physical_attack_source_1)
        # Target 1.
        physical_attack_target_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            physical_attack {
                source_id: 1
                defenders {
                    target: 1
                }
            }
            """,
            physical_attack_target_1_pb)
        physical_attack_target_1 = parser(packer(physical_attack_target_1_pb), 1)[0]
        encoded_physical_attack_target_1 = action_encoder([entities, indices, physical_attack_target_1])
        encoded_actions.append(encoded_physical_attack_target_1)

        ## Spells.
        # Source 0.
        spell_source_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            spell {
                source_id: 0
                target_key_to_actions {
                    target: -1
                }
            }
            """,
            spell_source_0_pb)
        spell_source_0 = parser(packer(spell_source_0_pb), 1)[0]
        encoded_spell_source_0 = action_encoder([entities, indices, spell_source_0])
        encoded_actions.append(encoded_spell_source_0)
        # Source 1.
        spell_source_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            spell {
                source_id: 1
                target_key_to_actions {
                    target: -1
                }
            }
            """,
            spell_source_1_pb)
        spell_source_1 = parser(packer(spell_source_1_pb), 1)[0]
        encoded_spell_source_1 = action_encoder([entities, indices, spell_source_1])
        encoded_actions.append(encoded_spell_source_1)
        # Target 0.
        spell_target_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            spell {
                source_id: 0
                target_key_to_actions {
                    target: 0
                }
            }
            """,
            spell_target_0_pb)
        spell_target_0 = parser(packer(spell_target_0_pb), 1)[0]
        encoded_spell_target_0 = action_encoder([entities, indices, spell_target_0])
        encoded_actions.append(encoded_spell_target_0)
        # Target 1.
        spell_target_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            spell {
                source_id: 0
                target_key_to_actions {
                    target: 1
                }
            }
            """,
            spell_target_1_pb)
        spell_target_1 = parser(packer(spell_target_1_pb), 1)[0]
        encoded_spell_target_1 = action_encoder([entities, indices, spell_target_1])
        encoded_actions.append(encoded_spell_target_1)

        ## Summons.
        # Source entity 0.
        summon_source_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            summon {
                source_id: 0
                index_to_actions {
                    index: 0
                }
            }
            """,
            summon_source_0_pb)
        summon_source_0 = parser(packer(summon_source_0_pb), 1)[0]
        encoded_summon_source_0 = action_encoder([entities, indices, summon_source_0])
        encoded_actions.append(encoded_summon_source_0)
        # Source entity 1.
        summon_source_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            summon {
                source_id: 1
                index_to_actions {
                    index: 0
                }
            }
            """,
            summon_source_1_pb)
        summon_source_1 = parser(packer(summon_source_1_pb), 1)[0]
        encoded_summon_source_1 = action_encoder([entities, indices, summon_source_1])
        encoded_actions.append(encoded_summon_source_1)
        # Index 1.
        summon_index_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            summon {
                source_id: 0
                index_to_actions {
                    index: 1
                }
            }
            """,
            summon_index_1_pb)
        summon_index_1 = parser(packer(summon_index_1_pb), 1)[0]
        encoded_summon_index_1 = action_encoder([entities, indices, summon_index_1])
        encoded_actions.append(encoded_summon_index_1)

        ## Weapons.
        # Source entity 0.
        weapon_source_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            weapon {
                source_id: 0
                index_to_actions {
                    index: 1
                }
            }
            """,
            weapon_source_0_pb)
        weapon_source_0 = parser(packer(weapon_source_0_pb), 1)[0]
        encoded_weapon_source_0 = action_encoder([entities, indices, weapon_source_0])
        encoded_actions.append(encoded_weapon_source_0)
        # Source entity 1.
        weapon_source_1_pb = action_pb2.Action()
        text_format.Parse(
            """
            weapon {
                source_id: 1
                index_to_actions {
                    index: 1
                }
            }
            """,
            weapon_source_1_pb)
        weapon_source_1 = parser(packer(weapon_source_1_pb), 1)[0]
        encoded_weapon_source_1 = action_encoder([entities, indices, weapon_source_1])
        encoded_actions.append(encoded_weapon_source_1)
        # Index 0.
        weapon_index_0_pb = action_pb2.Action()
        text_format.Parse(
            """
            weapon {
                source_id: 1
                index_to_actions {
                    index: 0
                }
            }
            """,
            weapon_index_0_pb)
        weapon_index_0 = parser(packer(weapon_index_0_pb), 1)[0]
        encoded_weapon_index_0 = action_encoder([entities, indices, weapon_index_0])
        encoded_actions.append(encoded_weapon_index_0)

        ## Mulligans.
        # Both discard and keep entity 0.
        mulligan_0s_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {
                discard_entity_ids: 0
                keep_entity_ids: 0
            }
            """, mulligan_0s_pb)
        mulligan_0s = parser(packer(mulligan_0s_pb), 1)[0]
        encoded_mulligan_0s = action_encoder([entities, indices, mulligan_0s])
        encoded_actions.append(encoded_mulligan_0s)
        # Both discard and keep entity 1.
        mulligan_1s_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {
                discard_entity_ids: 1
                keep_entity_ids: 1
            }
            """, mulligan_1s_pb)
        mulligan_1s = parser(packer(mulligan_1s_pb), 1)[0]
        encoded_mulligan_1s = action_encoder([entities, indices, mulligan_1s])
        encoded_actions.append(encoded_mulligan_1s)
        # Discard one entity and keep the other.
        mulligan_mixed_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {
                discard_entity_ids: 0
                keep_entity_ids: 1
            }
            """, mulligan_mixed_pb)
        mulligan_mixed = parser(packer(mulligan_mixed_pb), 1)[0]
        encoded_mulligan_mixed = action_encoder([entities, indices, mulligan_mixed])
        encoded_actions.append(encoded_mulligan_mixed)
        # Discard/keep one entity; keep/discard nothing.
        mulligan_single_discard_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {
                discard_entity_ids: 0
            }
            """, mulligan_single_discard_pb)
        mulligan_single_discard = parser(packer(mulligan_single_discard_pb), 1)[0]
        encoded_mulligan_single_discard = action_encoder([entities, indices, mulligan_single_discard])
        encoded_actions.append(encoded_mulligan_single_discard)
        mulligan_single_keep_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {
                keep_entity_ids: 1
            }
            """, mulligan_single_keep_pb)
        mulligan_single_keep = parser(packer(mulligan_single_keep_pb), 1)[0]
        encoded_mulligan_single_keep = action_encoder([entities, indices, mulligan_single_keep])
        encoded_actions.append(encoded_mulligan_single_keep)
        # Discard/keep none.
        mulligan_empty_pb = action_pb2.Action()
        text_format.Parse("""
            mulligan {}
            """, mulligan_empty_pb)
        mulligan_empty = parser(packer(mulligan_empty_pb), 1)[0]
        encoded_mulligan_empty = action_encoder([entities, indices, mulligan_empty])
        encoded_actions.append(encoded_mulligan_empty)

        # All the above actions must be encoded differently.
        test_util.assert_ndarrays_not_close(*encoded_actions)


if __name__ == "__main__":
    FLAGS(["fake_program_name"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
