from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import concurrent.futures
import multiprocessing
import os
import random

from absl import flags
from absl import logging
import google.protobuf.text_format as text_format
import numpy as np

from ai.config import (
    GET_REPLAY_DIR,
)
from spellsource.proto import (
    simulator_v1_pb2,
)
import spellsource.proto.player_pb2 as player_pb2

FLAGS = flags.FLAGS
flags.DEFINE_integer("thread_pool_size",
                     multiprocessing.cpu_count(),
                     "Number of workers to parallelize gRPC calls across."
                     "Says we should play another gRPC client.")

# Truncate games after this many turns.
MAX_TURNS = 10000


def run_until(
        agent,
        minibatch_size,  # type: int
        opponent_agent,
        get_decks,
        grpc_client,
        gsvb_depth,  # type: int
        opponent_is_gsvb,  # type: bool
        randomize_cards,  # type: bool
        replay_subdir,  # type: Optional[str]
        save_replay_every_n_games,  # type: int
        sess,  # type: tf.train.MonitoredSession
        start_up_to_n_steps_from_end,  # type: int
        stopping_criterion,  # type: Callable[]
):
    # type: (...) -> None

    agents = {
        player_pb2.Player_1: agent,
        player_pb2.Player_2: opponent_agent,
    }

    game_counter = 0
    past_games = collections.deque()
    turn_counters = np.zeros((minibatch_size,), np.int32)
    win_counter = 0

    if stopping_criterion(sess, agent, game_counter, 0):
        return win_counter, game_counter

    game_ids = []
    for i in range(minibatch_size):
        game_ids.append(_start_new_game(
            agents=agents,
            get_decks=get_decks,
            grpc_client=grpc_client,
            gsvb_depth=gsvb_depth,
            i=i,
            opponent_is_gsvb=opponent_is_gsvb,
            sess=sess,
            start_up_to_n_steps_from_end=start_up_to_n_steps_from_end,
            randomize_cards=randomize_cards,
        )[0])

    # Play until we hit the step limit.
    # NOTE: The first agent is always the one we care about (either evaluating or training)
    # so we run until it has achieved the requisite number of steps.
    while not stopping_criterion(sess, agent, game_counter,
                                 _get_win_ratio(past_games)):
        (
            observations,
            valid_actions,
            game_ids,
            current_players,
            game_counter,
            past_games,
            turn_counters,
            win_counter,
        ) = _get_observations(
            agents=agents,
            game_counter=game_counter,
            game_ids=game_ids,
            get_decks=get_decks,
            grpc_client=grpc_client,
            gsvb_depth=gsvb_depth,
            past_games=past_games,
            opponent_is_gsvb=opponent_is_gsvb,
            randomize_cards=randomize_cards,
            replay_subdir=replay_subdir,
            save_replay_every_n_games=save_replay_every_n_games,
            sess=sess,
            start_up_to_n_steps_from_end=start_up_to_n_steps_from_end,
            turn_counters=turn_counters,
            win_counter=win_counter,
        )

        _apply_actions(
            agents=agents,
            current_players=current_players,
            game_ids=game_ids,
            grpc_client=grpc_client,
            observations=observations,
            opponent_is_gsvb=opponent_is_gsvb,
            sess=sess,
            valid_actions=valid_actions,
        )

        turn_counters += 1

    return win_counter, game_counter


def _apply_action(
        action,
        current_player,
        game_id,
        grpc_client,
):
    grpc_client.GameActionApply(simulator_v1_pb2.GameActionApplyRequestV1(
        game_id=game_id,
        player=current_player,
        action=action,
    ))


def _apply_actions(
        agents,
        current_players,
        game_ids,
        grpc_client,
        observations,
        opponent_is_gsvb,
        sess,
        valid_actions,
):
    p1_game_indices_to_obs_indices = {}
    p1_observations = []
    p1_valid_actions = []
    p2_game_indices_to_obs_indices = {}
    p2_observations = []
    p2_valid_actions = []
    for i in range(len(game_ids)):
        if current_players[i] == player_pb2.Player_1:
            p1_game_indices_to_obs_indices[i] = len(p1_observations)
            p1_observations.append(observations[i])
            p1_valid_actions.append(valid_actions[i])
        elif current_players[i] == player_pb2.Player_2:
            p2_game_indices_to_obs_indices[i] = len(p2_observations)
            p2_observations.append(observations[i])
            p2_valid_actions.append(valid_actions[i])
        else:
            raise Exception(
                "Cannot _apply_actions if current player is '{}'.".format(current_players[i]))

    logging.debug("Getting player 1 actions.")
    p1_actions = agents[player_pb2.Player_1].actions_for_observations(
        observations=p1_observations,
        monitored_sess=sess,
        valid_actions=p1_valid_actions,
        game_indices=sorted(p1_game_indices_to_obs_indices.keys()),
    )
    if not opponent_is_gsvb:
        logging.debug("Getting player 2 actions.")
        p2_actions = agents[player_pb2.Player_2].actions_for_observations(
            observations=p2_observations,
            monitored_sess=sess,
            valid_actions=p2_valid_actions,
            game_indices=sorted(p2_game_indices_to_obs_indices.keys()),
        )

    futures = []
    for i in range(len(game_ids)):
        if current_players[i] == player_pb2.Player_1:
            action = p1_actions[p1_game_indices_to_obs_indices[i]]
        else:
            action = p2_actions[p2_game_indices_to_obs_indices[i]]

        futures.append(_get_thread_pool().submit(
            _apply_action,
            action,
            current_players[i],
            game_ids[i],
            grpc_client,
        ))

    for future in futures:
        future.result()


def _end_game(
        agents,
        game_id,
        grpc_client,
        i,
        replay_subdir,
        save_replay_every_n_games,
        sess,
        winner,
):
    should_save_replay = False
    if save_replay_every_n_games:
        should_save_replay = random.random() < float(1) / save_replay_every_n_games
    if should_save_replay:
        replay_dir = GET_REPLAY_DIR()
        if replay_subdir != None:
            replay_dir = os.path.join(GET_REPLAY_DIR(), replay_subdir)
        if not os.path.exists(replay_dir):
            os.makedirs(replay_dir)

        replay_response = [response_chunk for response_chunk in grpc_client.GameReplayCreate(
            simulator_v1_pb2.GameReplayCreateRequestV1(game_id=game_id))]
        replay_str = "".join(r.replay_chunk for r in replay_response[:-1])
        partial_replay_proto = replay_response[-1]

        global_step = agents[player_pb2.Player_1].global_step(sess)
        if winner == player_pb2.Player_1:
            wl_str = "win"
        elif winner == player_pb2.Player_2:
            wl_str = "loss"
        elif winner == player_pb2.Player_NEITHER:
            wl_str = "double_loss"
        else:
            raise Exception("Unexpected game winner '{}'".format(winner))

        replay_file_path = os.path.join(replay_dir,
                                        "{}.{}.{}.yayson".format(global_step, wl_str, game_id))
        partial_replay_file_path = os.path.join(replay_dir,
                                                "{}.{}.{}.proto".format(global_step, wl_str, game_id))
        if os.path.exists(replay_file_path) or os.path.exists(partial_replay_file_path):
            raise Exception("Cowardlily refusing to overwrite existing replay '{}'.".format(
                replay_file_path))
        with open(replay_file_path, "w") as f:
            f.write(replay_str)
        with open(partial_replay_file_path, "w") as f:
            f.write(text_format.MessageToString(partial_replay_proto.partial_replay))

        logging.debug("Recorded replay at {}.".format(replay_file_path))

    agents[player_pb2.Player_1].end_game(sess, winner == player_pb2.Player_1, i)
    agents[player_pb2.Player_2].end_game(sess, winner == player_pb2.Player_2, i)
    grpc_client.GameDelete(simulator_v1_pb2.GameDeleteRequestV1(game_id=game_id))


def _get_current_player(grpc_client, game_id):
    return grpc_client.GameActivePlayerGet(simulator_v1_pb2.GameActivePlayerGetRequestV1(
        game_id=game_id,
    )).current_player


def _get_observation(
        agents,
        game_id,
        get_decks,
        grpc_client,
        gsvb_depth,
        i,
        opponent_is_gsvb,
        randomize_cards,
        replay_subdir,
        save_replay_every_n_games,
        sess,
        start_up_to_n_steps_from_end,
        turn_counter,
):
    current_player = _get_current_player(grpc_client, game_id)
    n_games_completed = 0
    n_wins = 0
    reset_turn_counter = False

    obs_response = grpc_client.GameObservationGet(
        simulator_v1_pb2.GameObservationGetRequestV1(
            game_id=game_id,
            player=current_player))

    # Start any new games if necessary.
    while obs_response.done:
        logging.info("Finished game in {} turns. Player {} won.".format(
            turn_counter, obs_response.winner))

        _end_game(
            agents=agents,
            game_id=game_id,
            grpc_client=grpc_client,
            i=i,
            replay_subdir=replay_subdir,
            save_replay_every_n_games=save_replay_every_n_games,
            sess=sess,
            winner=obs_response.winner,
        )
        n_games_completed += 1

        if obs_response.winner == player_pb2.Player_1:
            n_wins += 1

        game_id, current_player = _start_new_game(
            agents=agents,
            get_decks=get_decks,
            grpc_client=grpc_client,
            gsvb_depth=gsvb_depth,
            i=i,
            opponent_is_gsvb=opponent_is_gsvb,
            sess=sess,
            start_up_to_n_steps_from_end=start_up_to_n_steps_from_end,
            randomize_cards=randomize_cards,
        )
        reset_turn_counter = True
        obs_response = grpc_client.GameObservationGet(
            simulator_v1_pb2.GameObservationGetRequestV1(
                game_id=game_id,
                player=current_player))

    if turn_counter > MAX_TURNS:
        logging.warn("Truncating game after {} turns.".format(turn_counter))
        _end_game(
            agents=agents,
            game_id=game_id,
            grpc_client=grpc_client,
            i=i,
            replay_subdir=replay_subdir,
            save_replay_every_n_games=save_replay_every_n_games,
            sess=sess,
            winner=None,
        )
        n_games_completed += 1

        game_id, current_player = _start_new_game(
            agents=agents,
            get_decks=get_decks,
            grpc_client=grpc_client,
            gsvb_depth=gsvb_depth,
            i=i,
            opponent_is_gsvb=opponent_is_gsvb,
            sess=sess,
            start_up_to_n_steps_from_end=start_up_to_n_steps_from_end,
            randomize_cards=randomize_cards,
        )
        reset_turn_counter = True
        obs_response = grpc_client.GameObservationGet(
            simulator_v1_pb2.GameObservationGetRequestV1(
                game_id=game_id,
                player=current_player))

    return (
        obs_response.observation,
        obs_response.valid_action,
        game_id,
        current_player,
        n_games_completed,
        n_wins,
        reset_turn_counter,
    )


def _get_observations(
        agents,
        game_counter,
        game_ids,
        get_decks,
        grpc_client,
        gsvb_depth,
        past_games,
        opponent_is_gsvb,
        randomize_cards,
        replay_subdir,
        save_replay_every_n_games,
        sess,
        start_up_to_n_steps_from_end,
        turn_counters,
        win_counter,
):
    """
    Get next observation. This includes restarting the game if necessary and then getting an
    observation in the new game.
    """

    futures = []
    for i in range(len(game_ids)):
        futures.append(_get_thread_pool().submit(
            _get_observation,
            agents,
            game_ids[i],
            get_decks,
            grpc_client,
            gsvb_depth,
            i,
            opponent_is_gsvb,
            randomize_cards,
            replay_subdir,
            save_replay_every_n_games,
            sess,
            start_up_to_n_steps_from_end,
            turn_counters[i],
        ))

    current_players = []
    observations = []
    valid_actions = []
    for i in range(len(game_ids)):
        (
            observation,
            valid_action,
            game_ids[i] ,
            current_player,
            n_games_completed,
            n_wins,
            reset_turn_counter,
        ) = futures[i].result()

        observations.append(observation)
        valid_actions.append(valid_action)
        current_players.append(current_player)
        game_counter += n_games_completed
        win_counter += n_wins

        for j in range(n_games_completed):
            if j < n_wins:
                past_games.append(True)
            else:
                past_games.append(False)
        while len(past_games) > 200:
            past_games.popleft()

        if reset_turn_counter:
            turn_counters[i] = 0

    return (
        observations,
        valid_actions,
        game_ids,
        current_players,
        game_counter,
        past_games,
        turn_counters,
        win_counter,
    )


__thread_pool = None
def _get_thread_pool():
    global __thread_pool
    if __thread_pool == None:
        __thread_pool = concurrent.futures.ThreadPoolExecutor(max_workers=FLAGS.thread_pool_size)
    return __thread_pool


def _get_win_ratio(past_games):
    if len(past_games) == 0:
        return 0
    else:
        return sum(int(e) for e in past_games) / len(past_games)


def _start_new_game(
        agents,
        get_decks,
        grpc_client,
        gsvb_depth,
        i,
        opponent_is_gsvb,
        sess,
        start_up_to_n_steps_from_end,
        randomize_cards,
):
    logging.debug("Starting new game.")
    deck1, deck2 = get_decks()
    if start_up_to_n_steps_from_end > 0:
        start_n_steps_from_end = random.randint(5, start_up_to_n_steps_from_end)
    else:
        start_n_steps_from_end = 0

    response = grpc_client.GameCreate(simulator_v1_pb2.GameCreateRequestV1(
        deck1=deck1,
        deck2=deck2,
        player1_name=agents[player_pb2.Player_1].get_name(i),
        player2_name=agents[player_pb2.Player_2].get_name(i),
        player2_gsvb=opponent_is_gsvb,
        gsvb_depth=gsvb_depth,
        randomize_cards=randomize_cards,
        start_n_steps_from_end=start_n_steps_from_end,
    ))
    game_id = response.game_id
    agents[player_pb2.Player_1].start_new_game(i, deck1, deck2)
    agents[player_pb2.Player_2].start_new_game(i, deck2, deck1)

    return game_id, _get_current_player(grpc_client, game_id)
