from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import itertools
import sys
# NOTE: Read on for why we do this absolutely disgusting shit.
#
# We want to use the `py_proto_library` Skylark rule from `com_google_protobuf`. But that ends up
# putting the protobuf package provided by `com_goole_protobuf` on the Python path. Which is a pure
# Python (read: slow af) implementation, which we want to avoid like the plague.
#
# If there is ever a more blessed rule than
# https://github.com/protocolbuffers/protobuf/blob/52fee0e8f03a56931a38e63c2b2ac2ba91191742/protobuf.bzl#L364
# then I think that we can get rid of this.
sys.path = [p for p in sys.path if "com_google_protobuf" not in p]

import cPickle as pickle
import os

from absl import app as absl_app
from absl import flags
from absl import logging
import tensorboard.plugins.beholder as beholder_lib
import grpc
import tensorflow as tf

from ai.agent.a3c.agent import (
    A3cAgent,
)
from ai.agent.heuristic import (
    HeuristicAgent,
)
from ai.agent.meta import (
    MetaAgent,
)
from ai.agent.random_agent import (
    RandomAgent,
)
from ai.agent.gsvb_stub import (
    GsvbStubAgent,
)
from ai.config import (
    GET_SUMMARY_DIR,
)
from spellsource.proto import (
    simulator_v1_pb2,
)
from spellsource.proto import (
    simulator_v1_pb2_grpc,
)
import ai.katib_client as katib_client
import ai.mlflow_wrapper as mlflow_wrapper
import ai.run_loop
import ai.saver
import ai.util
import base.flags
import deck.decklists as decklists
import deck.util
import spellsource.proto.player_pb2 as player_pb2

FLAGS = flags.FLAGS
flags.DEFINE_enum("agent",
                  A3cAgent.__name__,
                  [a.__name__ for a in [A3cAgent, HeuristicAgent, RandomAgent]],
                  "Again to play.")
flags.DEFINE_bool("eval", False, "Whether or not to run evaluation.")
flags.DEFINE_bool("odd_paladin_only",
                  False,
                  "Whether or not to play only Odd Paladin during training.")
flags.DEFINE_bool("randomize_eval_cards", False, "")
flags.DEFINE_bool("randomize_train_cards", False, "")
flags.DEFINE_bool("terminate_spellsource_on_finish",
                  False,
                  "If true, terminate Spellsource server upon successful excecution.")
flags.DEFINE_bool("train", False, "Whether or not to run training.")
flags.DEFINE_enum("deck", None, deck.util.DECKLIST_NAMES, "Deck to use.")
flags.DEFINE_enum("eval_opponent",
                  GsvbStubAgent.__name__,
                  [a.__name__ for a in [GsvbStubAgent, HeuristicAgent, RandomAgent]],
                  "Opponent to train against. Note this flag is superceeded by gsvb_depth.")
flags.DEFINE_enum("opponent_deck", None, deck.util.DECKLIST_NAMES, "Opponent deck to use.")
flags.DEFINE_enum("opponent",
                  HeuristicAgent.__name__,
                  [a.__name__ for a in [A3cAgent, GsvbStubAgent, HeuristicAgent, RandomAgent]],
                  "Opponent to train against. Note this flag is superceeded by gsvb_depth.")
flags.DEFINE_float("epoch_termination_win_ratio",
                   None,
                   "Win rate at which we end epochs. Set to enable self play.")
flags.DEFINE_float("tf_per_process_gpu_memory_fraction", 0.4, "")
flags.DEFINE_integer("epoch_min_games", 200, "Minimal number of games to train an epoch.")
flags.DEFINE_integer("epoch_n_stationary_opponents", 3, "Keep this many prior epochs as opponents.")
flags.DEFINE_integer("save_replay_every_n_eval_games",
                     None,
                     "How often to save replays during evaluation.")
flags.DEFINE_integer("save_replay_every_n_train_games",
                     None,
                     "How often to save replays during training.")
flags.DEFINE_integer("simulator_port", 50051, "Port that Spellsource simulator is listening on.")
flags.DEFINE_integer("start_up_to_n_steps_from_end_train", 0, "")
flags.DEFINE_integer("start_up_to_n_steps_from_end_eval", 0, "")
flags.DEFINE_integer("train_n_steps", 20000, "Minimal number of _global steps_ to train for.")
flags.DEFINE_integer("eval_n_games", 50, "Number of _games_ to evaluate for.")
flags.DEFINE_list("beholder_vars", [], "(fragmented) Variable names to capture in Beholder.")
flags.DEFINE_string("agent_checkpoint", None,
                    "Checkpoint file from which to restore agent.")
flags.DEFINE_string("simulator_host", "localhost", "Host running Spellsource simulator to use.")


def _create_opponent(protagonist_agent):
    if FLAGS.epoch_termination_win_ratio != None:
        def opponent_to_agent_var_name(i, opponent_var_name):
            relative_name = opponent_var_name[len("opponent_agent{}/".format(i)):]
            agent_name = "agent/{}".format(relative_name)
            # NOTE: I don't understand why this is necessary.
            if agent_name.endswith(":0"):
                agent_name = agent_name[:-2]
            return agent_name

        opponent_game_counts = FLAGS.epoch_n_stationary_opponents * [
            int(FLAGS.minibatch_size / 4 / (FLAGS.epoch_n_stationary_opponents - 1))
        ]
        opponent_game_counts[0] = FLAGS.minibatch_size - sum(opponent_game_counts[1:])
        opponent_game_distribution = [float(i) / FLAGS.minibatch_size for i in opponent_game_counts]
        logging.info("Using opponent distribution {}".format(opponent_game_distribution))

        opponent_agents = []
        opponent_epoch_savers = []
        for i in range(FLAGS.epoch_n_stationary_opponents):
            with tf.variable_scope("opponent_agent{}".format(i)):
                game_count = opponent_game_counts[i]
                opponent_agent = globals()[FLAGS.opponent](game_count, FLAGS.beholder_vars, "predict_only",
                                                           player_pb2.Player_2)
                opponent_agents.append(opponent_agent)

                opponent_saver = ai.saver.FastSaver(
                    var_list={
                        opponent_to_agent_var_name(i, v.name): v for v in opponent_agent.get_trainable_vars()
                    },
                )
                opponent_epoch_savers.append(opponent_saver)
        opponent_agent = MetaAgent(opponent_agents, opponent_game_counts)

        # We only measure trainable variable distance from nearest opponent.
        protagonist_agent.register_opponent_trainable_vars(opponent_agents[0].get_trainable_vars())

    else:
        with tf.variable_scope("opponent_agent"):
            opponent_agent = globals()[FLAGS.opponent](
                FLAGS.minibatch_size, FLAGS.beholder_vars, "predict_only", player_pb2.Player_2)
            opponent_epoch_savers = []

    return opponent_agent, opponent_epoch_savers


def _find_best_epoch_ckpts(path_prefix):
    dir_ = os.path.dirname(path_prefix)
    filename_prefix = os.path.basename(path_prefix)

    win_percents_and_epock_ckpts = []
    for entry in os.listdir(dir_):
        if entry.startswith(filename_prefix) and entry.endswith(".index"):
            base_name = entry[:-len(".index")]
            end_of_file_name = base_name[len("epoch_"):].split("_", 1)[1]
            win_percent = int(end_of_file_name.split(".", 1)[0])
            win_percents_and_epock_ckpts.append((win_percent, os.path.join(dir_, base_name)))

    best_epoch_ckpts = sorted(win_percents_and_epock_ckpts,
                              reverse=True)[:FLAGS.epoch_n_stationary_opponents-1]
    return best_epoch_ckpts


def _find_current_epoch_ckpt(path_prefix):
    dir_ = os.path.dirname(path_prefix)
    filename_prefix = os.path.basename(path_prefix)

    epoch_ckpt = None
    epoch = -1
    for entry in os.listdir(dir_):
        if entry.startswith(filename_prefix) and entry.endswith(".index"):
            base_name = entry[:-len(".index")]
            curr_epoch = int(base_name[len("epoch_"):].split(".", 1)[0])
            if curr_epoch > epoch:
                epoch = curr_epoch
                epoch_ckpt = os.path.join(dir_, base_name)

    return (epoch, epoch_ckpt)


def _run_epochs(
        agent,
        minibatch_size,
        best_saver,
        deck_,
        epoch_saver,
        eval_opponent_agent,
        grpc_client,
        gsvb_depth,
        odd_paladin_only,
        opponent_agent,
        opponent_deck,
        opponent_epoch_savers,
        output_dir,
        save_replay_every_n_games,
        sess,
        start_up_to_n_steps_from_end,
        tf_beholder,
        train_n_steps,
):
    epoch_number = 0
    max_epoch_wr = 0

    if FLAGS.epoch_termination_win_ratio != None:
        assert type(opponent_agent) == MetaAgent
        if FLAGS.epoch_termination_win_ratio <= 0.5:
            raise Exception("Must set `epoch_termination_win_ratio` to value above 0.5.")

        best_ckpt_dir =os.path.join(output_dir, "best_checkpoints")
        if not os.path.exists(best_ckpt_dir):
            os.makedirs(best_ckpt_dir)
        epoch_ckpt_dir =os.path.join(output_dir, "epoch_checkpoints")
        if not os.path.exists(epoch_ckpt_dir):
            os.makedirs(epoch_ckpt_dir)
        epoch_ckpt_path_tmpl = os.path.join(epoch_ckpt_dir, "epoch_{}.ckpt")
        epoch_ckpt_path_prefix = os.path.join(epoch_ckpt_dir, "epoch_")
        best_epoch_ckpt_path_tmpl = os.path.join(best_ckpt_dir, "epoch_{}_{}.ckpt")
        best_epoch_ckpt_path_prefix = os.path.join(best_ckpt_dir, "epoch_")
        _, curr_epoch_ckpt = _find_current_epoch_ckpt(epoch_ckpt_path_prefix)
        if curr_epoch_ckpt == None:
            logging.info("Seeding initial epoch weights.")
            ckpt_path = epoch_ckpt_path_tmpl.format(0)
            global_step = agent.global_step(sess)
            if FLAGS.agent == A3cAgent.__name__:
                def step_fn(step_context):
                    epoch_saver.save(step_context.session, ckpt_path, global_step=global_step)
                sess.run_step_fn(step_fn)
            mlflow_wrapper.log_metric("epoch", 0)

    def stopping_criterion(sess, agent, game_counter, win_ratio):
        global_step = agent.global_step(sess)
        logging.debug(
            "`stopping_criterion` called after {} games, {} global steps with win ratio {}.".format(
                game_counter, global_step, win_ratio))
        if global_step >= train_n_steps:
            return True
        # If self play is enabled we actually terminate at the end of each epoch.
        if FLAGS.epoch_termination_win_ratio != None:
            if (game_counter >= FLAGS.epoch_min_games
                    and win_ratio > FLAGS.epoch_termination_win_ratio):
                return True
        return False

    global_step = agent.global_step(sess)
    agent.start_training(sess=sess, beholder=tf_beholder)
    while global_step < FLAGS.train_n_steps:

        if FLAGS.epoch_termination_win_ratio != None:
            epoch_number, curr_epoch_ckpt = _find_current_epoch_ckpt(epoch_ckpt_path_prefix)
            if curr_epoch_ckpt == None:
                raise Exception("No checkpoints with path prefix '{}'.".format(
                    epoch_ckpt_path_tmpl.format(epoch_number)))

            logging.info("Loading stationary opponents for self play.")
            best_epoch_ckpts = _find_best_epoch_ckpts(best_epoch_ckpt_path_prefix)
            ckpt_paths = itertools.cycle([curr_epoch_ckpt] + [p for _, p in best_epoch_ckpts])
            for opponent_epoch_saver, ckpt_path in zip(opponent_epoch_savers, ckpt_paths):
                def step_fn(step_context):
                    opponent_epoch_saver.restore(step_context.session, ckpt_path)
                sess.run_step_fn(step_fn)

        win_counter, game_counter = ai.run_loop.run_until(
            agent=agent,
            minibatch_size=minibatch_size,
            opponent_agent=opponent_agent,
            get_decks=deck.util.create_get_decks_train(deck_, opponent_deck, odd_paladin_only),
            grpc_client=grpc_client,
            gsvb_depth=gsvb_depth,
            opponent_is_gsvb=FLAGS.opponent == GsvbStubAgent.__name__,
            randomize_cards=FLAGS.randomize_train_cards,
            replay_subdir=None,
            save_replay_every_n_games=save_replay_every_n_games,
            sess=sess,
            start_up_to_n_steps_from_end=start_up_to_n_steps_from_end,
            stopping_criterion=stopping_criterion)
        agent.flush_training_queue(sess)
        agent.clear()
        opponent_agent.clear()
        global_step = agent.global_step(sess)

        if FLAGS.epoch_termination_win_ratio != None:
            ckpt_path = epoch_ckpt_path_tmpl.format(epoch_number + 1)
            logging.info(
                "Epoch {} finished, saving checkpoint to {}.".format(epoch_number + 1, ckpt_path))
            def step_fn(step_context):
                epoch_saver.save(step_context.session, ckpt_path, global_step=global_step)
            sess.run_step_fn(step_fn)

            logging.info("Evaluating epoch {}...".format(epoch_number + 1))
            agent.pause_training(sess)
            def eval_stopping_criterion(sess, agent, game_counter, win_ratio):
                return game_counter >= FLAGS.eval_n_games

            get_decks = deck.util.create_get_decks_eval(FLAGS.deck, FLAGS.opponent_deck)
            n_wins, n_games = ai.run_loop.run_until(
                agent=agent,
                minibatch_size=FLAGS.minibatch_size,
                opponent_agent=eval_opponent_agent,
                get_decks=get_decks,
                grpc_client=grpc_client,
                gsvb_depth=FLAGS.gsvb_depth,
                opponent_is_gsvb=FLAGS.eval_opponent == GsvbStubAgent.__name__,
                randomize_cards=FLAGS.randomize_eval_cards,
                replay_subdir = "eval-{}-{}".format(epoch_number + 1, global_step),
                save_replay_every_n_games=FLAGS.save_replay_every_n_eval_games,
                sess=sess,
                stopping_criterion=eval_stopping_criterion)

            win_ratio = float(n_wins) / n_games
            logging.info("Epoch {} winrate: {}.".format(epoch_number + 1, win_ratio))
            eval_summary = tf.Summary()
            eval_summary.value.add(tag="episode/win_ratio_eval", simple_value=win_ratio)
            summary_writer = tf.summary.FileWriterCache.get(GET_SUMMARY_DIR())
            summary_writer.add_summary(eval_summary, global_step=global_step)
            summary_writer.flush()

            mlflow_wrapper.log_metric("wr_epoch", win_ratio)
            max_epoch_wr = max(max_epoch_wr, win_ratio)
            mlflow_wrapper.log_metric("wr_epoch_max", max_epoch_wr)

            worst_best_win_percent = -1
            best_epoch_ckpts = _find_best_epoch_ckpts(best_epoch_ckpt_path_prefix)
            if len(best_epoch_ckpts) > 0:
                worst_best_win_percent, _ = best_epoch_ckpts[-1]
            win_percent = int(win_ratio * 100)
            if (win_percent > worst_best_win_percent
                    or len(best_epoch_ckpts) < FLAGS.epoch_n_stationary_opponents - 1):
                logging.info("Saving high win rate epoch {} (winrate {}).".format(epoch_number + 1,
                                                                                  win_ratio))
                ckpt_path = best_epoch_ckpt_path_tmpl.format(epoch_number + 1, win_percent)
                def step_fn(step_context):
                    best_saver.save(step_context.session, ckpt_path, global_step=global_step)
                sess.run_step_fn(step_fn)

            agent.resume_training()
            mlflow_wrapper.log_metric("epoch", epoch_number + 1)
    agent.stop_training(sess)


def _tf_setup_beholder():
    beholder = beholder_lib.Beholder(logdir=FLAGS.output_dir)

    beholder_cfg_path = os.path.join(FLAGS.output_dir, "plugins", "beholder", "config.pkl")
    with open(beholder_cfg_path) as f:
        beholder_cfg = pickle.load(f)

    beholder_cfg["FPS"] = 0
    if len(FLAGS.beholder_vars) > 0:
        beholder_cfg["FPS"] = 1
    beholder_cfg["values"] = "arrays"
    logging.debug("Using beholder config '{}'".format(beholder_cfg))
    with open(beholder_cfg_path, "w") as f:
        pickle.dump(beholder_cfg, f)

    return beholder


def _tf_setup_session(
        ready_for_local_init_op,
        ready_op,
        saver,
):
    """
    MUST be run after setting up any Agents that use Tensorflow.
    """
    logging.info("Setting up TF session.")

    # NOTE: We do this so that no matter what at least the global step variable exists (this makes
    # it so Tensorflow doesn't complain even when we aren't doing Tensorflow things at all).
    global_step = tf.train.get_or_create_global_step()

    logging.debug("Trainable vars:")
    for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, tf.get_variable_scope().name):
        logging.debug("  %s %s", v.name, v.get_shape())

    with tf.variable_scope("monitored_training_session"):
        return tf.train.MonitoredTrainingSession(
            checkpoint_dir=os.path.join(FLAGS.output_dir, "checkpoints"),
            config=tf.ConfigProto(
                allow_soft_placement=True,
                gpu_options=tf.GPUOptions(
                    #force_gpu_compatible=True,
                    per_process_gpu_memory_fraction=FLAGS.tf_per_process_gpu_memory_fraction,
                ),
            ),
            scaffold=tf.train.Scaffold(
                ready_for_local_init_op=ready_for_local_init_op,
                ready_op=ready_op,
                saver=saver,
            ),
            save_checkpoint_secs=60 if FLAGS.sanity_check else 900,
            save_summaries_secs=5 if FLAGS.sanity_check else 60,
            summary_dir=GET_SUMMARY_DIR(),
        )


def main(argv):
    # type: (...) -> None
    del argv

    if FLAGS.epoch_termination_win_ratio:
        assert FLAGS.opponent == A3cAgent.__name__
    if FLAGS.agent != A3cAgent.__name__:
        assert not FLAGS.train

    # Configure Abseil logging-to-directory if it was requested.
    if FLAGS.log_dir:
        if not os.path.exists(FLAGS.log_dir):
            os.makedirs(FLAGS.log_dir)
        logging.get_absl_handler().use_absl_log_file("main", FLAGS.log_dir)

    # Create output directory if necessary.
    if not os.path.exists(FLAGS.output_dir):
        os.makedirs(FLAGS.output_dir)

    # Record flag file for easy re-running.
    flag_file_path = os.path.join(FLAGS.output_dir, "flags")
    if not os.path.exists(flag_file_path):
        with open(flag_file_path, "w") as f:
            f.write(base.flags.get_flags_string())
    else:
        logging.info("Flag file '{}' already exists; won't overwrite.".format(flag_file_path))

    # Connect to simulator GRPC service.
    channel = grpc.insecure_channel("{}:{}".format(FLAGS.simulator_host, FLAGS.simulator_port))
    grpc_client = simulator_v1_pb2_grpc.SimulatorV1Stub(channel)

    try:
        # Set up agent.
        if FLAGS.opponent == GsvbStubAgent.__name__ or FLAGS.eval_opponent == GsvbStubAgent.__name__:
            assert FLAGS.gsvb_depth > 0
        with tf.variable_scope("agent"):
            agent = globals()[FLAGS.agent](FLAGS.minibatch_size, FLAGS.beholder_vars,
                                           "predict_or_train", player_pb2.Player_1)

            if FLAGS.agent == A3cAgent.__name__:
                saver = ai.saver.FastSaver(
                    save_relative_paths=True,
                )
                best_saver = ai.saver.FastSaver(
                    max_to_keep=None,
                    save_relative_paths=True,
                )
                epoch_saver = ai.saver.FastSaver(
                    max_to_keep=1,
                    save_relative_paths=True,
                )
            else:
                saver = None
                best_saver = None
                epoch_saver = None

            agent.register_error_saver(best_saver)

        # Set up evaluation agent.
        eval_opponent_agent = globals()[FLAGS.eval_opponent](FLAGS.minibatch_size, FLAGS.beholder_vars,
                                                             "predict_only", player_pb2.Player_2)

        tf_beholder = _tf_setup_beholder()

        # Set up opponent agent.
        # NOTE: This should come after all other TF variables have been created so that we can correctly
        # set the `ready_for_local_init_op` and `ready_op` to ignore "opponent_agent" variables (they
        # will be loaded from a checkpoint file).
        ready_for_local_init_op = tf.report_uninitialized_variables(tf.global_variables())
        ready_op = tf.report_uninitialized_variables()

        opponent_agent, opponent_epoch_savers = _create_opponent(agent)

        if FLAGS.epoch_termination_win_ratio != None:
            logging.info("Marking stationary agent's variables untrainable.")
            trainable_variables = tf.get_collection_ref(tf.GraphKeys.TRAINABLE_VARIABLES)
            for opponent_trainable_var in opponent_agent.get_trainable_vars():
                idx = trainable_variables.index(opponent_trainable_var)
                del trainable_variables[idx]

        # Finalize TF graph and set up session.
        tf_monitored_training_session = _tf_setup_session(ready_for_local_init_op, ready_op, saver)

        with mlflow_wrapper.run(), tf_monitored_training_session as sess:
            if FLAGS.train:
                if FLAGS.agent_checkpoint != None:
                    raise Exception("'agent_checkpoint' flag is not valid when training.")

                base_profile_path = os.path.join(FLAGS.output_dir, "train-inference")
                with ai.util.maybe_profile(base_log_path=base_profile_path):
                    _run_epochs(
                        agent=agent,
                        minibatch_size=FLAGS.minibatch_size,
                        best_saver=best_saver,
                        deck_=FLAGS.deck,
                        epoch_saver=epoch_saver,
                        eval_opponent_agent=eval_opponent_agent,
                        grpc_client=grpc_client,
                        gsvb_depth=FLAGS.gsvb_depth,
                        odd_paladin_only=FLAGS.odd_paladin_only,
                        opponent_agent=opponent_agent,
                        opponent_deck=FLAGS.opponent_deck,
                        opponent_epoch_savers=opponent_epoch_savers,
                        output_dir=FLAGS.output_dir,
                        save_replay_every_n_games=FLAGS.save_replay_every_n_train_games,
                        sess=sess,
                        start_up_to_n_steps_from_end=FLAGS.start_up_to_n_steps_from_end_train,
                        tf_beholder=tf_beholder,
                        train_n_steps=FLAGS.train_n_steps,
                    )

                # Flush checkpoint to disk so that we can <ctrl>-c during eval without losing results.
                if FLAGS.agent == A3cAgent.__name__:
                    global_step = agent.global_step(sess)
                    def step_fn(step_context):
                        dest = os.path.join(FLAGS.output_dir, "checkpoints", "model.ckpt")
                        logging.info("Saving checkpoints for {} into {}.".format(global_step, dest))
                        saver.save(step_context.session, dest, global_step=global_step)
                    sess.run_step_fn(step_fn)

            if FLAGS.eval:
                logging.info("Starting evaluation.")
                replay_subdir = "eval"
                if FLAGS.agent_checkpoint != None:
                    replay_subdir = "eval-{}".format(os.path.basename(FLAGS.agent_checkpoint))
                    if FLAGS.agent == A3cAgent.__name__:
                        def step_fn(step_context):
                            logging.info(
                                "Restoring checkpoints for evaluation from {}.".format(
                                    FLAGS.agent_checkpoint))
                            saver.restore(step_context.session, FLAGS.agent_checkpoint)
                        sess.run_step_fn(step_fn)

                def stopping_criterion(sess, agent, game_counter, win_ratio):
                    return game_counter >= FLAGS.eval_n_games

                get_decks = deck.util.create_get_decks_eval(FLAGS.deck, FLAGS.opponent_deck)
                deck1, deck2 = get_decks()
                base_profile_path = os.path.join(FLAGS.output_dir, "eval-inference")
                with ai.util.maybe_profile(base_log_path=base_profile_path):
                    n_wins, n_games = ai.run_loop.run_until(
                        agent=agent,
                        minibatch_size=FLAGS.minibatch_size,
                        opponent_agent=eval_opponent_agent,
                        get_decks=get_decks,
                        grpc_client=grpc_client,
                        gsvb_depth=FLAGS.gsvb_depth,
                        opponent_is_gsvb=FLAGS.eval_opponent == GsvbStubAgent.__name__,
                        randomize_cards=FLAGS.randomize_eval_cards,
                        replay_subdir=replay_subdir,
                        save_replay_every_n_games=FLAGS.save_replay_every_n_eval_games,
                        sess=sess,
                        start_up_to_n_steps_from_end=FLAGS.start_up_to_n_steps_from_end_eval,
                        stopping_criterion=stopping_criterion)

                logging.info("Won {} / {} games during evaluation.".format(n_wins, n_games))
                win_ratio = float(n_wins)/n_games
                logging.info("Won {} % during evaluation.".format(win_ratio * 100))
                eval_metric_name = "wre_{}_{}".format(decklists.get_sanitized_name(deck1),
                                                      decklists.get_sanitized_name(deck2))
                mlflow_wrapper.log_metric(eval_metric_name, win_ratio)
                katib_client.log_metric(eval_metric_name, win_ratio)

    finally:
        if FLAGS.terminate_spellsource_on_finish:
            try:
                grpc_client.Shutdown(simulator_v1_pb2.Empty())
            except grpc._channel._Rendezvous:
                # `Shutdown` violently kills the server; so this final call is expected to end with the
                # server puking.
                pass


if __name__ == "__main__":
    try:
        absl_app.run(main)
    except SystemExit as e:
        if e.code != None:
            logging.exception("hai terminated with non-zero exit code {}.".format(e.code))
            katib_client.log_error()
    except:
        logging.exception("hai terminated with an exception.")
        katib_client.log_error()
