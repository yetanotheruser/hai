from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import contextlib
import os
import threading

from absl import flags
from absl import logging
import mlflow

import base.buildinfo.buildinfo as buildinfo

FLAGS = flags.FLAGS
flags.DEFINE_string("mlflow_experiment",
                    "hai_default",
                    "Name of the MLFlow experiment to log this in.")
flags.DEFINE_string("mlflow_run",
                    None,
                    "Name of the MLFlow experiment to log this in.")
flags.DEFINE_string("mlflow_tracking_uri",
                    "",
                    "Name of the MLFlow experiment to log this in.")

def GET_MLFLOW_UUID_FILE():
    return os.path.join(FLAGS.output_dir, "run_uuid.mlflow")
MLFLOW_LOCK = threading.Lock()
ABSL_MODULE_PREFIX = "absl"

def _harvest_flags():
    modules_to_harvest_flags_of = []
    for module in FLAGS.__flags_by_module:
        if not module.startswith(ABSL_MODULE_PREFIX):
            modules_to_harvest_flags_of.append(module)

    flags = {}
    for m in modules_to_harvest_flags_of:
        for flag in FLAGS.__flags_by_module[m]:
            flags[flag.name] = flag.value

    return flags


def _threadsafe_mlflow_call(mlflow_fn):
    """
    Get a threadsafe wrapper around an `mlflow` function.
    NOTE: If the tracking API ever becomes threadsafe (see
          https://www.mlflow.org/docs/latest/python_api/mlflow.htm) then we can stop doing this.
    """
    def threadsafe_mlflow_fn(*args, **kwargs):
        if FLAGS.mlflow_tracking_uri:
            try:
                MLFLOW_LOCK.acquire()
                mlflow_fn(*args, **kwargs)
            finally:
                MLFLOW_LOCK.release()
    return threadsafe_mlflow_fn


log_metric = _threadsafe_mlflow_call(mlflow.log_metric)
log_param = _threadsafe_mlflow_call(mlflow.log_param)
set_tag = _threadsafe_mlflow_call(mlflow.set_tag)


@contextlib.contextmanager
def run():
    if FLAGS.mlflow_tracking_uri:
        mlflow.set_tracking_uri(FLAGS.mlflow_tracking_uri)

        if os.path.exists(GET_MLFLOW_UUID_FILE()):
            with open(GET_MLFLOW_UUID_FILE()) as f:
                mlflow_run_uuid = f.read()
            run_ctx = mlflow.start_run(run_uuid=mlflow_run_uuid)
        else:
            mlflow.set_experiment(FLAGS.mlflow_experiment)
            run_ctx = mlflow.start_run(
                source_version=buildinfo.STABLE_BUILD_SCM_REVISION,
                run_name=FLAGS.mlflow_run,
            )
            for k, v in _harvest_flags().items():
                if v != None:
                    if str(v) == "":
                        # NOTE: Empty strings seem to completely bork MLFLow, so we don't send them.
                        logging.warn(
                            "Param '{}' had empty value '{}'; not reporting it to MLFlow.".format(k, v))
                    else:
                        log_param(k, v)

        with run_ctx as run:
            if not os.path.exists(GET_MLFLOW_UUID_FILE()):
                with open(GET_MLFLOW_UUID_FILE(), "w") as f:
                    f.write(run.info.run_uuid)

            yield
    else:
        logging.warn("MLFlow is disabled. This experiment will not be tracked.")
        yield
