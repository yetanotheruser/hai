from __future__ import absolute_import
from __future__ import print_function

from collections import namedtuple

Action = namedtuple("Action", [
    "type_",  # index into `ACTION_TYPE_KEYS` of this action's type

    "battlecry",
    "discover",
    # No `end_turn` because logit is sufficient.
    "hero",
    "hero_power",
    "physical_attack",
    "spell",
    "summon",
    "weapon",

    "mulligan",
])

DiscoverAction = namedtuple("DiscoverAction", [
    "card_id",  # NOTE: This is an Entity ID. It ends up calling
                # `<Spellsource>/game/src/main/java/net/demilich/metastone/game/entities/Entity.java#getId`
])
MulliganAction = namedtuple("MulliganAction", ["n_to_discard", "entity_ids"])
PhysicalAttackAction = namedtuple("PhysicalAttackAction", ["source_id", "target"])
SpellAction = namedtuple("SpellAction", [
    "source_id",
    "target",  # optional; always output but may be ignored if selected spell is untargetable.
])
SummonAction = namedtuple("SummonAction", ["source_id", "index"])
