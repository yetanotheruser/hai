from __future__ import absolute_import
from __future__ import print_function

import collections
import sys

import numpy as np
import pytest
import tensorflow as tf
tf.enable_eager_execution()

from ai.tf_util import (
    batch_inner_product_iterables,
    concatenate_iterables,
    iterables_to_tuples,
    map_over_iterables,
    namedtuple_to_template,
    namedtuple_from_template,
    assert_tensor_has_static_shape,
)
import ai.tf_util as tf_util

A = collections.namedtuple("A", ["b", "c"])
B = collections.namedtuple("B", ["d", "e"])
C = collections.namedtuple("C", ["f", "g"])
D = collections.namedtuple("D", ["h"])


class TestBatchInnerProductIterables(object):
    def test_np_ndarray(self):
        in1 = (
            (
                np.array([[1, 2], [3, 4]]),
            ),
            np.array([[1], [2]]),
        )
        in2 = (
            (
                np.array([[0, 1], [1, 0]]),
            ),
            np.array([[0], [1]]),
        )
        expected_out = (
            (
                np.array([2, 3]),
            ),
            np.array([0, 2]),
        )
        actual_out = batch_inner_product_iterables(in1, in2, sum_fn=lambda x: np.sum(x, axis=1))

        assert len(actual_out) == 2
        assert len(actual_out[0]) == 1
        assert np.array_equal(actual_out[0][0], expected_out[0][0])
        assert np.array_equal(actual_out[1], expected_out[1])


class TestConcatenateIterables(object):
    def test_np_ndarray(self):
        in_ = (
            (
                np.array([[1, 2], [3, 4]]),
            ),
            np.array([[5], [6]]),
        )
        expected_out = np.array([[1, 2, 5], [3, 4, 6]])
        actual_out = concatenate_iterables(in_, concat_fn=np.concatenate)
        assert np.array_equal(actual_out, expected_out)


class TestIterablesToTuples(object):
    def test_base_case_numpy_array(self):
        in_ = np.array([[1]])
        expected_out = in_
        assert iterables_to_tuples(in_) == expected_out

    def test_simple_idempotence_on_tuples(self):
        in_ = (np.array([[1]]), np.array([[1, 2], [3, 4]]))
        expected_out = in_
        assert iterables_to_tuples(in_) == expected_out

    def test_list_to_tuple(self):
        in_ = [np.array([[1]]), np.array([[1, 2], [3, 4]])]
        expected_out = tuple(in_)
        assert iterables_to_tuples(in_) == expected_out


class TestMapOverIterables(object):
    def test_basic(self):
        in_ = ((1, 2), 3)
        assert map_over_iterables(lambda x: x + 1, in_) == ((2, 3), 4)

    def test_named_tuple(self):
        in_ = A(
            b=[1, 2, 3],
            c=5,
        )
        expected_out = (
            (2, 3, 4),
            6,
        )
        assert map_over_iterables(lambda x: x + 1, in_) == expected_out


class TestMapOverNamedtuple(object):
    def test_basic(self):
        in_ = A(B([1, 2, 3], 4), C(5, (6, 7)))
        expected_out = A(B([2, 3, 4], 5), C(6, (7, 8)))
        assert expected_out == tf_util.map_over_namedtuple(lambda x: x + 1, in_)


class TestNamedtupleFromTemplate(object):
    @pytest.mark.parametrize("namedtuple_", [
        A(1, 2),
        A(B(1, 2), 3),
        A(B(D(1), 2), C(3, 4)),
        A(B([1, 2, 3], 4), C(5, (6, 7))),
    ])
    def test_basic(self, namedtuple_):
        tuple_ = tf_util.flatten(namedtuple_)
        template = namedtuple_to_template(namedtuple_)
        assert namedtuple_ == namedtuple_from_template(template, tuple_)


class TestTensorHasStaticShape(object):
    def test_basic(self):
        in_ = tf.constant([[1], [2]], dtype=tf.float32)
        prefix_dims = [2, 1]
        assert_tensor_has_static_shape(in_, prefix_dims) == True

    def test_returns_false_for_prefix(self):
        in_ = tf.constant([[1, 2], [3, 4]], dtype=tf.float32)
        prefix_dims = [2]
        with pytest.raises(tf_util.UnexpectedShapeException):
            assert_tensor_has_static_shape(in_, prefix_dims)


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "--pdb",  # On exception drop into pdb++.
        "-s",
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
