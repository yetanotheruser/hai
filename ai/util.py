from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import contextlib
import os

from absl import flags
from absl import logging
from tensorflow.python.client import timeline
import tensorflow as tf

import ai.config  # For `output_dir` flag.
import ai.mem_util as mem_util

FLAGS = flags.FLAGS
flags.DEFINE_bool("profile_cpu",
                  False,
                  "Whether or not to use cProfile to characterize performance.")
flags.DEFINE_bool("profile_mem_leaks",
                  False,
                  "Whether or not to use Pympler to detect memory leaks.")
flags.DEFINE_bool("report_tensor_allocations_upon_oom",
                  False,
                  "Whether or not to report what tensors were allocated on TF OOM."
                  "May slow things down when enabled.")
flags.DEFINE_integer("profile_tf_iters",
                     None,
                     "Record GPU peroformance timelines every <n> (act or training) iterations .")


@contextlib.contextmanager
def launch_pdb_on_exception():
    try:
        yield
    except Exception:
        import sys
        import traceback
        import pdb
        info = sys.exc_info()
        traceback.print_exception(*info)
        pdb.post_mortem(info[2])


@contextlib.contextmanager
def maybe_profile(
        base_log_path,  # type: str
        break_on_startstop=False,
):
    if FLAGS.profile_cpu:
        import cProfile, pstats, StringIO
        pr = cProfile.Profile()
        pr.enable()

    if FLAGS.profile_mem_leaks:
        from pympler.tracker import SummaryTracker
        tracker = SummaryTracker()

    if break_on_startstop:
        import pdb
        pdb.set_trace()

    try:
        yield
    finally:
        if FLAGS.profile_cpu:
            stats_file_path = "{}.cpu.pstats".format(base_log_path)
            pr.create_stats()
            pr.dump_stats(stats_file_path)
            logging.info("Saved cpu stats to '{}'.".format(stats_file_path))

        if FLAGS.profile_mem_leaks:
            # Force generational gc sweep before collecting stats about memory. I'm not sure if this
            # affects anything.
            import gc
            gc.collect()

            with open("{}.mem_leaks.muppydiff".format(base_log_path), "w") as f:
                for l in tracker.format_diff():
                    f.write(l + "\n")

    if break_on_startstop:
        import pdb
        pdb.set_trace()


def maybe_instrument_tf(
        sess_name,  # type: str
):
    class Context:
        call_counter = 0

    def _maybe_instrument_tf(sess_run_wrapper):
        run_options = tf.RunOptions(
            report_tensor_allocations_upon_oom=FLAGS.report_tensor_allocations_upon_oom,
        )
        if FLAGS.profile_tf_iters and Context.call_counter % FLAGS.profile_tf_iters == 0:
            #run_options.trace_level = tf.RunOptions.FULL_TRACE
            run_options.trace_level = tf.RunOptions.SOFTWARE_TRACE
            run_options.output_partition_graphs = True
            run_metadata = tf.RunMetadata()

            results = sess_run_wrapper(options=run_options, run_metadata=run_metadata)

            # Create the Timeline object, and write it to a json file
            fetched_timeline = timeline.Timeline(run_metadata.step_stats)
            chrome_trace = fetched_timeline.generate_chrome_trace_format(show_memory=True)
            timeline_file_path = os.path.join(
                FLAGS.output_dir,
                "{}_{}.tl.json".format(sess_name, Context.call_counter))
            with open(timeline_file_path, "w") as f:
                f.write(chrome_trace)
            logging.info("Logged timeline to '{}'".format(timeline_file_path))

            # Log peak memory usage.
            logging.info(mem_util.peak_memory(run_metadata))

        else:
            results = sess_run_wrapper(options=run_options)

        Context.call_counter += 1
        return results

    return _maybe_instrument_tf


def add_check_numerics_ops(name_prefixes):
    """
    A slightly improved `tf.add_check_numerics_ops`. In particular it adds:
    - Support for only instrumenting ops within certain name prefixes.
    """
    if tf.executing_eagerly():
        raise RuntimeError(
            "add_check_numerics_ops() is not compatible with eager execution. "
            "To check for Inf's and NaN's under eager execution, call "
            "tfe.seterr(inf_or_nan='raise') once before executing the "
            "checked operations.")

    check_op = []
    # This code relies on the ordering of ops in get_operations().
    # The producer of a tensor always comes before that tensor's consumer in
    # this list. This is true because get_operations() returns ops in the order
    # added, and an op can only be added after its inputs are added.
    for op in tf.get_default_graph().get_operations():
        for output in op.outputs:
            if output.dtype in [tf.float16, tf.float32, tf.float64]:
                if any(op.name.startswith(name_prefix) for name_prefix in name_prefixes):
                    if op._get_control_flow_context() is not None:
                        raise ValueError("`tf.add_check_numerics_ops() is not compatible "
                                         "with TensorFlow control flow operations such as "
                                         "`tf.cond()` or `tf.while_loop()`.")

                    message = op.name + ":" + str(output.value_index)
                    with tf.control_dependencies(check_op):
                        check_op = [tf.check_numerics(output, message=message)]
    return tf.group(*check_op)
