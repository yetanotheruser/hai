from __future__ import absolute_import
from __future__ import print_function

import os

from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string("output_dir",
                    "/tmp/output_dir",
                    "Directory in which to save training logs / checkpoints / etc.")

def GET_REPLAY_DIR():
    return os.path.join(FLAGS.output_dir, "replays")
def GET_SUMMARY_DIR():
    return os.path.join(FLAGS.output_dir, "summaries")
TF_DEVICE = "/gpu:0"
QUEUE_WAIT_TIME_SECONDS = 1800
