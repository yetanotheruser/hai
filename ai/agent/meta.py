from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import bisect

from absl import flags

from ai.agent.base import BaseAgent


class InvalidGameIndexException(Exception): pass


def _agent_and_relative_idx(partition_boundaries, absolute_idx):
    if absolute_idx < 0:
        raise InvalidGameIndexException(
            "Game index '{}' must be non-negative.".format(absolute_idx))
    if absolute_idx >= partition_boundaries[-1]:
        raise InvalidGameIndexException(
            "Game index '{}' must be less than {}.".format(absolute_idx, partition_boundaries[-1]))

    agent_idx = bisect.bisect_right(partition_boundaries, absolute_idx) - 1
    relative_game_idx = absolute_idx - partition_boundaries[agent_idx]

    return agent_idx, relative_game_idx


def _partition_boundaries(game_counts):
    current_boundary = 0
    partition_boundaries = [current_boundary]
    for game_count in game_counts:
        current_boundary += game_count
        partition_boundaries.append(current_boundary)
    return partition_boundaries


def _partition_game_indices(partition_boundaries, game_indices, lists):
    current_partition = []
    current_partition_number = 1
    input_idx = 0
    partitions = [current_partition]
    sorted_inputs = sorted(zip(game_indices, *lists))

    while input_idx < len(sorted_inputs):
        inputs = sorted_inputs[input_idx]
        game_idx = inputs[0]

        if game_idx < 0:
            raise InvalidGameIndexException(
                "Game index '{}' must be non-negative.".format(game_idx))
        if game_idx >= partition_boundaries[-1]:
            raise InvalidGameIndexException(
                "Game index '{}' must be less than {}.".format(game_idx, partition_boundaries[-1]))

        if game_idx < partition_boundaries[current_partition_number]:
            # Adjust game index to be relative to partition beginning.
            adjusted_inputs = [inputs[0] - partition_boundaries[current_partition_number - 1]] \
                + list(inputs[1:])
            current_partition.append(adjusted_inputs)
            input_idx += 1
        else:
            current_partition = []
            partitions.append(current_partition)
            current_partition_number += 1

    return partitions


class MetaAgent(BaseAgent):
    """
    Agent that cannot actually make actions. It is intended to be used as a placeholder when
    playing against native Spellsource agents.
    """

    def __init__(self, agents, game_counts):
        for agent in agents:
            assert isinstance(agent, BaseAgent)
        self._agents = agents
        self._partition_boundaries = _partition_boundaries(game_counts)

    def actions_for_observations(
            self,
            observations,  # type: List[observation_pb2.Observation]
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[List[action_pb2.Action]]
            game_indices,  # type: List[int]
    ):
        # type: (...) -> List[action_pb2.Action]
        partitioned_inputs = _partition_game_indices(self._partition_boundaries,
                                                     game_indices,
                                                     [observations, valid_actions])

        partitioned_indices = []
        partitioned_observations = []
        partitioned_valid_actions = []
        for inputs in partitioned_inputs:
            agent_indices = []
            partitioned_indices.append(agent_indices)
            agent_observations = []
            partitioned_observations.append(agent_observations)
            agent_valid_actions = []
            partitioned_valid_actions.append(agent_valid_actions)

            for game_elem in inputs:
                agent_indices.append(game_elem[0])
                agent_observations.append(game_elem[1])
                agent_valid_actions.append(game_elem[2])

        actions = []
        for agent, idxs, os, vas in zip(self._agents, partitioned_indices, partitioned_observations,
                                        partitioned_valid_actions):
            actions.extend(
                agent.actions_for_observations(os, monitored_sess, vas, idxs))

        return actions

    def clear(self):
        for agent in self._agents:
            agent.clear()

    def end_game(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
            won,  # type: Optional[bool]
            i,  # type: int
    ):
        agent_idx, relative_game_idx = _agent_and_relative_idx(self._partition_boundaries, i)
        return self._agents[agent_idx].end_game(monitored_sess, won, relative_game_idx)

    def get_name(self, i):
        agent_idx, relative_game_idx = _agent_and_relative_idx(self._partition_boundaries, i)
        return self._agents[agent_idx].get_name(relative_game_idx)

    def get_trainable_vars(self):
        trainable_vars = []
        for agent in self._agents:
            trainable_vars.extend(agent.get_trainable_vars())
        return trainable_vars

    def start_training(self, *args, **kwargs):
        raise Exception("MetaAgent is not indended for training!")

    def stop_training(self, *args, **kwargs):
        raise Exception("MetaAgent is not indended for training!")

    def start_new_game(self, i, deck, opponent_deck):
        agent_idx, relative_game_idx = _agent_and_relative_idx(self._partition_boundaries, i)
        return self._agents[agent_idx].start_new_game(relative_game_idx, deck, opponent_deck)
