from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cPickle as pickle
import os
import threading

from absl import flags
from absl import logging
import tensorflow as tf

import ai.util as util

FLAGS = flags.FLAGS


class AsyncTrainer(object):
    def __init__(
            self,
            train_fn,  # type: function
    ):
        self._tf_initialized = False
        self._trainer_thread = TrainerThread(
            train_fn=train_fn,
        )

    def start(
            self,
            beholder,  # type: tensorboard.plugins.beholder.Beholder
            sess,  # type: tf.Session
    ):
        self._trainer_thread.start_runner(beholder=beholder, sess=sess)

    def join(self):
        self._trainer_thread.join()


class TrainerThread(threading.Thread):
    def __init__(
            self,
            train_fn,
    ):
        threading.Thread.__init__(self)
        self.daemon = True

        self._session = None
        self._train = train_fn

    def run(self):
        assert self._session, \
            "Must set TrainerThread TF session before starting it. Try `start_runner`."
        logging.debug("Starting background trainer thread.")

        with util.maybe_profile(base_log_path=os.path.join(FLAGS.output_dir, "training")):
            while True:
                logging.debug("Running training iteration.")
                fetches, queue_closed = self._train(self._session)
                if queue_closed:
                    logging.info("Training queue closed; stopping training.")
                    break

                beholder_vars = fetches["beholder_vars"]
                logging.debug("Finished one training call.")
                try:
                    self._beholder.update(session=self._session, arrays=beholder_vars)
                except:
                    logging.warn("Beholder update failed.")

    def start_runner(self, beholder, sess):
        self._beholder = beholder
        self._session = sess
        self.start()
