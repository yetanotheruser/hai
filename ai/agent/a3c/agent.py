"""
An agent that learns using A3C while playing.
"""
from __future__ import absolute_import
from __future__ import print_function

import collections
import random

from absl import flags
from absl import logging
import numpy as np
import threading
import tensorflow as tf

from ai.agent.base import BaseAgent
from ai.agent.a3c.trainer import AsyncTrainer
from ai.agent.util import (
    InvalidArgumentException,
    Rollout,
    calculate_best_action,
)
from ai.config import (
    GET_SUMMARY_DIR,
    TF_DEVICE,
)
from ai.deserialization.action import (
    deflate_action,
    inflate_actions,
)
from ai.policy.input.common import (
    ProtogenException,
)
from ai.policy.optimizer.a3c import (
    A3cPolicyOptimizer,
)
from ai.policy.optimizer.ppo import (
    PpoPolicyOptimizer,
)
from ai.policy.policy import (
    BasicPolicy,
)
from ai.simulator_parameters import (
    GET_MAX_N_ACTIONS,
)
from ai.tf_util import iterables_to_tuples
import ai.mlflow_wrapper as mlflow_wrapper
import ai.tf_util as tf_util
import deck.decklists as decklists
import spellsource.proto.action_pb2 as action_pb2

FLAGS = flags.FLAGS
flags.DEFINE_bool("shuffle_actions",
                  False,
                  "Causes input actions to be shuffled before inference. This can help make up for"
                  + "`max_n_actions`.")
flags.DEFINE_enum("policy_optimizer",
                  PpoPolicyOptimizer.__name__,
                  [o.__name__ for o in [A3cPolicyOptimizer, PpoPolicyOptimizer]],
                  "Policy optimization algorithm to use during trainng.")


def _mulligan_heuristic(inflated_valid_actions, observation):
    entity_ids = set()
    assert inflated_valid_actions[0].WhichOneof("action_oneof") == "mulligan"
    for discard_entity_id in inflated_valid_actions[0].mulligan.discard_entity_ids:
        entity_ids.add(discard_entity_id)
    for keep_entity_id in inflated_valid_actions[0].mulligan.keep_entity_ids:
        entity_ids.add(keep_entity_id)

    entities = []
    for entity in observation.game_state.entities:
        if entity.id in entity_ids:
            entities.append(entity)

    action = action_pb2.Action(mulligan=action_pb2.MulliganAction())
    for entity in entities:
        if entity.state.mana_cost > 3:
            action.mulligan.discard_entity_ids.extend([entity.id])
        else:
            action.mulligan.keep_entity_ids.extend([entity.id])

    return action


def _mulligan_random(inflated_valid_actions, observation):
    for inflated_valid_action in inflated_valid_actions:
        assert inflated_valid_action.WhichOneof("action_oneof") == "mulligan"
    return random.choice(inflated_valid_actions)


class A3cAgent(BaseAgent):
    def __init__(
            self,
            n_concurrent_games,  # type: int
            beholder_var_fragments,  # type: List[str]
            mode,  # type: str
            player,  # type: player_pb2.Player
    ):
        self._decks = n_concurrent_games * [None]
        self._game_started = n_concurrent_games * [False]
        self._global_step = tf.train.get_or_create_global_step()
        self._last_m_episode_results = collections.deque()
        self._last_m_episode_results_lock = threading.Lock()
        self._max_win_ratio = 0
        self._mode = mode
        self._n_concurrent_games = n_concurrent_games
        self._opponent_trainable_vars = None
        self._player = player
        self._rollouts = n_concurrent_games * [None]
        self._summary_writer = tf.summary.FileWriterCache.get(GET_SUMMARY_DIR())
        self._training = False

        with tf.device(TF_DEVICE):
            self._policy = BasicPolicy(
                beholder_var_fragments=beholder_var_fragments,
                policy_optimizer_class=globals()[FLAGS.policy_optimizer](),
                mode=mode,
            )

        self._agent_state = np.concatenate(
            n_concurrent_games * [self._policy.get_initial_model_state()], axis=0)
        if mode == "predict_or_train":
            self._trainer = AsyncTrainer(train_fn=self._policy.create_train_fn())

    def actions_for_observations(
            self,
            observations,  # type: List[observation_pb2.Observation]
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[List[action_pb2.Action]]
            game_indices,  # type: List[int]
    ):
        # type: (...) -> List[action_pb2.Action]

        if len(observations) == 0:
            return []

        agent_state = self._agent_state[game_indices, :]

        # Process `observation` into format for `policy.act`.
        game_states_np = self._policy.observations_to_np(observations, self._player)

        # Process `valid_actions` into format for `policy.act`.
        inflated_valid_actions_list = [inflate_actions(vas) for vas in valid_actions]
        if FLAGS.shuffle_actions:
            for ivas in inflated_valid_actions_list:
                random.shuffle(ivas)

        valid_actions_np = self._policy.actions_to_np(
            inflated_valid_actions_list,
            observations,
            self._player)
        n_valid_actions_np = np.asarray([len(ival) for ival in inflated_valid_actions_list], np.int32)
        n_valid_actions_np = n_valid_actions_np[:, np.newaxis]

        # Get recommended action and estimated value (according to policy).
        policy_outputs = self._policy.act(
            game_state_input=game_states_np,
            model_state_input=agent_state,
            monitored_sess=monitored_sess,
            n_valid_actions_input=n_valid_actions_np,
            valid_actions_input=valid_actions_np,
        )
        action_probs = policy_outputs[0]
        agent_state = policy_outputs[1]

        actions, action_indices = [], []
        for i in range(len(inflated_valid_actions_list)):
            action, action_idx = calculate_best_action(
                inflated_valid_actions_list[i],
                action_probs[i:i+1, :],
            )
            actions.append(deflate_action(action))
            action_indices.append(action_idx)

        self._agent_state[game_indices, :] = agent_state

        if self._training:
            value = policy_outputs[2]
            for relative_idx, game_idx in enumerate(game_indices):
                selected_action_prob = action_probs[relative_idx, action_indices[relative_idx]]
                if selected_action_prob < 0.001:
                    raise Exception(
                        "Suspiciously low selected action prob '{}'".format(
                            selected_action_prob))

                # Record this transition for training.
                self._rollouts[game_idx].add(
                    game_state=game_states_np[relative_idx:relative_idx+1, :, :],
                    model_state=self._agent_state[game_idx:game_idx+1, :],
                    reward=0.0,
                    selected_action_idx=int(action_indices[relative_idx]),
                    selected_action_prob=float(selected_action_prob),
                    valid_actions=valid_actions_np[relative_idx:relative_idx+1, :, :],
                    n_valid_actions=len(inflated_valid_actions_list[relative_idx]),
                    value=float(value[relative_idx]),
                )

        # Overwrite mulligans with heuristic.
        #for i, ivas in enumerate(inflated_valid_actions_list):
        #    if ivas[0].WhichOneof("action_oneof") == "mulligan":
        #        actions[i] = _mulligan_heuristic(ivas, observations[i])
        # Overwrite mulligans with random.
        #for i, ivas in enumerate(inflated_valid_actions_list):
        #    if ivas[0].WhichOneof("action_oneof") == "mulligan":
        #        actions[i] = _mulligan_random(ivas, observations[i])
        # Overwrite non-mulligans with GSVB playout.
        #for i, ivas in enumerate(inflated_valid_actions_list):
        #    if ivas[0].WhichOneof("action_oneof") != "mulligan":
        #        actions[i] = action_pb2.Action(playout=action_pb2.PlayoutAction())

        return actions

    def clear(self):
        for i in range(len(self._game_started)):
            self._game_started[i] = False

    def end_game(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
            won,  # type: Optional[bool]
            i,  # type: int
    ):
        assert self._game_started[i], "Cannot end game that hasn't started."
        self._game_started[i] = False

        # NOTE: We check the rollout length is at least 1, because when being seeded from random
        # game states, sometimes (somewhat often) we end up with a game that the opponent
        # immediately wins (which is just an empty game for us; which we should ignore).
        if self._training and won != None and len(self._rollouts[i].rewards) > 0:
            reward = -1
            if won:
                reward = 1
            self._rollouts[i].update_last_reward(reward)
            self._rollouts[i].terminate()
            self._policy.enqueue_rollout(monitored_sess, self._rollouts[i])
            logging.debug("Enqueued rollout with reward {}.".format(reward))
            self._record_episode_summaries(monitored_sess, self._rollouts[i], self._decks[i], won)

    def flush_training_queue(self, monitored_sess):
        assert self._mode == "predict_or_train"
        logging.info("Waiting for training queue to deplete...")
        # NOTE: We do this because Tensorflow queues are incredibly obnoxious to close and re-open.
        # See
        # https://stackoverflow.com/questions/39204335/can-a-tensorflow-queue-be-reopened-after-it-is-closed
        self._policy.flush_training_queue(monitored_sess)

    def get_name(self, *args, **kwargs):
        return "A3c"

    def get_trainable_vars(self):
        return self._policy.trainable_vars

    def global_step(self, monitored_sess):
        def step_fn(step_context):
            return step_context.session.run(self._global_step)
        return monitored_sess.run_step_fn(step_fn)

    def pause_training(self, monitored_sess):
        assert self._mode == "predict_or_train"
        self.flush_training_queue(monitored_sess)
        self._training = False
        self.clear()

    def register_opponent_trainable_vars(self, opponent_trainable_vars):
        self._policy.register_baseline_trainable_vars(opponent_trainable_vars)

    def register_error_saver(
            self,
            error_saver,  # type: tf.train.Saver
    ):
        self._policy.register_error_saver(error_saver)

    def resume_training(self):
        assert self._mode == "predict_or_train"
        self.clear()
        self._training = True

    def start_new_game(self, i, deck, opponent_deck):
        if self._game_started[i]:
            raise Exception(
                "Player {} cannot start game that's already in progress.".format(self._player))
        self._game_started[i] = True

        deck_name = decklists.get_sanitized_name(deck)
        opponent_deck_name = decklists.get_sanitized_name(opponent_deck)
        self._decks[i] = {
            "deck_name": deck_name,
            "deck": deck,
            "opp_deck_name": opponent_deck_name,
            "opp_deck": opponent_deck,
        }

        self._agent_state[i, :] = self._policy.get_initial_model_state()
        self._rollouts[i] = Rollout()

    def start_training(self, *args, **kwargs):
        assert self._mode == "predict_or_train"
        # Init and start background thread that trains this agent.
        self._trainer.start(*args, **kwargs)
        self._training = True

    def stop_training(self, monitored_sess):
        assert self._mode == "predict_or_train"
        logging.info("Stopping trainer thread.")
        self._policy.stop_trainer(monitored_sess)
        logging.debug("Waiting for trainer thread to stop.")
        self._trainer.join()
        logging.debug("Trainer thread stopped.")

        self._training = False
        self.clear()

    def _record_episode_summaries(self, monitored_sess, rollout, decks, won):
        deck_str = decks["deck_name"]
        opp_deck_str = decks["opp_deck_name"]
        result = (deck_str, opp_deck_str, won)
        global_step = self.global_step(monitored_sess)

        with self._last_m_episode_results_lock:
            self._last_m_episode_results.append(result)
            if len(self._last_m_episode_results) > 100:
                self._last_m_episode_results.popleft()

            wins = 0
            total = 0
            deck_wins = 0
            deck_total = 0
            matchup_wins = 0
            matchup_total = 0
            for d1, d2, won in self._last_m_episode_results:
                wins += float(won)
                total += 1
                if d1 == deck_str:
                    deck_wins += float(won)
                    deck_total += 1
                    if d2 == opp_deck_str:
                        matchup_wins += float(won)
                        matchup_total += 1

        win_ratio = wins / total
        self._max_win_ratio = max(self._max_win_ratio, win_ratio)
        if deck_total > 0:
            win_ratio_deck = deck_wins / deck_total
        if matchup_total > 0:
            win_ratio_matchup = matchup_wins / matchup_total

        # Don't log things until episode queue is saturated.
        episode_summary = tf.Summary()
        episode_summary.value.add(
            tag="episode/length", simple_value=int(rollout.length))
        episode_summary.value.add(
            tag="episode/reward", simple_value=float(rollout.total_rewards))
        if total == 100:
            episode_summary.value.add(
                tag="episode/win_ratio_100", simple_value=win_ratio)
            if deck_total > 0:
                episode_summary.value.add(
                    tag="deck_win_ratios_100/{}".format(deck_str), simple_value=win_ratio_deck)
            if matchup_total > 0:
                episode_summary.value.add(
                    tag="matchup_win_ratios_100/{}_{}".format(deck_str, opp_deck_str),
                    simple_value=win_ratio_matchup)

        self._summary_writer.add_summary(
            episode_summary,
            global_step=global_step,
        )
        self._summary_writer.flush()

        # Also (periodically) report things to MLFlow so we can understand how the
        # experiments are doing.
        if random.random() < 0.05:  # Report every ~1/20 games.
            mlflow_wrapper.log_metric("global_step", global_step)
            if total == 100:
                mlflow_wrapper.log_metric("wr100t", win_ratio)
                mlflow_wrapper.log_metric("wr100t_max", self._max_win_ratio)
                if deck_total > 0:
                    mlflow_wrapper.log_metric("wr100t_{}".format(deck_str), win_ratio_deck)
