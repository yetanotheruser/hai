from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from ai.agent.base import BaseAgent
from ai.agent.util import (
    make_action_singular,
)
from ai.simulator_parameters import (
    ACTION_TYPE_PHYSICAL_ATTACK,
    ACTION_TYPE_SUMMON,
)
import spellsource.proto.player_pb2 as player_pb2


class HeuristicAgent(BaseAgent):
    def __init__(
            self,
            batch_size,  # type: int
            beholder_vars,  # type: List[str]
            mode,  # type: str
            player,  # type: player_pb2.Player
    ):
        # type: (...) -> None
        if player == player_pb2.Player_1:
            self._enemy_player = player_pb2.Player_2
        else:
            self._enemy_player = player_pb2.Player_1
        self._player = player

    def action_for_observation(
            self,
            observation,  # type: observation_pb2.Observation
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[action_pb2.Action]
    ):
        # type: (...) -> action_pb2.Action

        # First try attacking
        attack_actions = filter(
            lambda a: a.WhichOneof("action_oneof") == ACTION_TYPE_PHYSICAL_ATTACK,
            valid_actions,
        )
        if len(attack_actions) > 0:
            return make_action_singular(attack_actions[0])

        # Otherwise we play a minion if possible.
        play_minion_actions = filter(
            lambda a: a.WhichOneof("action_oneof") == ACTION_TYPE_SUMMON,
            valid_actions,
        )
        if len(play_minion_actions) > 0:
            return make_action_singular(play_minion_actions[0])

        # As a last resort return an arbitrary action.
        return make_action_singular(valid_actions[0])

    def actions_for_observations(
            self,
            observations,  # type: List[observation_pb2.Observation]
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[List[action_pb2.Action]]
            game_indices,  # type: List[int]
    ):
        # type: (...) -> List[action_pb2.Action]
        actions = []
        for i in range(len(observations)):
            actions.append(self.action_for_observation(observations[i], None, valid_actions[i]))
        return actions

    def end_game(self, *args, **kwargs):
        pass

    def get_name(self, *args, **kwargs):
        return "Heuristic"

    def get_trainable_vars(self):
        raise Exception("No trainable vars in HeuristicAgent.")

    def start_training(self, *args, **kwargs):
        pass

    def stop_training(self, *args, **kwargs):
        pass

    def start_new_game(self, *args, **kwargs):
        pass
