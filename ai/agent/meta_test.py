from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import pytest

import ai.agent.meta as meta


class Test_AgentAndRelativeIdx(object):
    @pytest.mark.parametrize("partition_boundaries,absolute_idx,expected_out", [(
        [0, 1, 2], 1,
        (1, 0),
    ), (
        [0, 2, 5, 7], 4,
        (1, 2),
    ), (
        [0, 2, 5, 7], 5,
        (2, 0),
    ), (
        [0, 2, 5, 10], 8,
        (2, 3),
    )], ids=[
        "basic1",
        "basic2",
        "basic3",
        "basic4",
    ])
    def test_template(self, partition_boundaries, absolute_idx, expected_out):
        assert expected_out == meta._agent_and_relative_idx(partition_boundaries, absolute_idx)

    def test_index_out_of_range(self):
        # Too low.
        with pytest.raises(meta.InvalidGameIndexException) as e:
            meta._agent_and_relative_idx([0, 3], -1)
        assert "must be non-negative" in e.value.message

        # Too high
        with pytest.raises(meta.InvalidGameIndexException) as e:
            meta._agent_and_relative_idx([0, 3], 3)
        assert "index '3' must be less than 3" in e.value.message

class Test_PartitionGameIndices(object):
    @pytest.mark.parametrize("game_counts,game_indices,l1,l2,expected_out", [(
        [0, 1, 2, 3],
        [0, 1, 2],
        ["zero", "one", "two"],
        ["zero", "un", "deux"],
        [[[0, "zero", "zero"]], [[0, "one", "un"]], [[0, "two", "deux"]]],
    ), (
        [0, 1, 2, 3],
        [2, 1, 0],
        ["two", "one", "zero"],
        ["deux", "un", "zero"],
        [[[0, "zero", "zero"]], [[0, "one", "un"]], [[0, "two", "deux"]]],
    ), (
        [0, 3, 6, 9],
        [2, 1, 0, 8, 4, 7],
        ["two", "one", "zero", "eight", "four", "seven"],
        ["deux", "un", "zero", "huit", "quatre", "sept"],
        [
            [[0, "zero", "zero"], [1, "one", "un"], [2, "two", "deux"]],
            [[1, "four", "quatre"]],
            [[1, "seven", "sept"], [2, "eight", "huit"]],
        ],
    )], ids=[
        "basic",
        "disordered_game_indices",
        "sparse_game_indices",
    ])
    def test_template(self, game_counts, game_indices, l1, l2, expected_out):
        assert expected_out == meta._partition_game_indices(game_counts, game_indices, [l1, l2])

    def test_index_out_of_range(self):
        # Too low.
        with pytest.raises(meta.InvalidGameIndexException) as e:
            meta._partition_game_indices([1], [-1], [])
        assert "must be non-negative" in e.value.message

        # Too high
        with pytest.raises(meta.InvalidGameIndexException) as e:
            meta._partition_game_indices([1], [10], [])
        assert "index '10' must be less than 1" in e.value.message


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        #"-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
