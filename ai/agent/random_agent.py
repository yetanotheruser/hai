from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import random

from ai.agent.base import BaseAgent
from ai.deserialization.action import (
    inflate_actions,
)
from ai.simulator_parameters import (
    ACTION_TYPE_PHYSICAL_ATTACK,
    ACTION_TYPE_SUMMON,
)
import spellsource.proto.player_pb2 as player_pb2


class RandomAgent(BaseAgent):
    def __init__(self, *args, **kwargs): pass

    def action_for_observation(
            self,
            observation,  # type: observation_pb2.Observation
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[action_pb2.Action]
    ):
        # type: (...) -> action_pb2.Action
        inflated_valid_actions = inflate_actions(valid_actions)
        action_idx = random.randrange(len(inflated_valid_actions))
        return inflated_valid_actions[action_idx]

    def actions_for_observations(
            self,
            observations,  # type: List[observation_pb2.Observation]
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[List[action_pb2.Action]]
            game_indices,  # type: List[int]
    ):
        # type: (...) -> List[action_pb2.Action]
        actions = []
        for i in range(len(observations)):
            actions.append(self.action_for_observation(observations[i], None, valid_actions[i]))
        return actions

    def end_game(self, *args, **kwargs):
        pass

    def get_name(self, *args, **kwargs):
        return "Random"

    def get_trainable_vars(self):
        raise Exception("No trainable vars in RandomAgent.")

    def start_training(self, *args, **kwargs):
        pass

    def stop_training(self, *args, **kwargs):
        pass

    def start_new_game(self, *args, **kwargs):
        pass
