from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import google.protobuf.text_format as text_format
import pytest

from ai.agent.util import make_action_singular
import spellsource.proto.action_pb2 as action_pb2


class Test_MakeActionSingular(object):
    def test_basic(self):
        input_proto = action_pb2.Action()
        text_format.Parse("""
physical_attack {
    source_id: 1
    defenders {
        target: 1
        action: 1
    }
    defenders {
        target: 2
        action: 2
    }
    defenders {
        target: 3
        action: 3
    }
}
""", input_proto)

        output_proto = make_action_singular(input_proto)
        assert len(output_proto.physical_attack.defenders) == 1

    def test_spell_without_target(self):
        input_proto = action_pb2.Action()
        text_format.Parse("""
spell {
    source_id: 1
}
""", input_proto)

        output_proto = make_action_singular(input_proto)
        assert output_proto.spell.source_id == 1



if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
