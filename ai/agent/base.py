from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc


class BaseAgent(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(
            self,
            batch_size,  # type: int
            beholder_vars,  # type: List[str]
            mode,  # type: str
            player,  # type: player_pb2.Player
    ):
        # type: (...) -> None
        pass

    @abc.abstractmethod
    def actions_for_observations(
            self,
            observations,  # type: List[observation_pb2.Observation]
            monitored_sess,  # type: tf.train.MonitoredSession
            valid_actions,  # type: List[List[action_pb2.Action]]
            game_indices,  # type: List[int]
    ):
        # type: (...) -> List[action_pb2.Action]
        pass

    def clear(self):
        # type: (...) -> None
        pass

    @abc.abstractmethod
    def end_game(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
            won,  # type: Optional[bool]
            i,  # type: int
    ):
        """
        Register a game as finished.

        MUST BE THREAD SAFE (may be called with different `i` concurrently).
        """
        pass

    @abc.abstractmethod
    def get_name(self, i):
        """
        Return the name (to appear in replays) of this agent.
        """
        pass

    @abc.abstractmethod
    def get_trainable_vars(self):
        # type: (...) -> List[tf.Variable]
        """
        Return any Tensorflow trainable variables associated with this agent.
        """
        pass

    @abc.abstractmethod
    def start_training(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
    ):
        """
        Start any (potentially TF-dependent) training background processes for this Agent.

        Args:
        - session: The session in which the Agent will be run for the duration of simulation.
        """
        pass

    @abc.abstractmethod
    def stop_training(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
    ):
        """
        Stop any background training processes for this Agent.
        """
        pass

    @abc.abstractmethod
    def start_new_game(self, i, deck, opponent_deck):
        """
        Start a new game.

        MUST BE THREAD SAFE (may be called with different `i` concurrently).
        """
        pass
