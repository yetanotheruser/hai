from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

from absl import flags
import google.protobuf.text_format as text_format
import numpy as np
import pytest

from ai.agent.util import (
    entity_address,
)
import ai.agent.util
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.player_pb2 as player_pb2

FLAGS = flags.FLAGS


class TestEntityAddress(object):
    @pytest.mark.parametrize("in_location,in_pov_player,expected_address", [(
        "player: 0\n"
        "zone: WEAPON\n",
        player_pb2.Player_1,
        28
    ), (
        "player: 1\n"
        "zone: WEAPON\n",
        player_pb2.Player_1,
        29
    ), (
        "player: 0\n"
        "zone: WEAPON\n",
        player_pb2.Player_2,
        29
    ), (
        "player: 0\n"
        "index: 3\n"
        "zone: BATTLEFIELD\n",
        player_pb2.Player_2,
        20
    ), (
        "player: 0\n"
        "index: 2\n"
        "zone: SET_ASIDE_ZONE\n",
        player_pb2.Player_2,
        -1
    ), (
        "player: 1\n"
        "index: 1\n"
        "zone: SET_ASIDE_ZONE\n",
        player_pb2.Player_2,
        31
    )], ids=[
        "weapon1_pov1",
        "weapon2_pov1",
        "weapon1_pov2",
        "minion1_pov2",
        "mulligan1_pov2",
        "mulligan2_pov2",
    ])
    def test_template(self, in_location, in_pov_player, expected_address):
        in_ = entity_location_pb2.EntityLocation()
        text_format.Parse(in_location, in_)
        actual_address = entity_address(in_, in_pov_player)
        assert actual_address == expected_address


if __name__ == "__main__":
    FLAGS(["--sanity_check"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
