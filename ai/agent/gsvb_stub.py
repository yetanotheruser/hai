from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags

from ai.agent.base import BaseAgent

FLAGS = flags.FLAGS
flags.DEFINE_integer("gsvb_depth",
                     1,
                     "GameStateValueBehaviour search depth."
                     "Says we should play another gRPC client.")

class GsvbStubAgent(BaseAgent):
    """
    Agent that cannot actually make actions. It is intended to be used as a placeholder when playing
    against native Spellsource agents.
    """

    def __init__(*args, **kwargs):
        pass

    def actions_for_observations(*args, **kwargs):
        raise Exception("GsvbStubAgent cannot (should not) take actions.")

    def end_game(self, *args, **kwargs):
        pass

    def get_name(self, *args, **kwargs):
        return "GSVB-{}".format(FLAGS.gsvb_depth)

    def get_trainable_vars(self):
        raise Exception("No trainable vars in GsvbStubAgent.")

    def start_training(self, *args, **kwargs):
        pass

    def stop_training(self, *args, **kwargs):
        pass

    def start_new_game(self, *args, **kwargs):
        pass
