from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import random

from absl import flags
from absl import logging
import numpy as np
import tensorflow as tf

from ai.tf_util import (
    assert_ndarray_has_shape,
)
import ai.simulator_parameters as sp
import ai.tf_util as tf_util
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.entity_pb2 as entity_pb2
import spellsource.proto.player_pb2 as player_pb2

FLAGS = flags.FLAGS

class IllegalStateException(Exception): pass
class InvalidArgumentException(Exception): pass


class Rollout(object):
    def __init__(self):
        # type: (...) -> None
        self._awaiting_reward = False
        self.game_states = []  # type: List[GameState[np.ndarray[1, ?]]]
        self.length = 0  # type: int
        self.model_states = []  # type: List[np.ndarray]
        self.n_valid_actions = []  # type: List[int]
        self.rewards = []  # type: List[float]
        self.terminated = False  # type: bool
        self.selected_action_indices = []  # type: List[int]
        self.selected_action_probs = []  # type: List[float]
        self.total_rewards = 0  # type: float
        self.valid_actions = []  # type: List[Tuple[Action[np.ndarray]]
        self.values = []  # type: List[float]

    def add(
            self,
            game_state,
            model_state,
            n_valid_actions,
            reward,  # type: float
            selected_action_idx,  # type: int
            selected_action_prob,  # type: float
            valid_actions,  # type: Tuple[action_pb2.Action]
            value,  # type: float
    ):
        assert not self.terminated, "Cannot add to a terminated rollout."
        assert not self._awaiting_reward, \
                "Must report a reward before reporting next state transition."

        if FLAGS.sanity_check:
            assert_ndarray_has_shape(game_state, (1, sp.N_ADDRESSABLE_ENTITIES, tf_util.ANY))
            assert_ndarray_has_shape(model_state, (1, tf_util.ANY))
            assert type(n_valid_actions) == int
            assert type(reward) == float
            assert type(selected_action_idx) == int
            assert type(selected_action_prob) == float
            assert_ndarray_has_shape(valid_actions, (1, sp.GET_MAX_N_ACTIONS(), tf_util.ANY))
            assert type(value) == float

        self.game_states.append(game_state)
        self.length += 1
        self.model_states.append(model_state)
        self.n_valid_actions.append(n_valid_actions)
        self.rewards.append(reward)
        self.selected_action_indices.append(selected_action_idx)
        self.selected_action_probs.append(selected_action_prob)
        self.total_rewards += reward
        self.values.append(value)
        self.valid_actions.append(valid_actions)

    def update_last_reward(
            self,
            last_reward_delta,  # type: float
    ):
        self.total_rewards += last_reward_delta
        self.rewards[-1] += last_reward_delta

    def terminate(self):
        """
        Terminate a rollout.
        """
        self.terminated = True


def calculate_best_action(
        actions,  # type: List[action_pb2.Action]
        logits,  # type: np.ndarray[1 x GET_MAX_N_ACTIONS()]
):
    # type: (...) -> Tuple[action_pb2.Action, int]
    """
    Selects best action according to `logits` ignoring logits for padded actions.

    Args:
    - actions: (inflated/non-singular but unpadded) Actions to select from.
    - logits: The array of logits representing the value of each action.

    Returns:
    - The action corresponding to maximal logit.
    - The index of said action.
    """
    n_actions_to_consider = min(len(actions), sp.GET_MAX_N_ACTIONS())
    idx = np.argmax(logits[0, :n_actions_to_consider])
    action = actions[idx]
    return action, idx


def get_addressable_entities_in_zone(addressable_entities, zone, protagonist_or_antagonist):
    """
    Passes from the list of all addressible entities to the addressible entities in a particular
    zone (for example Player 1's hand).
    """
    # Select an arbitrary player, since in this function the "point of view" doesn't matter.
    pov_player = player_pb2.Player_1
    if protagonist_or_antagonist == "protagonist":
        poa_idx = 0
    else:
        poa_idx = 1

    location = entity_location_pb2.EntityLocation(player=poa_idx, zone=zone)
    begin_idx = entity_address(location, pov_player)

    location.index = sp.ADDRESSABLE_ENTITIES[zone][protagonist_or_antagonist] - 1
    end_idx = entity_address(location, pov_player)

    return addressable_entities[:, begin_idx:end_idx + 1, :]


def make_action_singular(action):
    if action.WhichOneof("action_oneof") == sp.ACTION_TYPE_PHYSICAL_ATTACK:
        _select_arbitrary_listitem(action.physical_attack, "defenders")

    # Summon.
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_SUMMON:
        _select_arbitrary_listitem(action.summon, "index_to_actions")
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_WEAPON:
        _select_arbitrary_listitem(action.weapon, "index_to_actions")

    # Spell.
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_BATTLECRY:
        if len(action.battlecry.target_key_to_actions) < 1:
            return action
        else:
            _select_arbitrary_listitem(action.battlecry, "target_key_to_actions")
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_HERO:
        if len(action.hero.target_key_to_actions) < 1:
            return action
        else:
            _select_arbitrary_listitem(action.hero, "target_key_to_actions")
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_HERO_POWER:
        if len(action.hero_power.target_key_to_actions) < 1:
            return action
        else:
            _select_arbitrary_listitem(action.hero_power, "target_key_to_actions")
    elif action.WhichOneof("action_oneof") == sp.ACTION_TYPE_SPELL:
        if len(action.spell.target_key_to_actions) < 1:
            return action
        else:
            _select_arbitrary_listitem(action.spell, "target_key_to_actions")

    return action


_PROTO_PLAYER_TO_PLAYER_IDX = {
    player_pb2.Player_1: 0,
    player_pb2.Player_2: 1,
}
def entity_address(entity_location, pov_player):
    """
    Compute an entities "address" index. -1 indicates entity_location is not addressable.
    """
    if entity_location.player == _PROTO_PLAYER_TO_PLAYER_IDX[pov_player]:
        entity_location_relative_player = "protagonist"
    else:
        entity_location_relative_player = "antagonist"

    if entity_location.zone not in sp.ADDRESSABLE_ENTITIES:
        # This entity is not in an addressable zone type.
        return -1

    zone_counts = sp.ADDRESSABLE_ENTITIES[entity_location.zone]
    if entity_location_relative_player not in zone_counts:
        # This entity in an addressable zone type but its player's zone of this type is not
        # addressable.
        return -1

    n_indices = zone_counts[entity_location_relative_player]
    if entity_location.index < 0 or n_indices <= entity_location.index:
        raise InvalidArgumentException((
            "Received invalid entity_location index '{}' for entity entity_location '{}'."
            " It is outside of valid range '0-{}'.").format(
                entity_location.index, entity_location, n_indices - 1))

    addressable_idx = 0
    for addressable_zone, _ in sp.SORTED_ADDRESSIBLE_ENTITIES:
        if entity_location.zone == addressable_zone:
            # We consider (arbitrarily) the protagonists indices to come before antagonists.
            if entity_location_relative_player == "antagonist":
                addressable_idx += zone_counts["protagonist"]
            break
        addressable_idx += sum(sp.ADDRESSABLE_ENTITIES[addressable_zone].values())

    addressable_idx += entity_location.index
    return addressable_idx


def _select_arbitrary_listitem(action, list_attr):
    list_ = getattr(action, list_attr)
    list_len = len(list_)
    assert list_len > 0, "Cannot select a listitem when there is none!"
    selected_idx = random.randrange(list_len)
    selected_listitem = list_[selected_idx]
    del list_[:]
    list_.extend([selected_listitem])
    return action
