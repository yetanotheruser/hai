from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import namedtuple
import functools
import Queue
import random
import time

from absl import flags
from absl import logging
import numpy as np
import tensorflow as tf

from ai.agent.util import (
    InvalidArgumentException,
)
from ai.model.complex.action_encoder import (
    ACTION_ENTITY_ID_FIELDS,
)
from ai.model.complex.complex import (
    ComplexModel,
)
from ai.policy.input.common import (
    ProtogenException,
)
from ai.policy.input.input import (
    get_action_placeholders_and_packer,
    get_game_state_placeholders_and_packer,
)
from ai.simulator_parameters import (
    GET_MAX_N_ACTIONS,
)
from ai.util import (
    maybe_instrument_tf,
)
import ai.agent.util as agent_util
import ai.config as config
import ai.simulator_parameters as sp
import ai.tf_util as tf_util
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.entity_pb2 as entity_pb2

DEFAULT_GAMMA = 0.99
DEFAULT_LAMBDA = 1

FLAGS = flags.FLAGS
flags.DEFINE_float("gae_gamma", DEFAULT_GAMMA, "")
flags.DEFINE_float("gae_lambda", DEFAULT_LAMBDA, "")

_ACTION_ENTITY_ID_FIELDS_UNPROCESSED = {}
# NOTE: Unfortunately for repeated elements our input format doesn't match protobufs; we have to
# pass from our input format to protos by removing extra `"elems"` keys from entity paths.
for action_type, paths in ACTION_ENTITY_ID_FIELDS.items():
    unprocessed_paths = []
    for path in paths:
        unprocessed_path = tuple(e for e in path if e != "elems")
        unprocessed_paths.append(unprocessed_path)
    _ACTION_ENTITY_ID_FIELDS_UNPROCESSED[action_type] = unprocessed_paths

QueueItem = namedtuple("QueueItem", [
    # Inference.
    "game_state",
    "model_state",
    "n_valid_actions",
    "valid_actions",
    # Training.
    "action_prob",
    "actual_value",
    "advantage",
    "selected_action",
])

def _action_entity_ids_to_addressable_entity_ids(action, entity_dict, pov_player):
    """
    Rewrites entity ids to addressable entity ids in place (in input `action`).
    """
    action_type = action.WhichOneof("action_oneof")
    entity_id_paths = _ACTION_ENTITY_ID_FIELDS_UNPROCESSED[action_type]

    for entity_id_path in entity_id_paths:
        try:
            leaf_attr = entity_id_path[-1]
            leaf_parent = _get_attr_or_idx(action, action_type)

            for interior_path_elem in entity_id_path[:-1]:
                leaf_parent = _get_attr_or_idx(leaf_parent, interior_path_elem)

            entity_id = _get_attr_or_idx(leaf_parent, leaf_attr)

            # NOTE: As a special case targetting entity -1 means "no target". We map this to
            # addressable entity index `N_ADDRESSABLE_ENTITIES`.
            if entity_id == -1:
                _set_attr_or_idx(leaf_parent, leaf_attr,
                                 sp.N_ADDRESSABLE_ENTITIES)
            else:
                target_entity = entity_dict[entity_id]
                entity_address = agent_util.entity_address(target_entity.state.location, pov_player)
                if entity_address == -1:
                    raise Exception(
                        "Action '{}' attempted to target invalid entity '{}'".format(
                            action, target_entity,
                        ))
                _set_attr_or_idx(leaf_parent, leaf_attr, entity_address)

        except IndexError:
            # This indicates a mulligan action with less than 4 discards.
            pass


def _compute_advantages(
        rewards,  # type: np.ndarray[n]
        predicted_values,  # type: np.ndarray[n+1]
):
    # type: (...) -> np.ndarray[n]
    """
    Computes GAE(gamma, lambda_), as described in https://arxiv.org/abs/1506.02438.
    """
    delta_t = rewards + FLAGS.gae_gamma * predicted_values[1:] - predicted_values[:-1]
    discounted_deltas = _discount(delta_t, FLAGS.gae_gamma * FLAGS.gae_lambda)
    summed_discounted_deltas = np.cumsum(discounted_deltas[::-1])[::-1]
    return _discount(summed_discounted_deltas, (1 / (FLAGS.gae_gamma * FLAGS.gae_lambda)))


def _create_input_pipeline(
        mode,  # type: str
        model_class,  # type: tf.keras.layers.Layer subclass
        policy_optimizer_class,  # type: ai.policy.optimizer.baseBasePolicyOptimizer subclass
):
    train_queue = None
    train_queue_close = None
    train_queue_closed_sentinel = None
    train_queue_min_size = None

    action_prob_placeholder = None
    actual_value_placeholder = None
    advantage_placeholder = None
    selected_action_placeholder = None

    with tf.variable_scope("input"):
        with tf.variable_scope("inference_placeholders"):
            (
                game_state_placeholder,
                unpack_aggregate_entities_placeholder,
                entity_to_np,
            ) = get_game_state_placeholders_and_packer(np.float32)
            (
                valid_action_placeholder,
                unpack_aggregate_action_placeholders,
                action_to_np,
            ) = get_action_placeholders_and_packer(np.float32)
            model_state_placeholder = model_class.get_state_placeholder()
            n_valid_action_placeholder = tf.placeholder(tf.int32, [None, 1], name="n_valid_action")

        # If training the model must also support an input path through the examples queue.
        if mode == "predict_or_train":
            (
                train_queue,
                train_queue_close,
                train_queue_closed_sentinel,
                train_queue_min_size,
            ) = policy_optimizer_class.create_train_queue()

            with tf.variable_scope("training_placeholders"):
                action_prob_placeholder = tf.placeholder(tf.float32, [None, 1], name="action_prob")
                actual_value_placeholder = tf.placeholder(tf.float32, [None, 1],
                                                          name="actual_value")
                advantage_placeholder = tf.placeholder(tf.float32, [None, 1], name="advantage")
                selected_action_placeholder = tf.placeholder(
                    tf.int32,
                    [None, 1],
                    name="selected_action_idx",
                )

        with tf.variable_scope("unpacked_placeholders"):
            with tf.variable_scope("game_state"):
                game_state_unpacked = unpack_aggregate_entities_placeholder(
                    game_state_placeholder)
            with tf.variable_scope("actions"):
                valid_action_unpacked = unpack_aggregate_action_placeholders(
                    valid_action_placeholder)

    samples_placeholder = QueueItem(
        game_state=game_state_placeholder,
        model_state=model_state_placeholder,
        n_valid_actions=n_valid_action_placeholder,
        valid_actions=valid_action_placeholder,

        action_prob=action_prob_placeholder,
        actual_value=actual_value_placeholder,
        advantage=advantage_placeholder,
        selected_action=selected_action_placeholder,
    )

    return (
        action_to_np,
        entity_to_np,
        game_state_placeholder,
        game_state_unpacked,
        model_state_placeholder,
        n_valid_action_placeholder,
        samples_placeholder,
        train_queue,
        train_queue_close,
        train_queue_closed_sentinel,
        train_queue_min_size,
        valid_action_placeholder,
        valid_action_unpacked,
    )


def _deserialize_and_pad_actions(
        actions,  # type: List[action_pb2.Action]
        entities,  # type: List[entity_pb2.Entity]
        deserializer,  # type: Callable[action_pb2.Action, np.ndarray[1, 1, <action size>]]
        pov_player, # type: player_pb2.Player_1
        max_n_actions=None,  # type: int
):
    # type: (...) -> np.ndarray[1, MAX_N_ACTIONS, <action size>]
    """
    Deserializes and pads `actions`.

    Args:
    - actions: (inflated/non-singular) Actions to pad and deserialize.
    - entities: The entities that these actions may reference (through entity ids).
    - deserializer: Function that turns action protos into (packed!) numpy arrays.
    - max_n_actions: Maximum number of actions to pad to. Should only be set during testing.

    Returns:
    - A single `np.ndarray` representing all input actions (padded with empty actions).
    """
    if max_n_actions == None:
        max_n_actions = sp.GET_MAX_N_ACTIONS()

    n_inflated_actions = len(actions)
    if n_inflated_actions > max_n_actions:
        logging.log_every_n_seconds(logging.WARN,
                                    "Received {} action (more than maximum {} supported).".format(
                                        n_inflated_actions, max_n_actions),
                                    5)

    # Translate entity ids into "addressable entity" indices.
    entity_dict = { e.id: e for e in entities }

    translated_actions = []
    for a in actions:
        # We copy the input actions to avoid mutating them.
        translated_action = action_pb2.Action()
        translated_action.CopyFrom(a)
        _action_entity_ids_to_addressable_entity_ids(translated_action, entity_dict, pov_player)
        translated_actions.append(translated_action)

    # Deserialize and pad actions.
    deserialized_inflated_actions_list = []
    for i in range(max_n_actions):
        if i < n_inflated_actions:
            deserialized_inflated_actions_list.append(
                deserializer(translated_actions[i]))
        else:  # We need to pad out with empty until `MAX_N_ACTIONS`.
            deserialized_inflated_actions_list.append(
                deserializer(action_pb2.Action()))

    return np.concatenate(deserialized_inflated_actions_list, axis=1)


def _deserialize_and_pad_entities(
        entities,  # type: List[entity_pb2.Entity]
        deserializer,  # type: Callable[entity_pb2.Entity, np.ndarray[1, 1, <entity size>]]
        pov_player,  # type: player_pb2.Player
):
    # type: (...) -> np.ndarray[1 x N_ADDRESSABLE_ENTITIES x <entity size>]
    """
    Deserializes and pads `entities`.
    NOTE: Currently ignores all-but-addressable entities.

    Args:
    - actions: Entities to pad and deserialize.
    - deserializer: Function that turns entity protos into numpy arrays.

    Returns:
    - A single `np.ndarray` representing all input entities (padded with empty entities).
    """
    deserialized_entities_list = sp.N_ADDRESSABLE_ENTITIES * [None]
    for e in entities:
        try:
            address = agent_util.entity_address(e.state.location, pov_player)
        except InvalidArgumentException:
            logging.warn("Failed to get entity address for '{}'.".format(e))
            raise

        if address < 0:
            # Skip unaddressable entities for now.
            continue
        if address >= len(deserialized_entities_list):
            raise IllegalStateException(
                "Unexpected entity address '{}' is larger than max '{}'.".format(
                    address, sp.N_ADDRESSABLE_ENTITIES - 1))

        deserialized_entities_list[address] = deserializer(e)

    for i in range(len(deserialized_entities_list)):
        # N.B.: We check types instead of values because `numpy` will "helpfully" broadcast a
        # direct comparison.
        if type(deserialized_entities_list[i]) == type(None):
            deserialized_entities_list[i] = deserializer(entity_pb2.Entity())

    return np.concatenate(deserialized_entities_list, axis=1)


def _discount(x, alpha, dtype=np.float32):
    # type: (...) -> Any
    alpha_relative_factors = np.full(x.shape, alpha, dtype=dtype)
    alpha_relative_factors[0] = 1
    alpha_pows = np.cumprod(alpha_relative_factors)
    return np.multiply(x, alpha_pows)


def _get_attr_or_idx(o, attr_or_idx):
    if type(attr_or_idx) == str:
        return getattr(o, attr_or_idx)
    else:
        return o[attr_or_idx]


def _minibatch_from_samples(samples):
    game_states = []
    model_states = []
    n_valid_actions = []
    valid_actions = []

    action_probs = []
    actual_values = []
    advantages = []
    selected_action_indices = []

    for sample in samples:
        game_states.append(sample.game_state)
        model_states.append(sample.model_state)
        n_valid_actions.append(sample.n_valid_actions)
        valid_actions.append(sample.valid_actions)

        action_probs.append(sample.action_prob)
        actual_values.append(sample.actual_value)
        advantages.append(sample.advantage)
        selected_action_indices.append(sample.selected_action)

    return QueueItem(
        # Inference.
        game_state=np.concatenate(game_states, axis=0),
        model_state=np.concatenate(model_states, axis=0),
        n_valid_actions=np.concatenate(n_valid_actions, axis=0),
        valid_actions=np.concatenate(valid_actions, axis=0),
        # Training.
        action_prob=np.concatenate(action_probs, axis=0),
        actual_value=np.concatenate(actual_values, axis=0),
        advantage=np.concatenate(advantages, axis=0),
        selected_action=np.concatenate(selected_action_indices, axis=0),
    )


def _rollout_to_samples(
        rollout,  # type: Rollout
):
    # type: (...) -> List[QueueItem[tf.Tensor]] (a batch of samples)
    rewards = np.asarray(rollout.rewards)

    # This is the sum of all future rewards from this state.
    actual_values = np.cumsum(rewards[::-1])[::-1]

    # We append a 0 to the end of values because the value of a terminal state is 0 (we've
    # already taken into account its reward in the `rewards` array).
    predicted_values = np.asarray(rollout.values + [0])
    advantages = _compute_advantages(rewards, predicted_values)

    samples = []
    for i in range(len(rollout.game_states)):
        samples.append(QueueItem(
            game_state=rollout.game_states[i],
            model_state=rollout.model_states[i],
            n_valid_actions=np.array([[ rollout.n_valid_actions[i] ]], np.int32),
            valid_actions=rollout.valid_actions[i],

            action_prob=np.array([[ rollout.selected_action_probs[i] ]], np.float32),
            actual_value=np.array([[ actual_values[i] ]], np.float32),
            advantage=np.array([[ advantages[i] ]], np.float32),
            selected_action=np.array([[ rollout.selected_action_indices[i] ]], np.int32),
        ))

    return samples


def _set_attr_or_idx(o, attr_or_idx, val):
    if type(attr_or_idx) == str:
        setattr(o, attr_or_idx, val)
    else:
        o[attr_or_idx] = val


class BasicPolicy(object):

    def __init__(
            self,
            beholder_var_fragments,  # type: List[str]
            policy_optimizer_class,
            mode,  # type: str
    ):
        # type: (...) -> None
        """
        Args:
        - beholder_var_fragments: List of regular expressions to match Beholder variables on.
        - mode: "predict_or_train" or "predict_only".
        """
        assert mode in ["predict_or_train", "predict_only"]
        self._act_instrumenter = maybe_instrument_tf("act")
        self._mode = mode
        self._policy_optimizer_class = policy_optimizer_class

        with tf.variable_scope("a3c_policy"):
            (
                self._action_to_np,
                self._entity_to_np,
                self._game_state_placeholder,
                game_state_unpacked,
                self._model_state_placeholder,
                self._n_valid_action_placeholder,
                samples_placeholder,
                self._train_queue,
                self._train_queue_close,
                self._train_queue_closed_sentinel,
                self._train_queue_min_size,
                self._valid_action_placeholder,
                valid_action_unpacked,
            ) = _create_input_pipeline(
                mode=mode,
                model_class=ComplexModel,
                policy_optimizer_class=policy_optimizer_class,
            )
            self._samples_placeholder_tuple = tuple(samples_placeholder)

            self._complex_model = ComplexModel(name="model")
            (
                self._output_action_probs,
                output_log_action_probs,
                self._value,
                self._model_state_output,
            ) = self._complex_model([
                game_state_unpacked,
                valid_action_unpacked,
                self._model_state_placeholder,
                self._n_valid_action_placeholder,
            ])

            # Other (non-queue) training-specific setup.
            if mode == "predict_or_train":
                self._beholder_var_fragments = beholder_var_fragments

                # Set up TF Loss Op.
                self._loss_op, _ = policy_optimizer_class.create_loss_op(
                    action_prob=samples_placeholder.action_prob,
                    actual_value=samples_placeholder.actual_value,
                    advantage=samples_placeholder.advantage,
                    output_action_probs=self._output_action_probs,
                    output_log_action_probs=output_log_action_probs,
                    output_value=self._value,
                    selected_action_idx=samples_placeholder.selected_action,
                )

            self.trainable_vars = tf.get_collection(
                tf.GraphKeys.TRAINABLE_VARIABLES,
                scope=tf.get_variable_scope().name,
            )

        super(BasicPolicy, self).__init__()

    def act(
            self,
            game_state_input,  # type: np.ndarray[None x N_ADDRESSABLE_ENTITIES x <entity size>]
            model_state_input,  # type: np.ndarray[None x GRU_NUM_UNITS]
            monitored_sess,  # type: tf.train.MonitoredSession
            n_valid_actions_input,  # type: np.ndarray[None x 1]
            valid_actions_input,  # type: Tuple[<this type>] np.ndarray[None x <action size>]
    ):
        # type: (...) -> (Action[tf.Tensor], tf.Tensor, tf.Tensor)
        fetches = [self._output_action_probs, self._model_state_output]
        if self._mode == "predict_or_train":
            fetches.append(self._value)
        def get_results(**kwargs):
            def step_fn(step_context):
                return step_context.session.run(
                    fetches, {
                        self._game_state_placeholder: game_state_input,
                        self._model_state_placeholder: model_state_input,
                        self._n_valid_action_placeholder: n_valid_actions_input,
                        self._valid_action_placeholder: valid_actions_input,
                    },
                    **kwargs
                )
            return monitored_sess.run_step_fn(step_fn)

        return self._act_instrumenter(get_results)

    def actions_to_np(
            self,
            actions,  # List[List[action_pb2.Action]]
            observations,  # List[observation_pb2.Observation]
            pov_player,  # type: player_pb2.Player
    ):
        # type: (...) -> np.ndarray[None, MAX_N_ACTIONS, <action size>]
        """
        Args:
        - actions: _inflated_ (singular) actions. Each inner list corresponds to actions for a
                   single game.
        - observations: .
        """
        deserialized_valid_actions_list = []
        for i in range(len(actions)):
            a = actions[i]
            entities = observations[i].game_state.entities
            deserialized_valid_actions_list.append(
                _deserialize_and_pad_actions(a, entities, self._action_to_np, pov_player))
        deserialized_valid_actions = np.concatenate(deserialized_valid_actions_list, axis=0)
        return deserialized_valid_actions

    def create_train_fn(self):
        # type: (...) -> None
        assert self._mode == "predict_or_train"

        beholder_vars = []
        if self._beholder_var_fragments:
            for trainable_var in self.trainable_vars:
                for beholder_var_fragment in self._beholder_var_fragments:
                    if beholder_var_fragment in trainable_var.name:
                        beholder_vars.append(trainable_var)

        extra_fetches = { "beholder_vars": beholder_vars }
        extra_check_numerics = ["encoder", "decoder"]

        return self._policy_optimizer_class.create_train_fn(
            extra_fetches=extra_fetches,
            extra_check_numerics=extra_check_numerics,
            minibatch_from_samples_fn=_minibatch_from_samples,
            samples_placeholder=self._samples_placeholder_tuple,
            train_queue=self._train_queue,
            train_queue_close=self._train_queue_close,
            train_queue_closed_sentinel=self._train_queue_closed_sentinel,

            loss=self._loss_op,
            source_vars=self.trainable_vars,
            target_vars=self.trainable_vars,
            global_step=tf.train.get_or_create_global_step(),
        )

    def enqueue_rollout(
            self,
            monitored_sess,  # type: tf.train.MonitoredSession
            rollout,
    ):
        samples = _rollout_to_samples(rollout)
        for sample in samples:
            self._train_queue.put((random.random(), sample), block=True,
                                  timeout=config.QUEUE_WAIT_TIME_SECONDS)

    def flush_training_queue(self, monitored_sess):
        while self._train_queue.qsize() > self._train_queue_min_size:
            logging.info(
                "Sleeping; training queue size is {} (waiting for <= {}).".format(
                    self._train_queue.qsize(), self._train_queue_min_size))
            time.sleep(2)

    def get_initial_model_state(self):
        return self._complex_model.initial_state

    def observations_to_np(
            self,
            observations,  # type: List[observation_pb2.Observation]
            pov_player,  # type: player_pb2.Player
    ):
        # type: (...) -> np.ndarray[<n observations> x N_ADDRESSABLE_ENTITIES x <entity size>]
        deserialized_entities_list = []
        for o_idx, o in enumerate(observations):
            try:
                deserialized_entities_list.append(
                    _deserialize_and_pad_entities(o.game_state.entities,
                                                  self._entity_to_np,
                                                  pov_player))
            except ProtogenException, InvalidArgumentException:
                logging.error("Error occurred deserializing entities '{}'.".format(
                    o.game_state.entities))
                raise
        deserialized_entities = np.concatenate(deserialized_entities_list, axis=0)
        return deserialized_entities

    def register_baseline_trainable_vars(self, baseline_vars):
        deltas = []
        for trainable_var, baseline_var in zip(self.trainable_vars, baseline_vars):
            deltas.append(trainable_var - baseline_var)
        tf.summary.scalar("model/delta_from_baseline", tf.global_norm(deltas))

    def register_error_saver(
            self,
            error_saver,  # type: tf.train.Saver
    ):
        self._policy_optimizer_class.register_error_saver(error_saver)

    def stop_trainer(self, monitored_sess):
        self._train_queue.put((0, self._train_queue_closed_sentinel), block=True,
                              timeout=config.QUEUE_WAIT_TIME_SECONDS)

        try:
            self._train_queue_close.put(None)
        except Queue.Empty:
            log.warn("Tried to signal on train_queue_close but it was full.")
