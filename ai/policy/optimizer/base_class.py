from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc


class BasePolicyOptimizer(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def create_loss_op(
            _,  # Ignored.
            actual_value,  # type: tf.Tensor
            advantage,  # type: tf.Tensor
            n_valid_actions,  # type: tf.Tensor
            output_action_logits,  # type: tf.Tensor
            output_value,  # type: tf.Tensor
            selected_action_idx,  # type: tf.Tensor
    ):
        # type: (...) -> Tuple[tf.Tensor, tf.Tensor]
        pass

    @abc.abstractmethod
    def create_train_fn(
            _,  # Ignored.
            batch_size,  # type: tf.Tensor[1, dtype=tf.float64]
            loss,  # type: tf.Tensor
            source_vars,  # type: List[tf.Variable]
            target_vars,  # type: List[tf.Variable]
            global_step,  # type: tf.Tensor
            max_grad_magnitude=49.0,
    ):
        # type: (...) -> Tuple[tf.Tensor, tf.Tensor]
        pass
