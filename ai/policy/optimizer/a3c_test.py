from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math

from absl import flags
import numpy as np
import pytest
import sys
import tensorflow as tf
tf.enable_eager_execution()
tfe = tf.contrib.eager

from ai.policy.optimizer.a3c import (
    A3cPolicyOptimizer,
)
import ai.tf_util as tf_util

FLAGS = flags.FLAGS


class TestCreateLossOp(object):
    def test_policy_loss(self):
        advantage = tf.constant([[3], [4]], dtype=tf.float32)
        selected_action_idx = tf.constant([[0], [1]], dtype=tf.int32)

        def loss_fn(output_action_probs_, output_log_action_probs_):
            return A3cPolicyOptimizer().create_loss_op(
                action_prob=tf.constant(output_action_probs_.shape[0]*[[1]], dtype=tf.float32),
                actual_value=tf.zeros([output_action_probs.shape[0], 1], dtype=tf.float32),
                advantage=advantage,
                output_action_probs=output_action_probs_,
                output_log_action_probs=output_log_action_probs_,
                output_value=tf.zeros([output_action_probs.shape[0]], dtype=tf.float32),
                selected_action_idx=selected_action_idx,
                # Zero out all loss components except policy loss.
                rel_value_loss_weight=0.0,
                rel_regularization_loss_weight=0.0,
            )[0]

        output_action_probs = tf.constant([[0.1, 0.4, 0.5], [0.3, 0.7, 0]], dtype=tf.float32)
        output_log_action_probs = tf.constant([
            [math.log(0.1), math.log(0.4), math.log(0.5)],
            [math.log(0.3), math.log(0.7), 1]], dtype=tf.float32)
        batch_size, max_n_actions = tuple(output_action_probs.shape.as_list())

        # Compute policy loss by hand:
        #   policy loss = - mean_over_turns( advantage * log( P(action | state) ) )
        loss_0 = -advantage[0][0] * output_log_action_probs[0, selected_action_idx[0][0]]
        loss_1 = -advantage[1][0] * output_log_action_probs[1, selected_action_idx[1][0]]
        expected_loss = (loss_0 + loss_1) / 2

        # Compute Tensorflow loss.
        actual_loss = loss_fn(output_action_probs, output_log_action_probs).numpy()

        # All losses and gradients must match.
        assert np.isclose(actual_loss, expected_loss)


if __name__ == "__main__":
    FLAGS(["asdf"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assertions.
        __file__,
    ]))
