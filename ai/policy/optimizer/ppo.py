from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import Queue
import random

from absl import flags
from absl import logging
import tensorflow as tf

from ai.policy.optimizer.base_class import (
    BasePolicyOptimizer,
)
from ai.tf_util import (
    assert_tensor_has_static_shape,
)
from ai.util import (
    add_check_numerics_ops,
    maybe_instrument_tf,
)
import ai.config as config
import ai.policy.optimizer.common as common
import ai.simulator_parameters as sp

FLAGS = flags.FLAGS
flags.DEFINE_float("ppo_max_proportion_clipped", 0.05, "")
flags.DEFINE_float("ppo_clip_eps", 0.02, "Max proportional divergence between action probs.")
flags.DEFINE_integer("ppo_batch_size", 4096, "Number of samples per iteration of PPO.")
flags.DEFINE_integer("ppo_n_epochs", 4, "Number of epochs per iteration of PPO.")


class PpoPolicyOptimizer(BasePolicyOptimizer):

    def __init__(self):
        assert FLAGS.ppo_batch_size % FLAGS.minibatch_size == 0
        self._error_saver = None

    def create_loss_op(
            self,
            action_prob,  # type: tf.Tensor
            actual_value,  # type: tf.Tensor
            advantage,  # type: tf.Tensor
            output_action_probs,  # type: tf.Tensor
            output_log_action_probs,  # type: tf.Tensor
            output_value,  # type: tf.Tensor
            selected_action_idx,  # type: tf.Tensor
            # For testing only.
            ppo_clip_eps=None,  # type: Optional[float]
            rel_value_loss_weight=None,  # type: Optional[float]
            rel_regularization_loss_weight=None,  # type: Optional[float]
    ):
        # type: (...) -> Tuple[tf.Tensor, tf.Tensor]
        if ppo_clip_eps == None:
            ppo_clip_eps = FLAGS.ppo_clip_eps
        if rel_value_loss_weight == None:
            rel_value_loss_weight = FLAGS.rel_value_loss_weight
        if rel_regularization_loss_weight == None:
            rel_regularization_loss_weight = FLAGS.rel_regularization_loss_weight

        batch_dim = output_action_probs.shape.as_list()[0]
        assert_tensor_has_static_shape(action_prob, (batch_dim, 1))
        assert_tensor_has_static_shape(actual_value, (batch_dim, 1))
        assert_tensor_has_static_shape(advantage, (batch_dim, 1))
        assert_tensor_has_static_shape(output_action_probs, (batch_dim, sp.GET_MAX_N_ACTIONS()))
        assert_tensor_has_static_shape(output_log_action_probs, (batch_dim, sp.GET_MAX_N_ACTIONS()))
        assert_tensor_has_static_shape(output_value, (batch_dim,))
        assert_tensor_has_static_shape(selected_action_idx, (batch_dim, 1))

        with tf.variable_scope("loss"):
            # Select the output action probability of the action taken.
            selected_action_onehot = tf.one_hot(selected_action_idx, sp.GET_MAX_N_ACTIONS(), axis=1)
            assert_tensor_has_static_shape(selected_action_onehot, (batch_dim, sp.GET_MAX_N_ACTIONS(), 1))
            selected_output_action_prob_fat = tf.matmul(tf.expand_dims(output_action_probs,
                                                                       axis=1),
                                                        selected_action_onehot)
            assert_tensor_has_static_shape(selected_output_action_prob_fat, (batch_dim, 1, 1))
            selected_output_action_prob = tf.squeeze(selected_output_action_prob_fat, axis=-1)
            assert_tensor_has_static_shape(selected_output_action_prob, (batch_dim, 1))

            # Determine ratio of output action prob to original action prob; clip it.
            ratio = tf.divide(selected_output_action_prob, action_prob)
            assert_tensor_has_static_shape(ratio, (batch_dim, 1))
            clipped_ratio = tf.clip_by_value(ratio, 1.0 - ppo_clip_eps, 1.0 + ppo_clip_eps)

            # The "policy gradients" loss. Note it's the negative of the objective in PPO since
            # we're minimizing instead of maximizing.
            policy_loss_per_turn = -tf.minimum(
                tf.multiply(advantage, ratio),
                tf.multiply(advantage, clipped_ratio),
            )
            policy_loss = tf.reduce_mean(policy_loss_per_turn)
            # NOTE: `actual_value` includes all (estimated) rewards from a state.
            value_loss = tf.reduce_mean(tf.square(actual_value - output_value))

            # Use negative entropy of each action as basic regularizer for now (we want to push the
            # policy to maximize entropy).
            regularization_loss = common.entropic_regularizer(output_action_probs,
                                                              output_log_action_probs)

            total_loss = \
                    policy_loss \
                    + rel_value_loss_weight * value_loss \
                    + rel_regularization_loss_weight * regularization_loss

            # Create loss-related summaries op.
            summary_op = None
            self._proportion_clipped = tf.reduce_mean(tf.to_float(tf.greater(tf.abs(ratio - 1.0),
                                                                             ppo_clip_eps)))
            if not tf.executing_eagerly():
                sortof_kl_div = -tf.reduce_mean(action_prob * tf.log(ratio))

                summary_op = tf.summary.merge([
                    tf.summary.scalar("model/policy_loss", policy_loss),
                    tf.summary.scalar("model/proportion_clipped", self._proportion_clipped),
                    tf.summary.scalar("model/regularization_loss", regularization_loss),
                    tf.summary.scalar("model/sortof_kl_div", sortof_kl_div),
                    tf.summary.scalar("model/total_loss", total_loss),
                    tf.summary.scalar("model/value_loss", value_loss),
                ])

        return (
            total_loss,
            summary_op,
        )

    def create_train_fn(
            self,
            extra_check_numerics,
            extra_fetches,
            minibatch_from_samples_fn,
            samples_placeholder,
            train_queue,
            train_queue_close,
            train_queue_closed_sentinel,
            # Passed through to `create_default_train_op`
            loss,  # type: tf.Tensor
            source_vars,  # type: List[tf.Variable]
            target_vars,  # type: List[tf.Variable]
            global_step,  # type: tf.Tensor
            ):
        n_minibatches = FLAGS.ppo_batch_size / FLAGS.minibatch_size

        train_instrumenter = maybe_instrument_tf("train")
        bare_train_op, _ = common.create_default_train_op(
            batch_size=FLAGS.minibatch_size,
            loss=loss,
            source_vars=source_vars,
            target_vars=target_vars,
            global_step=global_step,
        )
        with tf.control_dependencies([bare_train_op]):
            train_op = global_step.assign_add(int(FLAGS.ppo_batch_size / n_minibatches))

        train_fetches = {
            "proportion_clipped": self._proportion_clipped,
            "train": train_op,
        }
        if FLAGS.check_numerics:
            train_fetches["check_numerics"] = add_check_numerics_ops(["loss"] +
                                                                     extra_check_numerics)
        final_fetches = extra_fetches

        def train(
                monitored_sess,  # type: tf.train.MonitoredSession
        ):
            # type: (...) -> tf.Tensor
            assert train_op != None

            batch = []
            queue_closed = False
            for _ in range(FLAGS.ppo_batch_size):
                _, queue_elem = train_queue.get(block=True, timeout=config.QUEUE_WAIT_TIME_SECONDS)
                if queue_elem == train_queue_closed_sentinel:
                    queue_closed = True
                    break
                batch.append(queue_elem)

            if len(batch) > 0:
                random.shuffle(batch)
                minibatches = []
                for s in range(0, len(batch), FLAGS.minibatch_size):
                    samples = batch[s:s + FLAGS.minibatch_size]
                    minibatches.append(minibatch_from_samples_fn(samples))

                for _ in range(FLAGS.ppo_n_epochs):
                    sum_proportion_clipped = 0

                    for minibatch in minibatches:
                        def run_train_op(**kwargs):
                            error_saver = self._error_saver
                            with common.capture_checknumerics_errors(error_saver, minibatch,
                                                                     monitored_sess):

                                # Uncomment to figure out where NaNs / Infs are coming from
                                # import cPickle as pickle
                                # f = open("/srv/nfs/190113-gsvb1m-39a279/i7643ca884cbdfea/exception_minibatch_fd9667fd-2b30-4d52-9f95-26c780a88f7c.pkl")
                                # mb = pickle.load(f)
                                # def get_load_model_fn(ckpt_file):
                                #     def load_model_fn(step_context):
                                #         error_saver.restore(step_context.session, ckpt_file)
                                #     return load_model_fn

                                # monitored_sess.run_step_fn(get_load_model_fn("/srv/nfs/190113-gsvb1m-39a279/i7643ca884cbdfea/exception_model_a615700f-17d7-4029-85f9-c6bab7c60216.ckpt"))

                                # from tensorflow.python import debug as tf_debug
                                # dbg_sess = tf_debug.LocalCLIDebugWrapperSession(monitored_sess)
                                # tuple_mb = tuple(mb)
                                # dbg_fs = { k: v for k, v in train_fetches.items() if k !=
                                #                "check_numerics" }
                                # dbg_feeds = { samples_placeholder[i]: tuple_mb[i] for i in range(len(samples_placeholder)) }
                                #dbg_sess.run(dbg_fs, feed_dict=dbg_feeds, **kwargs)

                                return monitored_sess.run(train_fetches,
                                                          feed_dict={
                                                              samples_placeholder: minibatch,
                                                          },
                                                          **kwargs)

                        sum_proportion_clipped += train_instrumenter(run_train_op)[
                            "proportion_clipped"]

                    # If there is fresh data use that.
                    if train_queue.qsize() >= FLAGS.ppo_batch_size:
                        break

                    # Apply adaptive learning rate if requested.
                    if FLAGS.ppo_max_proportion_clipped > 0:
                        proportion_clipped = sum_proportion_clipped / len(minibatches)
                        if proportion_clipped > FLAGS.ppo_max_proportion_clipped:
                            break

                logging.debug("Trained a PPO batch.")

            def get_final_fetches(**kwargs):
                def step_fn(step_context):
                    return step_context.session.run(final_fetches, **kwargs)
                return monitored_sess.run_step_fn(step_fn)
            return train_instrumenter(get_final_fetches), queue_closed

        return train

    def create_train_queue(_):
        queue = Queue.Queue(maxsize=FLAGS.ppo_batch_size)
        queue_close = Queue.Queue(maxsize=1)
        train_queue_closed_sentinel = object()
        return queue, queue_close, train_queue_closed_sentinel, 0

    def register_error_saver(
            self,
            error_saver,  # type: tf.train.Saver
    ):
        self._error_saver = error_saver
