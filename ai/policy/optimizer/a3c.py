from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import Queue
import time

from absl import flags
from absl import logging
import tensorflow as tf

from ai.policy.optimizer.base_class import (
    BasePolicyOptimizer,
)
from ai.tf_util import (
    assert_tensor_has_static_shape,
)
from ai.util import (
    add_check_numerics_ops,
    maybe_instrument_tf,
)
from base.shuffle_queue import (
    ShuffleQueue,
)
import ai.config as config
import ai.policy.optimizer.common as common
import ai.simulator_parameters  # for sanity_check flag

FLAGS = flags.FLAGS
flags.DEFINE_integer("a3c_train_buffer_size",
                     2048,
                     "Minimum number of elements allowed in training queue.")

def _GET_MIN_TRAIN_QUEUE_SIZE():
    if FLAGS.sanity_check:
        return min(256, FLAGS.a3c_train_buffer_size)
    else:
        return FLAGS.a3c_train_buffer_size


class A3cPolicyOptimizer(BasePolicyOptimizer):

    def __init__(self):
        if FLAGS.sanity_check:
            FLAGS.a3c_train_buffer_size = 256
        self._error_saver = None

    def create_loss_op(
            _,  # ignored
            action_prob,  # type: tf.Tensor
            actual_value,  # type: tf.Tensor
            advantage,  # type: tf.Tensor
            output_action_probs,  # type: tf.Tensor
            output_log_action_probs,  # type: tf.Tensor
            output_value,  # type: tf.Tensor
            selected_action_idx,  # type: tf.Tensor
            # For testing only.
            rel_value_loss_weight=None,  # type: Optional[float]
            rel_regularization_loss_weight=None,  # type: Optional[float]
    ):
        # type: (...) -> Tuple[tf.Tensor, tf.Tensor]
        if rel_value_loss_weight == None:
            rel_value_loss_weight = FLAGS.rel_value_loss_weight
        if rel_regularization_loss_weight == None:
            rel_regularization_loss_weight = FLAGS.rel_regularization_loss_weight

        batch_dim, max_n_actions = output_action_probs.shape.as_list()
        assert_tensor_has_static_shape(action_prob, (batch_dim, 1))
        assert_tensor_has_static_shape(actual_value, (batch_dim, 1))
        assert_tensor_has_static_shape(advantage, (batch_dim, 1))
        assert_tensor_has_static_shape(output_action_probs, (batch_dim, max_n_actions))
        assert_tensor_has_static_shape(output_log_action_probs, (batch_dim, max_n_actions))
        assert_tensor_has_static_shape(output_value, (batch_dim,))
        assert_tensor_has_static_shape(selected_action_idx, (batch_dim, 1))

        with tf.variable_scope("loss"):
            # Select the output log action probability of the action taken.
            selected_action_onehot = tf.one_hot(selected_action_idx, max_n_actions, axis=1)
            assert_tensor_has_static_shape(selected_action_onehot, (batch_dim, max_n_actions, 1))
            selected_log_action_prob_fat = tf.matmul(tf.expand_dims(output_log_action_probs, axis=1),
                                                     selected_action_onehot)
            assert_tensor_has_static_shape(selected_log_action_prob_fat, (batch_dim, 1, 1))
            selected_log_action_prob = tf.squeeze(selected_log_action_prob_fat, axis=-1)
            assert_tensor_has_static_shape(selected_log_action_prob, (batch_dim, 1))

            # The "policy gradients" loss. Constructed such that its derivative is precisely the
            # policy gradient.
            policy_loss_per_turn = -tf.multiply(advantage, selected_log_action_prob)
            policy_loss = tf.reduce_mean(policy_loss_per_turn)
            # NOTE: `actual_value` includes all (estimated) rewards from a state.
            value_loss = tf.reduce_mean(tf.square(actual_value - output_value))

            # Use negative entropy of each action as basic regularizer for now (we want to push the
            # policy to maximize entropy).
            regularization_loss = common.entropic_regularizer(output_action_probs,
                                                              output_log_action_probs)

            total_loss = \
                    policy_loss \
                    + rel_value_loss_weight * value_loss \
                    + rel_regularization_loss_weight * regularization_loss

            # Create loss-related summaries op.
            summary_op = None
            if not tf.executing_eagerly():
                summary_op = tf.summary.merge([
                    tf.summary.scalar("model/policy_loss", policy_loss),
                    tf.summary.scalar("model/regularization_loss", regularization_loss),
                    tf.summary.scalar("model/total_loss", total_loss),
                    tf.summary.scalar("model/value_loss", value_loss),
                ])

        return (
            total_loss,
            summary_op,
        )

    def create_train_fn(
            self,
            extra_check_numerics,
            extra_fetches,
            minibatch_from_samples_fn,
            samples_placeholder,
            train_queue,
            train_queue_close,
            train_queue_closed_sentinel,
            # Passed through to `create_default_train_op`
            loss,  # type: tf.Tensor
            source_vars,  # type: List[tf.Variable]
            target_vars,  # type: List[tf.Variable]
            global_step,  # type: tf.Tensor
            ):

        train_instrumenter = maybe_instrument_tf("train")
        train_op, _ = common.create_default_train_op(
            batch_size=FLAGS.minibatch_size,
            loss=loss,
            source_vars=source_vars,
            target_vars=target_vars,
            global_step=global_step,
        )

        fetches = dict(
            train_op=train_op,
            update_global_step=global_step.assign_add(FLAGS.minibatch_size),
            **extra_fetches)
        if FLAGS.check_numerics:
            fetches["check_numerics_op"] = add_check_numerics_ops(["loss"] + extra_check_numerics)

        def train(
                sess,  # type: tf.train.MonitoredSession
        ):
            # type: (...) -> tf.Tensor
            assert train_op != None
            queue_closed = False

            while train_queue.qsize() <= FLAGS.a3c_train_buffer_size:
                try:
                    train_queue_close.get_nowait()
                    # If we got a close via close queue then finish up the last training batch.
                    queue_closed = True
                    break
                except Queue.Empty:
                    # If nothing was on the close queue keep blocking.
                    pass
                time.sleep(2)
            train_queue.shuffle()

            samples = []
            for _ in range(FLAGS.minibatch_size):
                _, queue_elem = train_queue.get(block=True, timeout=config.QUEUE_WAIT_TIME_SECONDS)
                if queue_elem == train_queue_closed_sentinel:
                    queue_closed = True
                    break
                samples.append(queue_elem)
            logging.debug(
                "A3c dequeued samples; remaining queue size is {}".format(train_queue.qsize()))

            if len(samples) > 0:
                minibatch = minibatch_from_samples_fn(samples)
                def get_results(**kwargs):
                    error_saver = self._error_saver
                    with common.capture_checknumerics_errors(error_saver, minibatch, sess):

                        # Uncomment to figure out where NaNs / Infs are coming from
                        # import cPickle as pickle
                        # f = open("/srv/nfs/190113-gsvb1m-a9e82f/s3e93fdc8877d68d/exception_minibatch_2d8ba926-7598-4aa7-81ac-18f9eee8b66f.pkl")
                        # mb = pickle.load(f)
                        # def get_load_model_fn(ckpt_file):
                        #     def load_model_fn(step_context):
                        #         error_saver.restore(step_context.session, ckpt_file)
                        #     return load_model_fn

                        # sess.run_step_fn(get_load_model_fn("/srv/nfs/190113-gsvb1m-a9e82f/s3e93fdc8877d68d/exception_model_f6340719-80ba-44b2-86ae-bdf95666b08f.ckpt"))

                        # from tensorflow.python import debug as tf_debug
                        # dbg_sess = tf_debug.LocalCLIDebugWrapperSession(sess)
                        # tuple_mb = tuple(mb)
                        # dbg_fs = { k: v for k, v in fetches.items() if k !=
                        #                "check_numerics_op" }
                        # dbg_feeds = { samples_placeholder[i]: tuple_mb[i] for i in range(len(samples_placeholder)) }
                        # dbg_sess.run(dbg_fs, feed_dict=dbg_feeds, **kwargs)

                        return sess.run(fetches,
                                        feed_dict={samples_placeholder: minibatch},
                                        **kwargs)

                return train_instrumenter(get_results), queue_closed

            else:
                return None, queue_closed

        return train

    def create_train_queue(_):
        queue = ShuffleQueue(maxsize=FLAGS.a3c_train_buffer_size + 2 * FLAGS.minibatch_size)
        queue_close = Queue.Queue(maxsize=1)
        train_queue_closed_sentinel = object()
        return queue, queue_close, train_queue_closed_sentinel, FLAGS.a3c_train_buffer_size

    def register_error_saver(
            self,
            error_saver,  # type: tf.train.Saver
    ):
        self._error_saver = error_saver
