from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math

import numpy as np
import pytest
import scipy.stats
import sys
import tensorflow as tf
tf.enable_eager_execution()
tfe = tf.contrib.eager

from ai.policy.optimizer.common import (
    entropic_regularizer,
)


class TestEntropicRegularizer(object):
    def test_basic(self):
        actual_value = tf.constant([[1], [2]], dtype=tf.float32)
        advantage = tf.constant([[3], [4]], dtype=tf.float32)
        n_valid_actions = tf.constant([[3], [2]], dtype=tf.int32)
        output_value = tf.constant([1.2, 2.2], dtype=tf.float32)
        selected_action_idx = tf.constant([[1], [1]], dtype=tf.int32)

        # The loss increases when probs have less entropy.
        low_entropy_probs = tf.constant([[0.8, 0.1, 0.1], [0.9, 0.1, 0]], dtype=tf.float32)
        low_entropy_log_probs = tf.constant([
            [math.log(0.8), math.log(0.1), math.log(0.1)],
            [math.log(0.9), math.log(0.1), 0]], dtype=tf.float32)
        low_entropy_output = entropic_regularizer(low_entropy_probs,
                                                  low_entropy_log_probs).numpy()
        high_entropy_probs = tf.constant([[0.3, 0.3, 0.4], [0.5, 0.5, 0]], dtype=tf.float32)
        high_entropy_log_probs = tf.constant([
            [math.log(0.3), math.log(0.3), math.log(0.1)],
            [math.log(0.5), math.log(0.5), 0]], dtype=tf.float32)
        high_entropy_output = entropic_regularizer(high_entropy_probs,
                                                   high_entropy_log_probs).numpy()
        assert low_entropy_output > high_entropy_output

        # A check that it ignores "dummy" log probabilities.
        probs1 = tf.constant([[0.2, 0.2, 0.6], [0.5, 0.5, 0]], dtype=tf.float32)
        log_probs1 = tf.constant([
            [math.log(0.2), math.log(0.2), math.log(0.6)],
            [math.log(0.5), math.log(0.5), 1]], dtype=tf.float32)
        out1 = entropic_regularizer(probs1, log_probs1).numpy()
        probs2 = tf.constant([[0.2, 0.2, 0.6], [0.5, 0.5, 0]], dtype=tf.float32)
        log_probs2 = tf.constant([
            [math.log(0.2), math.log(0.2), math.log(0.6)],
            [math.log(0.5), math.log(0.5), 3]], dtype=tf.float32)
        out2 = entropic_regularizer(probs2, log_probs2).numpy()
        assert out1 == out2

        # Entropy is correct for two uniformly likely actions.
        probs = tf.constant([3*[float(1)/3], [0.5, 0.5, 0]], dtype=tf.float32)
        log_probs = tf.constant([3*[math.log(float(1)/3)],
                                 [math.log(0.5), math.log(0.5), math.log(2)]], dtype=tf.float32)
        entropy = (scipy.stats.entropy(probs[0, :]) + scipy.stats.entropy(probs[1, :2])) / 2
        assert np.isclose(-entropy, entropic_regularizer(probs, log_probs).numpy())


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assertions.
        __file__,
    ]))
