from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags
import numpy as np
import pytest
import sys
import tensorflow as tf
tf.enable_eager_execution()
tfe = tf.contrib.eager

from ai.policy.optimizer.ppo import (
    PpoPolicyOptimizer,
)
import ai.simulator_parameters

FLAGS = flags.FLAGS


class TestCreateLossOp(object):
    def test_policy_loss_sanity_check(self, monkeypatch):
        monkeypatch.setattr(ai.simulator_parameters, "GET_MAX_N_ACTIONS", lambda: 2)
        selected_action_idx = tf.constant([[0], [1]], dtype=tf.int32)

        def loss_fn(advantage, action_probs, output_action_probs, epsilon):
            return PpoPolicyOptimizer().create_loss_op(
                action_prob=action_probs,
                actual_value=tf.zeros([output_action_probs.shape[0], 1], dtype=tf.float32),
                advantage=advantage,
                output_action_probs=output_action_probs,
                output_log_action_probs=tf.zeros(output_action_probs.shape, dtype=tf.float32),
                output_value=tf.zeros([output_action_probs.shape[0]], dtype=tf.float32),
                selected_action_idx=selected_action_idx,
                # Zero out all loss components except policy loss.
                ppo_clip_eps=epsilon,
                rel_value_loss_weight=0.0,
                rel_regularization_loss_weight=0.0,
            )[0]

        # Clips when too low.
        action_prob1 = tf.constant([[0.01], [0.1]], dtype=tf.float32)
        output_action_prob1 = tf.constant([[0.1, 0.2], [0.3, 0.4]], dtype=tf.float32)
        advantage1 = tf.constant([[1], [2]], dtype=tf.float32)
        assert loss_fn(advantage1, action_prob1, output_action_prob1, 1).numpy() == -3

        # Clips appropriately when some samples losses are high and some low.
        action_prob1 = tf.constant([[0.01], [0.01]], dtype=tf.float32)
        output_action_prob1 = tf.constant([[0.1, 0.2], [0.3, 0.4]], dtype=tf.float32)
        advantage1 = tf.constant([[1], [-1]], dtype=tf.float32)
        assert loss_fn(advantage1, action_prob1, output_action_prob1, 1).numpy() == 19


if __name__ == "__main__":
    FLAGS(["asdf"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assertions.
        __file__,
    ]))
