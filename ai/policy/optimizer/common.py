from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import contextlib
import cPickle as pickle
import os
import uuid

from absl import flags
from absl import logging
import numpy as np
import tensorflow as tf

FLAGS = flags.FLAGS
flags.DEFINE_bool("check_numerics", False, "")
flags.DEFINE_enum("optimizer",
                  tf.train.GradientDescentOptimizer.__name__,
                  [o.__name__ for o in [
                      tf.train.AdadeltaOptimizer,
                      tf.train.AdagradOptimizer,
                      tf.train.AdamOptimizer,
                      tf.train.GradientDescentOptimizer,
                      tf.train.RMSPropOptimizer]],
                  "Optimizer to use during training.")
flags.DEFINE_float("base_learning_rate", 5e-4, "Learning rate.")
flags.DEFINE_float("max_grad_magnitude",
                   20.0,
                   "Clip gradients to this magnitude if they exceed it.")
flags.DEFINE_float("rel_value_loss_weight",
                   0.005,
                   "Relative weight of value loss (wrt policy loss).")
flags.DEFINE_float("rel_regularization_loss_weight",
                   0.01,
                   "Relative weight of regularization loss (wrt policy loss).")
flags.DEFINE_integer("minibatch_size",
                     64,
                     "Number of samples to use per backprop pass.")


@contextlib.contextmanager
def capture_checknumerics_errors(
        error_saver,  # type: Optional[tf.train.Saver]
        minibatch,  # type: Any
        monitored_sess,  # type: tf.train.MonitoredSession
):
    try:
        yield

    except tf.errors.InvalidArgumentError as e:
        if "CheckNumerics" in e.message:
            logging.error(
                "Experience numerical instability. CheckNumerics' inputs"
                + " were '{}'.".format([i.name for i in e.op.inputs]))

            minibatch_file = os.path.join(
                FLAGS.output_dir,
                "exception_minibatch_{}.pkl".format(uuid.uuid4()))
            with open(minibatch_file, "wb") as f:
                pickle.dump(minibatch, f)

            logging.info(
                "Saved offending minibatch to '{}'.".format(minibatch_file))

            if error_saver != None:
                ckpt_file = os.path.join(
                    FLAGS.output_dir,
                    "exception_model_{}.ckpt".format(uuid.uuid4()))
                def step_fn(step_context):
                    error_saver.save(step_context.session, ckpt_file)
                monitored_sess.run_step_fn(step_fn)

                logging.info("Saved offending model to '{}'.".format(ckpt_file))

        raise


def create_default_train_op(
        batch_size,  # type: int
        loss,  # type: tf.Tensor
        source_vars,  # type: List[tf.Variable]
        target_vars,  # type: List[tf.Variable]
        global_step,  # type: tf.Tensor
):
    # type: (...) -> Tuple[tf.Tensor, tf.Tensor]
    """
    Args:
    - batch_size:
    - loss:
    - source_vars: The variables to compute gradient wrt.
    - target_vars: The variables to update with gradient.
    - global_step: Global step variable to update when training using this batch.

    Note: Does not create any weights; I do not think it needs to be used in a scope.
    """
    with tf.variable_scope("train"):
        # Apply clipped gradients.
        grads = tf.gradients(loss, source_vars)
        clipped_grads, _ = tf.clip_by_global_norm(grads, FLAGS.max_grad_magnitude)
        grads_and_target_vars = list(zip(clipped_grads, target_vars))

        logging.debug("Vars that will actually be trained:")
        for v in [v for _, v in grads_and_target_vars]:
            logging.debug("  %s %s", v.name, v.get_shape())

        optimizer = getattr(tf.train, FLAGS.optimizer)(
            learning_rate=FLAGS.base_learning_rate * tf.cast(batch_size, tf.float32))

        summary_op = tf.summary.merge([
            tf.summary.scalar("model/grad_global_norm", tf.global_norm(grads)),
            tf.summary.scalar("model/learning_rate", FLAGS.base_learning_rate),
            tf.summary.scalar("model/clipped_grad_global_norm", tf.global_norm(clipped_grads)),
            tf.summary.scalar("model/var_global_norm", tf.global_norm(source_vars)),
        ])

        with tf.control_dependencies([optimizer.apply_gradients(grads_and_target_vars)]):
            # Train op blocks on gradient application then increments the global step by batch size.
            train_op = global_step.assign_add(batch_size)

        return (
            train_op,
            summary_op,
        )


def entropic_regularizer(probs, log_probs):
    entropy = -tf.reduce_sum(probs * log_probs, axis=1)
    return -tf.reduce_mean(entropy)
