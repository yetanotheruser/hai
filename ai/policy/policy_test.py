from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags
import google.protobuf.text_format as text_format
import numpy as np
import pytest
import sys
import tensorflow as tf
tf.enable_eager_execution()
tfe = tf.contrib.eager

from ai.agent.util import (
    entity_address,
    Rollout,
)
from ai.policy.policy import (
    QueueItem,
    DEFAULT_GAMMA,
    DEFAULT_LAMBDA,
    _action_entity_ids_to_addressable_entity_ids,
    _compute_advantages,
    _deserialize_and_pad_actions,
    _discount,
    _rollout_to_samples,
)
import ai.deserialization.action
import ai.simulator_parameters as sp
import ai.tf_util as tf_util
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.entity_pb2 as entity_pb2
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.entity_state_pb2 as entity_state_pb2
import spellsource.proto.player_pb2 as player_pb2
import spellsource.proto.target_action_pair_pb2 as target_action_pair_pb2
import spellsource.proto.spell_action_pb2 as spell_action_pb2

FLAGS = flags.FLAGS


@pytest.fixture
def fake_rollout_and_samples():
    rollout = Rollout()
    rollout.add(
        game_state=np.array([[3, 3]]),
        model_state=np.array([[5,5]]),
        n_valid_actions=2,
        reward=3.0,
        selected_action_idx=1,
        selected_action_prob=0.6,
        valid_actions=np.array([[
            [1, 1],
            [2, 2],
            [3, 3],
        ]]),
        value=10.0,
    )
    rollout.add(
        game_state=np.array([[8, 8]]),
        model_state=np.array([[10,10]]),
        n_valid_actions=1,
        reward=11.0,
        selected_action_idx=0,
        selected_action_prob=0.7,
        valid_actions=np.array([[
            [4, 4],
            [5, 5],
            [6, 6],
        ]]),
        value=13.0,
    )

    delta_0 = 3 + DEFAULT_GAMMA * 13 - 10
    delta_1 = 11 + DEFAULT_GAMMA * 0 - 13
    expected_samples = [
        QueueItem(
            game_state=np.array([[3, 3]]),
            model_state=np.array([[5, 5]]),
            n_valid_actions=np.array([[2]], np.int32),
            valid_actions=np.array([[
                [1, 1],
                [2, 2],
                [3, 3],
            ]]),

            action_prob=np.array([[0.6]]),
            actual_value=np.array([[14.0]]),
            advantage=np.array([[delta_0 + DEFAULT_LAMBDA * DEFAULT_GAMMA * delta_1]]),
            selected_action=np.array([[1]], np.int32),
        ),
        QueueItem(
            game_state=np.array([[8, 8]]),
            model_state=np.array([[10, 10]]),
            n_valid_actions=np.array([[1]], np.int32),
            valid_actions=np.array([[
                [4, 4],
                [5, 5],
                [6, 6],
            ]]),

            action_prob=np.array([[0.7]]),
            actual_value=np.array([[11.0]]),
            advantage=np.array([[delta_1]]),
            selected_action=np.array([[0]], np.int32),
        ),
    ]

    return rollout, expected_samples


# Populating shared global (read only!) variable for tests to use.
entity1 = entity_pb2.Entity()
text_format.Parse(
    "id: 1\n"
    "state {\n"
    "    location {\n"
    "        player: 0\n"
    "        index: 2\n"
    "        zone: HAND\n"
    "    }\n"
    "}\n", entity1)
entity2 = entity_pb2.Entity()
text_format.Parse(
    "id: 2\n"
    "state {\n"
    "    location {\n"
    "        player: 0\n"
    "        zone: WEAPON\n"
    "    }\n"
    "}\n", entity2)
entity3 = entity_pb2.Entity()
text_format.Parse(
    "id: 3\n"
    "state {\n"
    "    location {\n"
    "        player: 1\n"
    "        zone: HERO\n"
    "    }\n"
    "}\n", entity3)
entity4 = entity_pb2.Entity()
text_format.Parse(
    "id: 4\n"
    "state {\n"
    "    location {\n"
    "        player: 1\n"
    "        index: 3\n"
    "        zone: BATTLEFIELD\n"
    "    }\n"
    "}\n", entity4)
ENTITY_DICT = {
    1: entity1,
    2: entity2,
    3: entity3,
    4: entity4,
}
class Test_ActionEntityIdsToAddressableEntityIds(object):
    @pytest.mark.parametrize("in_str,in_pov_player,expected_out_str", [(
        "discover {\n"
        "    card_id: 1\n"
        "}\n",
        player_pb2.Player_1,
        "discover {\n" +
        "    card_id: {}\n".format(entity_address(entity1.state.location, player_pb2.Player_1)) +
        "}\n"
    ), (
        "mulligan {\n"
        "    discard_entity_ids: 1\n"
        "    keep_entity_ids: 2\n"
        "}\n",
        player_pb2.Player_1,
        "mulligan {\n" +
        "    discard_entity_ids: {}\n".format(entity_address(entity1.state.location,
                                                             player_pb2.Player_1)) +
        "    keep_entity_ids: {}\n".format(entity_address(entity2.state.location,
                                                          player_pb2.Player_1)) +
        "}\n"
    ), (
        "physical_attack {\n"
        "    source_id: 2\n"
        "    defenders {\n"
        "        target: 4\n"
        "    }\n"
        "}\n",
        player_pb2.Player_1,
        "physical_attack {\n" +
        "    source_id: {}\n".format(entity_address(entity2.state.location, player_pb2.Player_1)) +
        "    defenders {\n" +
        "        target: {}\n".format(entity_address(entity4.state.location, player_pb2.Player_1)) +
        "    }\n" +
        "}\n"
    ), (
        "battlecry {\n"
        "    source_id: 2\n"
        "    target_key_to_actions {\n"
        "        target: 3\n"
        "    }\n"
        "}\n",
        player_pb2.Player_1,
        "battlecry {\n" +
        "    source_id: {}\n".format(entity_address(entity2.state.location, player_pb2.Player_1)) +
        "    target_key_to_actions {\n" +
        "        target: {}\n".format(entity_address(entity3.state.location, player_pb2.Player_1)) +
        "    }\n" +
        "}\n"
    ), (
        "spell {\n"
        "    source_id: 1\n"
        "    target_key_to_actions {\n" +
        "        target: -1\n" +
        "    }\n" +
        "}\n",
        player_pb2.Player_1,
        "spell {\n" +
        "    source_id: {}\n".format(entity_address(entity1.state.location, player_pb2.Player_1)) +
        "    target_key_to_actions {\n" +
        "        target: {}\n".format(sp.N_ADDRESSABLE_ENTITIES) +
        "    }\n" +
        "}\n"
    ), (
        "summon {\n"
        "    source_id: 1\n"
        "}\n",
        player_pb2.Player_1,
        "summon {\n" +
        "    source_id: {}\n".format(entity_address(entity1.state.location, player_pb2.Player_1)) +
        "}\n"
    )], ids=[
        "discover",
        "mulligan",
        "physical_attack",
        "spell",
        "spell_notarget",
        "summon",
    ])
    def test_template(self, in_str, in_pov_player, expected_out_str):
        in_ = action_pb2.Action()
        text_format.Parse(in_str, in_)
        expected_out = action_pb2.Action()
        text_format.Parse(expected_out_str, expected_out)
        out = action_pb2.Action()
        out.CopyFrom(in_)
        _action_entity_ids_to_addressable_entity_ids(out, ENTITY_DICT, in_pov_player)
        assert out == expected_out


class Test_ComputeAdvantages(object):
    def test_basic(self):
        rewards = np.array([3, 11])
        predicted_values = np.array([10, 13, 0])
        delta_0 = 3 + DEFAULT_GAMMA * 13 - 10
        delta_1 = 11 + DEFAULT_GAMMA * 0 - 13

        expected_out = np.array([delta_0 + DEFAULT_LAMBDA * DEFAULT_GAMMA * delta_1, delta_1])
        assert np.allclose(_compute_advantages(rewards, predicted_values), expected_out)


class Test_DeserializeAndPadActions(object):
    def test_entity_id_substitution(self):
        actions = [
            action_pb2.Action(
                battlecry=spell_action_pb2.SpellAction(
                    source_id=1,
                    target_key_to_actions=[
                        target_action_pair_pb2.TargetActionPair(target=2)])),
            action_pb2.Action(
                battlecry=spell_action_pb2.SpellAction(
                    source_id=2)),
        ]
        entities = [
            entity_pb2.Entity(
                id=1,
                state=entity_state_pb2.EntityState(
                    location=entity_location_pb2.EntityLocation(  # addressable index 1
                        index=1,
                        player=0,
                        zone=entity_location_pb2.EntityLocation.HAND,
                    ))),
            entity_pb2.Entity(
                id=2,
                state=entity_state_pb2.EntityState(
                    location=entity_location_pb2.EntityLocation(  # addressable index 18
                        index=1,
                        player=1,
                        zone=entity_location_pb2.EntityLocation.BATTLEFIELD,
                    ))),
        ]
        def deserializer(action):
            ret = np.array([[[action.battlecry.source_id, 0]]], np.float32)
            if len(action.battlecry.target_key_to_actions) > 0:
                ret[0][0][1] = action.battlecry.target_key_to_actions[0].target
            return ret
        out = _deserialize_and_pad_actions(actions, entities, deserializer, player_pb2.Player_1,
                                           max_n_actions=3)

        expected_out = np.array([[[1, 18],
                                  [18, 0],
                                  [0, 0]]], np.float32)
        assert (out == expected_out).all()


class Test_Discount(object):
    def test_basic(self):
        x = np.array([1, 2, 4])
        alpha = 0.5
        expected_out = np.array([1, 1, 1])
        assert np.allclose(_discount(x, alpha), expected_out)


class TestRolloutToSamples(object):
    def test_basic(self, fake_rollout_and_samples):
        in_, expected_out = fake_rollout_and_samples
        actual_out = _rollout_to_samples(in_)
        actual_out_flat = tf_util.flatten(actual_out)
        expected_out_flat = tf_util.flatten(expected_out)
        assert len(expected_out_flat) == len(actual_out_flat)
        assert all([np.shape(a) == np.shape(b) for a, b in zip(expected_out_flat, actual_out_flat)])
        assert all([np.allclose(a, b) for a, b in zip(expected_out_flat, actual_out_flat)])


if __name__ == "__main__":
    FLAGS(["asdf"])
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "-s", "--pdb",  # On exception drop into pdb++.
        "--verbose", "--verbose",  # Print full diffs on assertions.
        __file__,
    ]))
