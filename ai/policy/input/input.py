from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

from ai.observation_types import (
    GameState,
)
from ai.policy.input.protogen import (
    generate_deserializers,
    generate_make_placeholder,
)
import ai.simulator_parameters as sp
import ai.tf_util as tf_util
import spellsource.cards
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.entity_pb2 as entity_pb2
import spellsource.proto.entity_location_pb2 as entity_location_pb2
import spellsource.proto.entity_state_pb2 as entity_state_pb2
import spellsource.proto.game_state_pb2 as game_state_pb2


## Observation ##

ENTITY_IGNORED_FIELDS = {
    "id": True,
    "state": {
        "background": True,
        "portrait": True,
        "gold": True,
        # NOTE: This is the same as EntityLocation.index, so we ignore it.
        "board_position": True,
        "hero_class": True,
        "custom_renderer": True,
        "custom_data": True,
        "note": True,
        "tribe": True,
        "card_set": True,
        "rarity": True,
        "game_started": True,
    },
}
# NOTE: Spellsource uses owner `-1` to indicate unassigned, so we start the enum at `-1`
# to get possible owners `{0, 1, 2}` for unassigned, player 1, player 2 respectively.
_PLAYER_ENUM_MAPPING = {
    "_terminal_dict": True,
    -1: 0,
    0: 1,
    1: 2,
}
_STANDARD_CARD_IDS = { k: v for k, v in spellsource.cards.STANDARD_CARD_IDS.items() }
_STANDARD_CARD_IDS["_terminal_dict"] = True
ENTITY_SECRETLY_ENUM_FIELDS = {
    "card_id": _STANDARD_CARD_IDS,
    "state": {
        "location": {
            "player": _PLAYER_ENUM_MAPPING,
        },
        "owner": _PLAYER_ENUM_MAPPING,
    },
}
ENTITY_SQUASHED_MESSAGE_FIELDS = {
    "state": True,
}


def _generate_game_state_deserializers(np_dtype):
    return generate_deserializers(descriptor=entity_pb2.Entity.DESCRIPTOR,
                                  np_dtype=np_dtype,
                                  ignored_fields=ENTITY_IGNORED_FIELDS,
                                  repeated_fields={},
                                  secretly_enum_fields=ENTITY_SECRETLY_ENUM_FIELDS,
                                  squashed_message_fields=ENTITY_SQUASHED_MESSAGE_FIELDS)


def get_game_state_placeholders_and_packer(
        np_dtype,  # type: np.dtype
):
    pb_to_np, size, unpack_aggregate_placeholder = _generate_game_state_deserializers(np_dtype)
    make_aggregate_placeholder = generate_make_placeholder(placeholder_name="entity",
                                                           np_dtype=np_dtype,
                                                           size=size)

    with tf.variable_scope("addressable_entities"):
        aggregate_placeholder = make_aggregate_placeholder(sp.N_ADDRESSABLE_ENTITIES)

    def unpack_aggregate_placeholder_(placeholder):
        return unpack_aggregate_placeholder(placeholder, sp.N_ADDRESSABLE_ENTITIES)

    return aggregate_placeholder, unpack_aggregate_placeholder_, pb_to_np


## Action ##

_ACTION_ANY_TO_ACTION_IGNORED_FIELDS = {
    "action": True,
}
_ACTION_SUMMON_IGNORED_FIELDS = {
    "index_to_actions": _ACTION_ANY_TO_ACTION_IGNORED_FIELDS,
}
_ACTION_SPELL_IGNORED_FIELDS = {
    "action_oneof": True,
    "action": True,
    "target_key_to_actions": _ACTION_ANY_TO_ACTION_IGNORED_FIELDS,
}
ACTION_IGNORED_FIELDS = {
    "battlecry": _ACTION_SPELL_IGNORED_FIELDS,
    "discover": {
        "action": True,
    },
    "end_turn": True,
    "hero_power": _ACTION_SPELL_IGNORED_FIELDS,
    "hero": _ACTION_SPELL_IGNORED_FIELDS,
    "physical_attack": {
        "defenders": _ACTION_ANY_TO_ACTION_IGNORED_FIELDS,
    },
    "spell": _ACTION_SPELL_IGNORED_FIELDS,
    "summon": _ACTION_SUMMON_IGNORED_FIELDS,
    "weapon": _ACTION_SUMMON_IGNORED_FIELDS,
}

_ACTION_SPELL_REPEATED_FIELDS = {
    "target_key_to_actions": {
        "_value": (None, 1),
    },
}
_ACTION_SUMMON_REPEATED_FIELDS = {
    "index_to_actions": {
        "_value": (None, 1),
    },
}
ACTION_REPEATED_FIELDS = {
    "battlecry": _ACTION_SPELL_REPEATED_FIELDS,
    "hero": _ACTION_SPELL_REPEATED_FIELDS,
    "hero_power": _ACTION_SPELL_REPEATED_FIELDS,
    "physical_attack": {
        "defenders": {
            "_value": (None, 1),
        },
    },
    "spell": _ACTION_SPELL_REPEATED_FIELDS,
    "summon": _ACTION_SUMMON_REPEATED_FIELDS,
    "weapon": _ACTION_SUMMON_REPEATED_FIELDS,
    "mulligan": {
        "discard_entity_ids": (None, sp.MAX_STARTING_HAND_SIZE),
        "keep_entity_ids": (None, sp.MAX_STARTING_HAND_SIZE),
    },
}


def _generate_action_deserializers(np_dtype):
    return generate_deserializers(descriptor=action_pb2.Action.DESCRIPTOR,
                                  np_dtype=np_dtype,
                                  ignored_fields=ACTION_IGNORED_FIELDS,
                                  repeated_fields=ACTION_REPEATED_FIELDS)


def get_action_placeholders_and_packer(
        np_dtype,  # type: np.dtype
):
    pb_to_np, size, unpack_aggregate_placeholder = _generate_action_deserializers(np_dtype)
    make_aggregate_placeholder = generate_make_placeholder(placeholder_name="action",
                                                           np_dtype=np_dtype,
                                                           size=size)

    with tf.variable_scope("action_placeholders"):
        aggregate_placeholders = make_aggregate_placeholder(sp.GET_MAX_N_ACTIONS())

    def unpack_aggregate_placeholder_(placeholder):
        return unpack_aggregate_placeholder(placeholder, sp.GET_MAX_N_ACTIONS())

    return aggregate_placeholders, unpack_aggregate_placeholder_, pb_to_np
