from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np

from ai.policy.input.input import (
    _generate_action_deserializers,
    _generate_game_state_deserializers,
)
import ai.simulator_parameters as sp


class EagerActionsPacker(object):
    def __init__(self):
        self._action_packer, _, self._action_parser = _generate_action_deserializers(np.float32)

    def __call__(self, action_pbs):
        return [self._action_parser(self._action_packer(apb), 1)[0] for apb in action_pbs]


class EagerGameStatePacker(object):
    def __init__(self):
        self._entity_packer, _, self._entity_parser = _generate_game_state_deserializers(np.float32)

    def __call__(self, action_pbs):
        return self._entity_parser(self._deserialized_entities, sp.N_ADDRESSABLE_ENTITIES)
