from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import numpy as np
import pytest
import tensorflow as tf
tf.enable_eager_execution()

from ai.policy.input.common import (
    ProtogenException,
)
from ai.policy.input.input import (
    ACTION_IGNORED_FIELDS,
    ACTION_REPEATED_FIELDS,
    ENTITY_IGNORED_FIELDS,
    ENTITY_SECRETLY_ENUM_FIELDS,
    ENTITY_SQUASHED_MESSAGE_FIELDS,
)
from ai.policy.input.protogen import (
    generate_deserializers,
)
from spellsource.proto.action_pb2 import (
    Action,
)
from spellsource.proto.spell_action_pb2 import (
    SpellAction,
)
from spellsource.proto.summon_action_pb2 import (
    SummonAction,
    SummonActionIndexToActions,
)
from spellsource.proto.game_state_pb2 import (
    GameState,
)
from spellsource.proto.entity_pb2 import (
    Entity,
)
import ai.test_util as test_util
import spellsource.cards


class TestGenerateDeserializers(object):
    def test_supports_card_ids(self):
        card_ids = spellsource.cards._load_card_ids(["basic", "classic"])
        assert len(card_ids) == 472

        card_ids_enum_field = { k: v for k, v in card_ids.items() }
        card_ids_enum_field["_terminal_dict"] = True
        secretly_enum_fields = {
            "card_id": card_ids_enum_field,
        }
        # Ignore every field except `card_id` for this test.
        ignored_fields = {
            "id": True,
            "entity_type": True,
            "state": True,
        }
        packer, _, parser = generate_deserializers(
            descriptor=Entity.DESCRIPTOR,
            np_dtype=np.float32,

            repeated_fields={},
            ignored_fields=ignored_fields,
            secretly_enum_fields=secretly_enum_fields,
            squashed_message_fields=ENTITY_SQUASHED_MESSAGE_FIELDS,
        )

        in_ = [
            Entity(card_id="hero_garrosh"),  # idx 2
            Entity(card_id="minion_king_krush"),  # idx 126
            Entity(card_id="weapon_wicked_knife"),  # idx 471 / last
        ]
        packed_in = np.concatenate([packer(i) for i in in_], axis=1)
        out = parser(packed_in, len(in_))

        expected_out = [{
            "card_id": np.zeros([1, len(card_ids)], np.float32),
        }, {
            "card_id": np.zeros([1, len(card_ids)], np.float32),
        }, {
            "card_id": np.zeros([1, len(card_ids)], np.float32),
        }]
        expected_out[0]["card_id"][0, 2] = 1
        expected_out[1]["card_id"][0, 126] = 1
        expected_out[2]["card_id"][0, 471] = 1

        test_util.assert_dicts_of_iterables_equal(
            out,
            expected_out,
            lambda a, b: np.isclose(a, b).all())

    def test_supports_oneofs(self):
        packer, _, parser = generate_deserializers(
            descriptor=Action.DESCRIPTOR,
            np_dtype=np.float32,

            ignored_fields=ACTION_IGNORED_FIELDS,
            repeated_fields=ACTION_REPEATED_FIELDS,
            secretly_enum_fields={},
        )

        # Some helpers for expected outputs.
        expected_discover_empty = {
            "card_id": np.zeros((1, 1), np.float32),
        }
        expected_mulligan_empty = {
            "discard_entity_ids": {
                "elems": [
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                ],
                "size": np.zeros((1, 1), np.float32),
            },
            "keep_entity_ids": {
                "elems": [
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                    np.zeros((1, 1), np.float32),
                ],
                "size": np.zeros((1, 1), np.float32),
            },
        }
        expected_physical_attack_empty = {
            "source_id": np.zeros((1, 1), np.float32),
            "defenders": {
                "elems": [{
                    "target": np.zeros((1, 1), np.float32),
                }],
                "size": np.zeros((1, 1), np.float32),
            },
        }
        expected_spell_empty = {
            "source_id": np.zeros((1, 1), np.float32),
            "target_key_to_actions": {
                "elems": [{
                    "target": np.zeros((1, 1), np.float32),
                }],
                "size": np.zeros((1, 1), np.float32),
            },
        }
        expected_summon_empty = {
            "source_id": np.zeros((1, 1), np.float32),
            "index_to_actions": {
                "elems": [{
                    "index": np.zeros((1, 1), np.float32),
                }],
                "size": np.zeros((1, 1), np.float32),
            },
        }

        # Empty sets the "empty" oneof enum.
        expected_out1 = {
            "action_oneof": np.array([[0]], np.float32),
            "battlecry": expected_spell_empty,
            "discover": expected_discover_empty,
            "hero": expected_spell_empty,
            "hero_power": expected_spell_empty,
            "physical_attack": expected_physical_attack_empty,
            "spell": expected_spell_empty,
            "summon": expected_summon_empty,
            "weapon": expected_summon_empty,
            "mulligan": expected_mulligan_empty,
            "playout": {},
        }
        out1 = parser(packer(Action()), 1)[0]
        test_util.assert_dicts_of_iterables_equal(
            out1,
            expected_out1,
            lambda a, b: np.isclose(a, b).all())

        # Non-empty battlecry without target.
        expected_out2 = {
            "action_oneof": np.array([[1]], np.float32),
            "battlecry": {
                "source_id": np.array([[2]], np.float32),
                "target_key_to_actions": {
                    "elems": [{
                        "target": np.zeros((1, 1), np.float32),
                    }],
                    "size": np.zeros((1, 1), np.float32),
                },
            },
            "discover": expected_discover_empty,
            "hero": expected_spell_empty,
            "hero_power": expected_spell_empty,
            "physical_attack": expected_physical_attack_empty,
            "spell": expected_spell_empty,
            "summon": expected_summon_empty,
            "weapon": expected_summon_empty,
            "mulligan": expected_mulligan_empty,
            "playout": {},
        }
        in2 = packer(Action(
            battlecry=SpellAction(
                source_id=2,
                action=23,  # This should be ignored.
            ),
        ))
        out2 = parser(in2, 1)[0]
        test_util.assert_dicts_of_iterables_equal(
            out2,
            expected_out2,
            lambda a, b: np.isclose(a, b).all())

        # Non-empty summon with target.
        expected_out3 = {
            "action_oneof": np.array([[8]], np.float32),
            "battlecry": expected_spell_empty,
            "discover": expected_discover_empty,
            "hero": expected_spell_empty,
            "hero_power": expected_spell_empty,
            "physical_attack": expected_physical_attack_empty,
            "spell": expected_spell_empty,
            "summon": {
                "source_id": np.array([[3]], np.float32),
                "index_to_actions": {
                    "elems": [{
                        "index": np.array([[4]], np.float32),
                    }],
                    "size": np.array([[1]], np.float32),
                },
            },
            "weapon": expected_summon_empty,
            "mulligan": expected_mulligan_empty,
            "playout": {},
        }
        in3 = packer(Action(
            summon=SummonAction(
                source_id=3,
                index_to_actions=[SummonActionIndexToActions(index=4, action=5)],
            ),
        ))
        out3 = parser(in3, 1)[0]
        test_util.assert_dicts_of_iterables_equal(
            out3,
            expected_out3,
            lambda a, b: np.isclose(a, b).all())

    def test_throws_on_unknown_card_id(self):
        basic_card_ids = spellsource.cards._load_card_ids(["basic"])
        basic_card_ids_enum_field = { k: v for k, v in basic_card_ids.items() }
        basic_card_ids_enum_field["_terminal_dict"] = True
        secretly_enum_fields = {
            "card_id": basic_card_ids_enum_field,
        }
        packer, _, _ = generate_deserializers(
            descriptor=Entity.DESCRIPTOR,
            np_dtype=np.float32,

            repeated_fields={},
            ignored_fields=ENTITY_IGNORED_FIELDS,
            secretly_enum_fields=secretly_enum_fields,
            squashed_message_fields=ENTITY_SQUASHED_MESSAGE_FIELDS,
        )

        in_ = Entity(card_id="bad_id")
        with pytest.raises(ProtogenException) as e:
            packed_in = packer(in_)
        assert "has key ('bad_id') outside of specified enum mapping" in e.value.message

    def test_validates_input_dicts(self):
        # Validates `ignored_fields`.
        with pytest.raises(ProtogenException) as e:
            generate_deserializers(GameState.DESCRIPTOR, np.float32,
                                   ignored_fields={"bad_key": True})
        assert "'bad_key' not a member of valid field names" in e.value.message

        # Validates `repeated_fields`.
        with pytest.raises(ProtogenException) as e:
            generate_deserializers(GameState.DESCRIPTOR, np.float32,
                                   repeated_fields={"entities": {"bad_nested_key": ("id", 3)}})
        assert "'bad_nested_key' not a member of valid field names" in e.value.message

        # Validates `secretly_enum_fields`.
        with pytest.raises(ProtogenException) as e:
            generate_deserializers(GameState.DESCRIPTOR, np.float32,
                                   secretly_enum_fields={"entities": {"id": {"non_dict_thing": 4}}})
        assert "'id' must be a submessage of proto" in e.value.message

    def test_works(self):
        ignored_fields = ENTITY_IGNORED_FIELDS
        ignored_fields["state"] = True
        packer, _, parser = generate_deserializers(
            descriptor=Entity.DESCRIPTOR,
            np_dtype=np.float32,

            repeated_fields={},
            ignored_fields=ignored_fields,
            secretly_enum_fields=ENTITY_SECRETLY_ENUM_FIELDS,
            squashed_message_fields=ENTITY_SQUASHED_MESSAGE_FIELDS,
        )

        in_ = [
            Entity(
                card_id="minion_the_voraxx",
                entity_type=Entity.ABSENT),
            Entity(card_id="weapon_shadowblade",
                   entity_type=Entity.PLAYER),
            Entity(entity_type=Entity.HERO),
        ]

        packed_in = np.concatenate([packer(i) for i in in_], axis=1)
        out = parser(packed_in, len(in_))

        expected_out = [{
            "card_id": np.zeros([1, len(spellsource.cards.STANDARD_CARD_IDS)], np.float32),
            "entity_type": np.array([[1, 0, 0, 0, 0, 0, 0, 0, 0]], np.float32),
        }, {
            "card_id": np.zeros([1, len(spellsource.cards.STANDARD_CARD_IDS)], np.float32),
            "entity_type": np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0]], np.float32),
        }, {
            "card_id": np.zeros([1, len(spellsource.cards.STANDARD_CARD_IDS)], np.float32),
            "entity_type": np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0]], np.float32),
        }]
        # NOTE: Calculate these indices using:
        # `cat generated/spellsource/cards/{<sets>} | sort | uniq | less |nl`
        expected_out[0]["card_id"][0, 829] = 1
        expected_out[1]["card_id"][0, 1630] = 1
        expected_out[2]["card_id"][0, 0] = 1

        test_util.assert_dicts_of_iterables_equal(
            out,
            expected_out,
            lambda a, b: np.isclose(a, b).all())


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "--pdb",  # On exception drop into pdb++.
        "-s",
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
