from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class ProtogenException(Exception): pass


def get_elem_size(elem_sizes, elem_name):
    if elem_name not in elem_sizes:
        raise common.ProtogenException(
            "Message size for field '{}' not provided.".format(elem_name))
    return get_dict_value(elem_sizes, elem_name)


def get_fields_for_iteration(descriptor_fields, ignored_fields, secretly_enum_fields):
    ignored_fields_set = _get_ignored_fields_set(ignored_fields)
    fields = []
    for f in descriptor_fields:
        if f.name not in ignored_fields_set:
            # Ignore strings unless called out in enums.
            if f.type != f.TYPE_STRING:
                fields.append(f)
            if f.type == f.TYPE_STRING and f.name in secretly_enum_fields:
                fields.append(f)
    return fields


def get_oneofs_for_iteration(descriptor_oneofs, ignored_fields):
    ignored_fields_set = _get_ignored_fields_set(ignored_fields)
    oneofs = []
    for o in descriptor_oneofs:
        if o.name not in ignored_fields_set:
            oneofs.append(o)
    return oneofs


def get_repeated_fields_set(repeated_fields):
    repeated_fields_set = set()
    for k, v in repeated_fields.items():
        if type(v) != dict or "_value" in v:
            repeated_fields_set.add(k)
    return repeated_fields_set


def get_secretly_enum_fields_set(secretly_enum_fields):
    return set([k for k, v in secretly_enum_fields.items() if "_terminal_dict" in v])


def get_dict_value(repeated_fields, key):
    if type(repeated_fields[key]) != dict:
        return repeated_fields[key]
    else:
        return repeated_fields[key]["_value"]


def validate_proto_dict(descriptor, dict_or_elem, allow_value_key):
    if type(dict_or_elem) != dict:
        return
    if "_terminal_dict" in dict_or_elem:
        return

    fields = {f.name: f for f in descriptor.fields}
    oneofs = set([o.name for o in descriptor.oneofs])
    for k in dict_or_elem:
        if k in fields:
            next_dict_or_elem = dict_or_elem[k]
            next_descriptor = fields[k].message_type
            if (type(next_dict_or_elem) == dict
                    and "_terminal_dict" not in next_dict_or_elem
                    and next_descriptor == None):
                raise ProtogenException(
                    "Dictionary key '{}' must be a submessage of proto '{}'".format(k, descriptor))
            validate_proto_dict(next_descriptor, next_dict_or_elem, allow_value_key)

        elif k in oneofs:
            next_dict_or_elem = dict_or_elem[k]
            if type(next_dict_or_elem) == dict:
                raise ProtogenException(
                    "Oneofs (such as '{}') cannot have a nested spec.".format(k))
            validate_proto_dict(None, next_dict_or_elem, allow_value_key)

        elif allow_value_key and k == "_value":
            continue

        elif k == "_terminal_dict":
            continue

        else:
            raise ProtogenException(
                "Field '{}' not a member of valid field names '{}'".format(k, fields.keys()))


def _get_ignored_fields_set(ignored_fields):
    return set([k for k, v in ignored_fields.items() if type(v) != dict])
