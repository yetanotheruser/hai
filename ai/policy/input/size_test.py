from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import numpy as np
import pytest
import tensorflow as tf

from ai.policy.input.input import (
    ENTITY_IGNORED_FIELDS,
    ENTITY_SECRETLY_ENUM_FIELDS,
)
from ai.policy.input.size import (
    get_pb_placeholder_size,
)
import spellsource.proto.entity_pb2 as entity_pb2
import spellsource.proto.game_state_pb2 as game_state_pb2

class TestGetPbPlaceholderSize(object):
    def test_works(self):
        out = get_pb_placeholder_size(
            game_state_pb2.GameState.DESCRIPTOR,
            ignored_fields={
                "entities": {
                    "card_id": True,
                    "state": True,
                },
                "power_history": True,
                "timestamp": True,
            },
            repeated_fields={
                "entities": {
                    "_value": ("id", 3),
                }
            },
            secretly_enum_fields={
                "turn_number": {
                    "_terminal_dict": True,
                    0: 0,
                    1: 1,
                },
            },
        )

        expected_out = {
            "_value": 35,
            "entities": {
                "_value": 10,
                "id": 1,
                "entity_type": 9,
            },
            "is_local_player_turn": 2,
            "turn_number": 2,
        }
        assert expected_out == out

    def DISABLED_test_gets_game_state_size(self):
        # Useful to sanity check entity state size calculations.
        out = get_pb_placeholder_size(
            entity_pb2.Entity.DESCRIPTOR,
            ignored_fields=ENTITY_IGNORED_FIELDS,
            repeated_fields={},
            secretly_enum_fields=ENTITY_SECRETLY_ENUM_FIELDS,
        )

        expected_out = {
            "_value": 111,
            "id": 1,
            "entity_type": 9,
            "state": {
                "_value": 101,
                "location": {
                    "_value": 20,
                    "player": 3,
                    "index": 1,
                    "zone": 16,
                },
                "owner": 3,
                "base_hp": 1,
                "hp": 1,
                "durability": 1,
                "max_hp": 1,
                "base_attack": 1,
                "attack": 1,
                "base_mana_cost": 1,
                "mana_cost": 1,
                "armor": 1,
                "destroyed": 2,
                "summoning_sickness": 2,
                "frozen": 2,
                "uncensored": 2,
                "deflect": 2,
                "silenced": 2,
                "windfury": 2,
                "permanent": 2,
                "collectible": 2,
                "taunt": 2,
                "spell_damage": 1,
                "charge": 2,
                "rush": 2,
                "lifesteal": 2,
                "poisonous": 2,
                "enraged": 2,
                "battlecry": 2,
                "deathrattles": 2,
                "immune": 2,
                "divine_shield": 2,
                "stealth": 2,
                "combo": 2,
                "overload": 1,
                "choose_one": 2,
                "untargetable_by_spells": 2,
                "cannot_attack": 2,
                "under_aura": 2,
                "playable": 2,
                "condition_met": 2,
                "mana": 1,
                "max_mana": 1,
                "locked_mana": 1,
                "hosts_trigger": 2,
                "card_type": 6,
                "fires": 1,
                "count_until_cast": 1,
            },
        }
        assert expected_out == out


if __name__ == "__main__":
    sys.exit(pytest.main([
        "--color=yes",  # Colorize output.
        "--pdb",  # On exception drop into pdb++.
        "-s",
        "--verbose", "--verbose",  # Print full diffs on assert.
        __file__,
    ]))
