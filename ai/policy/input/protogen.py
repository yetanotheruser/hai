from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import logging
import google.protobuf.text_format as text_format
import numpy as np
import tensorflow as tf

from ai.policy.input.packer import get_pb_packer
from ai.policy.input.size import get_pb_placeholder_size
from ai.policy.input.unpacker import get_pb_unpacker
import ai.policy.input.common as common


def generate_deserializers(
        descriptor,  # type: google.protobuf.pyext._message.MessageDescriptor
        np_dtype,  # type: np.dtype
        ignored_fields=None,  # type: Optional[Dict[str, Dict[<this type>] | bool]]
        repeated_fields=None,  # type: Optional[Dict[str, Dict[<this type>] | Tuple[Optional[str], int]]]
        secretly_enum_fields=None,  # type: Optional[Dict[str, Dict[<this type>] | int]]
        squashed_message_fields=None,  # type: Optional[Dict[str, Dict[<this type>] | bool]]
):
    # type: (...) -> Tuple[
    #   Callable[<proto>, np.ndarray],
    #   int,
    #   Callable[tf.placeholders, Dict[tf.placeholders]],
    # ]
    """
    Create functions to:
    1. Turn the aggregate placeholder into a more convenient dict of placeholders.
    2. Pack a proto ino an `np.ndarray` conforming to the returned placeholders.

    Also calculates the size of each deserialized proto of type `descriptor`.

    Required Args:
    - descriptor: Descriptor of the protobuf to consume.
    - np_dtype: The `np.dtype` of the desired placeholder.

    Optional Args:
    - ignored_fields: A (nested) dictionary of message fields to ignore.
    - repeated_fields: A (nested) dictionary of repeated fields.
                       Each terminal entry should be of the form `(idx_field, n_elems)` where:
                       - `idx_field` (if not `None`) indicates which field in the repeated message
                         describes the index into the list that element should have.
                       - `n_elems`: Is the number of elements to pad/truncate to (if less than
                         `n_elems` are in the repeated message).
    - secretly_enum_fields: A (nested) dictionary of int fields that should be translated as enums.
    - squashed_message_fields: A (nested) dictionary of fields that should be collapsed.

    Returns:
    - Some helper functions. See earlier docs for a description.
    """
    if ignored_fields == None:
        ignored_fields = {}
    else:
        common.validate_proto_dict(descriptor, ignored_fields, False)

    if repeated_fields == None:
        repeated_fields = {}
    else:
        common.validate_proto_dict(descriptor, repeated_fields, True)

    if secretly_enum_fields == None:
        secretly_enum_fields = {}
    else:
        common.validate_proto_dict(descriptor, secretly_enum_fields, False)

    if squashed_message_fields == None:
        squashed_message_fields = {}
    else:
        common.validate_proto_dict(descriptor, squashed_message_fields, False)

    # Compute sizes.
    elem_sizes = get_pb_placeholder_size(
        descriptor=descriptor,
        ignored_fields=ignored_fields,
        repeated_fields=repeated_fields,
        secretly_enum_fields=secretly_enum_fields,
    )
    total_size = elem_sizes["_value"]

    # Packer.
    _packer = get_pb_packer(
            descriptor=descriptor,
            ignored_fields=ignored_fields,
            elem_sizes=elem_sizes,
            repeated_fields=repeated_fields,
            secretly_enum_fields=secretly_enum_fields,
    )
    def pack(pb):
        np_array = np.zeros((1, 1, total_size), dtype=np_dtype)
        _packer(np_array[0, :, :], 0, pb)
        return np_array

    # Unpacker.
    _unpack = get_pb_unpacker(
            descriptor=descriptor,
            elem_sizes=elem_sizes,
            ignored_fields=ignored_fields,
            repeated_fields=repeated_fields,
            secretly_enum_fields=secretly_enum_fields,
            squashed_message_fields=squashed_message_fields,
    )
    def unpack(placeholder, n):
        unpacked_elems = []
        for i in range(n):
            unpacked_elems.append(_unpack(placeholder[:, i, :], 0, [])[0])
        return unpacked_elems

    return (
        pack,
        total_size,
        unpack,
    )


def generate_make_placeholder(
        placeholder_name,  # type: string
        np_dtype,  # type: np.dtype
        size,  # type: int
):
    # type: (...) -> Callable[tf.placeholders],
    """
    Create functions to generate an aggregate placeholder (compatible with the output of
    `generate_deserializers`).

    Required Args:
    - placeholder_name: The placeholder name.
    - np_dtype: The `np.dtype` of the desired placeholder.

    Returns:
    - Some helper functions. See earlier docs for a description.
    """
    # Placeholder creator.
    tf_dtype = tf.as_dtype(np_dtype)
    def make_placeholder(n):
        return tf.placeholder(tf_dtype, [None, n, size], placeholder_name)

    return make_placeholder
