from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import logging

import ai.policy.input.common as common


def _pack_bool(np_array, curr_idx, value):
    if not value:
        np_array[0, curr_idx] = 1
    else:
        np_array[0, curr_idx + 1] = 1


def _pack_enum(np_array, curr_idx, value):
    np_array[0, curr_idx + value] = 1


def _pack_scalar(np_array, curr_idx, value):
    np_array[0, curr_idx] = value


def _repeat(idx_field, elem_size, n_elems):
    def _inner_repeat(fn, np_array, curr_idx, pbs):

        # Repeated begins with size.
        size = len(pbs)
        _pack_scalar(np_array, curr_idx, size)

        # Each payload follows.
        truncated = False
        for i in range(len(pbs)):
            if idx_field:
                dest_elem = getattr(pbs[i], idx_field)
            else:
                dest_elem = i
            if dest_elem < n_elems:
                fn(np_array, curr_idx + dest_elem * elem_size + 1, pbs[i])
            else:
                truncated = True

        if truncated:
            logging.log_every_n_seconds(
                logging.WARN,
                "Attempted to load more than {} elements; Truncated to {}.".format(
                    n_elems, n_elems),
                5)

        return curr_idx + n_elems * elem_size + 1

    return _inner_repeat


def get_pb_packer(
        descriptor,  # type: google.protobuf.pyext._message.MessageDescriptor
        ignored_fields,  # type: Dict[str, Dict[<this type>] | bool]
        elem_sizes,  # type: Dict[str, Dict[<this type>] | int]
        repeated_fields,  # type: Optional[Dict[str, Dict[<this type>] | Tuple[Optional[str], int]]]
        secretly_enum_fields,  # type: Dict[str, Dict[<this type>] | int]
):
    repeated_fields_set = common.get_repeated_fields_set(repeated_fields)
    secretly_enum_fields_set = common.get_secretly_enum_fields_set(secretly_enum_fields)
    fields = common.get_fields_for_iteration(descriptor.fields, ignored_fields,
                                             secretly_enum_fields)

    pack_message_fns = {}
    for f in fields:
        if f.type == f.TYPE_MESSAGE:
            if "_terminal_dict" in secretly_enum_fields:
                secretly_enum_subfields = {}
            else:
                secretly_enum_subfields = secretly_enum_fields.get(f.name, {})
            pack_message_fns[f.name] = get_pb_packer(
                f.message_type,
                ignored_fields=ignored_fields.get(f.name, {}),
                elem_sizes=elem_sizes.get(f.name, {}),
                repeated_fields=repeated_fields.get(f.name, {}),
                secretly_enum_fields=secretly_enum_subfields,
            )

    def packer(
            np_array,  # type: np.ndarray
            curr_idx,  # type: int
            pb,
    ):
        # type: (...) -> int
        for f in fields:
            elem_size = common.get_elem_size(elem_sizes, f.name)

            if f.label == f.LABEL_REPEATED:
                if f.name not in repeated_fields_set:
                    raise common.ProtogenException(
                        "Repeated field {} must be described in repeated_fields_set!".format(
                            f.name))
                idx_field, n_elems = common.get_dict_value(repeated_fields, f.name)
                apply_fn = _repeat(idx_field, elem_size, n_elems)
            else:
                def apply_fn(fn, np_array, curr_idx, value):
                    fn(np_array, curr_idx, value)
                    return curr_idx + elem_size

            f_value = getattr(pb, f.name)
            if f.name in secretly_enum_fields_set:
                vals_dict = secretly_enum_fields[f.name]
                if f_value not in vals_dict:
                    raise common.ProtogenException(
                        ("Secretly enum field '{}' has key ('{}') outside of specified enum"
                        " mapping.").format(f.name, f_value)
                    )
                f_enum_value = vals_dict[f_value]

                curr_idx = apply_fn(_pack_enum, np_array, curr_idx, f_enum_value)

            elif f.type == f.TYPE_BOOL:
                curr_idx = apply_fn(_pack_bool, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_DOUBLE:
                curr_idx = apply_fn(_pack_scalar, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_ENUM:
                curr_idx = apply_fn(_pack_enum, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_FLOAT:
                curr_idx = apply_fn(_pack_scalar, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_INT32:
                curr_idx = apply_fn(_pack_scalar, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_INT64:
                curr_idx = apply_fn(_pack_scalar, np_array, curr_idx, f_value)

            elif f.type == f.TYPE_MESSAGE:
                pack_message = pack_message_fns[f.name]
                curr_idx = apply_fn(pack_message, np_array, curr_idx, f_value)

            else:
                raise common.ProtogenException(
                    "Unsupported message type '{}' in field '{}' of message '{}'".format(
                        f.type, f.name, descriptor.name
                    )
                )

        oneofs = common.get_oneofs_for_iteration(descriptor.oneofs, ignored_fields)
        for o in oneofs:
            elem_size = common.get_elem_size(elem_sizes, o.name)
            oneof_field_names = [f.name for f in o.fields]
            which_oneof = pb.WhichOneof(o.name)
            if which_oneof:
                oneof_value = oneof_field_names.index(which_oneof) + 1
            else:
                # Default (empty) oneof is 0th.
                oneof_value = 0
            _pack_scalar(np_array, curr_idx, oneof_value)
            curr_idx += elem_size

    return packer
