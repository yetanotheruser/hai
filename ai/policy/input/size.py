from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import ai.policy.input.common as common


def get_pb_placeholder_size(
        descriptor,  # type: google.protobuf.pyext._message.MessageDescriptor
        ignored_fields,  # type: Dict[str, <this type> | bool]
        repeated_fields,  # type: Optional[Dict[str, <this type> | Tuple[Optional[str], int]]]
        secretly_enum_fields,  # type: Dict[str, <this type> | int]
):
    # type: (...) -> Dict[str, <this type> | int]
    repeated_fields_set = common.get_repeated_fields_set(repeated_fields)
    secretly_enum_fields_set = common.get_secretly_enum_fields_set(secretly_enum_fields)
    fields = common.get_fields_for_iteration(descriptor.fields, ignored_fields,
                                             secretly_enum_fields)

    size = 0
    elem_sizes = {}
    for f in fields:
        repeated_field_multiplier = 1
        repeated_field_size = 0
        if f.name in repeated_fields_set:
            _, repeated_field_multiplier = common.get_dict_value(repeated_fields, f.name)
            repeated_field_size = 1

        if f.name in secretly_enum_fields_set:
            if not (f.type == f.TYPE_INT32 or f.type == f.TYPE_INT64 or f.type == f.TYPE_STRING):
                raise common.ProtogenException(
                    ("Secretly enum fields must be ints, but field '{}' of message '{}' has"
                    "type '{}'.").format(f.name, descriptor.name, f.type))
            assert "_terminal_dict" in secretly_enum_fields[f.name]
            # NOTE: Subtract 1 for mandatory `"_terminal_dict"` entry.
            elem_size = len(secretly_enum_fields[f.name]) - 1

        elif f.type == f.TYPE_BOOL:
            elem_size = 2

        elif f.type == f.TYPE_DOUBLE:
            elem_size = 1

        elif f.type == f.TYPE_ENUM:
            elem_size = len(f.enum_type.values)

        elif f.type == f.TYPE_FLOAT:
            elem_size = 1

        elif f.type == f.TYPE_INT32:
            elem_size = 1

        elif f.type == f.TYPE_INT64:
            elem_size = 1

        elif f.type == f.TYPE_MESSAGE:
            if "_terminal_dict" in secretly_enum_fields:
                secretly_enum_subfields = {}
            else:
                secretly_enum_subfields = secretly_enum_fields.get(f.name, {})
            elem_sizes[f.name] = get_pb_placeholder_size(
                f.message_type,
                ignored_fields=ignored_fields.get(f.name, {}),
                repeated_fields=repeated_fields.get(f.name, {}),
                secretly_enum_fields=secretly_enum_subfields,
            )
            elem_size = elem_sizes[f.name]["_value"]

        else:
            raise Exception("Unsupported message type '{}' in field '{}' of message '{}'".format(
                f.type, f.name, descriptor.name
            ))

        # If `f.name` is already in `elem_sizes` it means `f.name` was a submessage and the
        # recursive call already populated `elem_sizes[f.name]["_value"]`.
        if f.name not in elem_sizes:
            elem_sizes[f.name] = elem_size
        size += repeated_field_multiplier * elem_size + repeated_field_size

    oneofs = common.get_oneofs_for_iteration(descriptor.oneofs, ignored_fields)
    for o in oneofs:
        elem_sizes[o.name] = 1
        # NOTE: Thanksfully `repeated` `oneof`s are not allowed. See
        # https://github.com/protocolbuffers/protobuf/issues/2592.
        size += elem_sizes[o.name]

    elem_sizes["_value"] = size
    return elem_sizes
