from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

import ai.policy.input.common as common


def _unpack_elem(tensor, curr_idx, name_prefixes, name, size):
    elem_tensor = tf.slice(
        tensor,
        [0, curr_idx],
        [-1, size],
        name=".".join(name_prefixes + [name]))
    return elem_tensor, curr_idx + size


def _repeat(n_elems):
    # type: (...) -> Tuple[List[], int]
    def _inner_repeat(fn, tensor, curr_idx, name_prefixes, *args, **kwargs):
        ret = {"elems": []}
        ret["size"], curr_idx = _unpack_elem(tensor=tensor,
                                             curr_idx=curr_idx,
                                             name_prefixes=name_prefixes,
                                             name="size",
                                             size=1)

        for i in range(n_elems):
            elem, curr_idx = fn(tensor, curr_idx, name_prefixes, *args, **kwargs)
            ret["elems"].append(elem)
        return ret, curr_idx

    return _inner_repeat


def get_pb_unpacker(
        descriptor,  # type: google.protobuf.pyext._message.MessageDescriptor
        elem_sizes,  # type: Dict[str, Dict[<this type>] | int]
        ignored_fields,  # type: Dict[str, Dict[<this type>] | bool]
        repeated_fields,  # type: Optional[Dict[str, Dict[<this type>] | Tuple[Optional[str], int]]]
        secretly_enum_fields,  # type: Dict[str, Dict[<this type>] | int]
        squashed_message_fields,  # type: Optional[Dict[str, Dict[<this type>] | bool]]
):
    fields = common.get_fields_for_iteration(descriptor.fields, ignored_fields,
                                             secretly_enum_fields)
    repeated_fields_set = common.get_repeated_fields_set(repeated_fields)

    def unpacker(
            tensor,  # type: tf.Tensor[None, <packed size>]
            curr_idx,  # type: int
            name_prefixes,  # type: List[str]
    ):
        # type: (...) -> Tuple[Dict[], int]
        unpacked = {}
        for f in fields:
            if f.name in repeated_fields_set:
                _, n_elems = common.get_dict_value(repeated_fields, f.name)
                apply_fn = _repeat(n_elems)

            else:
                def apply_fn(fn, *args, **kwargs):
                    return fn(*args, **kwargs)

            if f.type == f.TYPE_MESSAGE and squashed_message_fields.get(f.name, {}) != True:
                if "_terminal_dict" in secretly_enum_fields:
                    secretly_enum_subfields = {}
                else:
                    secretly_enum_subfields = secretly_enum_fields.get(f.name, {})
                unpack_message = get_pb_unpacker(
                    descriptor=f.message_type,
                    elem_sizes=elem_sizes[f.name],
                    ignored_fields=ignored_fields.get(f.name, {}),
                    repeated_fields=repeated_fields.get(f.name, {}),
                    secretly_enum_fields=secretly_enum_subfields,
                    squashed_message_fields=squashed_message_fields.get(f.name, {}),
                )

                name_prefixes.append(f.name)
                unpacked[f.name], curr_idx = apply_fn(
                    fn=unpack_message,
                    tensor=tensor, curr_idx=curr_idx, name_prefixes=name_prefixes,
                )
                name_prefixes.pop()

            else:
                elem_size = common.get_elem_size(elem_sizes, f.name)
                unpacked[f.name], curr_idx = apply_fn(
                    fn=_unpack_elem,
                    tensor=tensor, curr_idx=curr_idx, name_prefixes=name_prefixes, name=f.name,size=elem_size,
                )

        oneofs = common.get_oneofs_for_iteration(descriptor.oneofs, ignored_fields)
        for o in oneofs:
            elem_size = common.get_elem_size(elem_sizes, o.name)
            unpacked[o.name], curr_idx = _unpack_elem(tensor=tensor,
                                                      curr_idx=curr_idx,
                                                      name_prefixes=name_prefixes,
                                                      name=o.name,
                                                      size=elem_size)

        return unpacked, curr_idx

    return unpacker
