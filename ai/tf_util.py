from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections

import numpy as np
import tensorflow as tf


ANY = object()
class UnexpectedShapeException(Exception): pass


def assert_ndarray_has_shape(
        array,  # type: np.ndarray
        shape,  # type: Iterable[int | ANY]
):
    """
    Helper for asserting on `np.ndarray` shapes.

    Args:
    - array: `np.ndarray` whose shape to make an assertion on.
    - shape: Dimensions that `tensor`'s static shape must match. Use `ANY` to match any dimension.

    Returns:
    - `True` if static shape matches, `False` otherwise.
    """
    # type: (...) -> None
    assert_actual_shape_conforms_to_desired_shape(array.shape, shape)


def assert_actual_shape_conforms_to_desired_shape(
        actual_shape,  # type: Iterable[int | ANY]
        desired_shape,  # type: Iterable[int | ANY]
):
    # type: (...) -> None
    if len(actual_shape) != len(desired_shape):
        raise UnexpectedShapeException(
            "Shape is '{}' but expected '{}'".format(actual_shape, desired_shape))
    for array_dim, desired_dim in zip(actual_shape, desired_shape):
        if desired_dim == ANY:
            continue
        if array_dim != desired_dim:
            raise UnexpectedShapeException(
                "Shape is '{}' but expected '{}'".format(actual_shape, desired_shape))


def assert_tensor_has_static_shape(
        tensor,  # type: tf.Tensor
        shape,  # type: Iterable[int | ANY]
):
    """
    Helper for asserting on static tensor shapes at graph construction time.

    Args:
    - tensor: Tensor whose shape to make an assertion on.
    - shape: Dimensions that `tensor`'s static shape must match. Use `ANY` to match any dimension.

    Returns:
    - `True` if static shape matches, `False` otherwise.
    """
    # type: (...) -> bool
    tensor_shape = tensor.shape.as_list()
    assert_actual_shape_conforms_to_desired_shape(tensor_shape, shape)


def is_terminal(
        iter_or,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray | any
        ):
    # type: (...) -> bool
    return (
        isinstance(iter_or, tf.Tensor)
        or isinstance(iter_or, np.ndarray)
        or not isinstance(iter_or, collections.Iterable)
    )


def map_over_iterables(
        function,  # type: Callable[tf.Tensor | np.ndarray, tf.Tensor]
        iter_or,  # type: Iterable[<this type>] | tf.Tensor
        ):
    # type: (...) -> Tuple[<this type>] | tf.Tensor
    if is_terminal(iter_or):
        return function(iter_or)
    l = []
    for i in iter_or:
        l.append(map_over_iterables(function, i))
    return tuple(l)


def concatenate_iterables(
        iter_or,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
        axis=1,  # type: int
        concat_fn=tf.concat
        ):
    # type: (...) -> Optional[tf.Tensor | np.ndarray]
    flattened_iter = flatten(iter_or)

    try:
        initializer = flattened_iter[0]
    except IndexError:
        return None

    def reducable_concat_fn(accumulator, item):
        return concat_fn([accumulator, item], axis=axis)
    return reduce(reducable_concat_fn, flattened_iter[1:], initializer)


def flatten(
        iter_or,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
):
    # type: List[tf.Tensor | np.ndarray]
    flattened_iter = []   # type: List[tf.Tensor | np.ndarray]
    def flatten_iter_map_fn(item):
        flattened_iter.append(item)
    map_over_iterables(flatten_iter_map_fn, iter_or)
    return flattened_iter


def iterables_to_tuples(
        iter_or,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
):
    # type: (...) -> Tuple[<this type>] | tf.Tensor | np.ndarray
    if is_terminal(iter_or):
        return iter_or
    l = []
    for i in iter_or:
        l.append(iterables_to_tuples(i))
    return tuple(l)


def batch_inner_product_iterables(
        iter_or_a,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
        iter_or_b,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
        sum_fn=lambda x: tf.reduce_sum(x, 1, keepdims=True),
        ):
    # type: (...) -> Tuple[<this type>] | tf.Tensor | np.ndarray
    """
    Apply `tf.tensordot` to two nested iterables of `tf.Tensor | np.ndarray`s with the same shape.
    """
    def inner_product_sum(a, b):
        c = a * b
        return sum_fn(c)
    return map_over_two_iterables(inner_product_sum, iter_or_a, iter_or_b)


def map_over_two_iterables(
        function,  # type: Callable[[tf.Tensor | np.ndarray, tf.Tensor | np.ndarray], tf.Tensor]
        iter_or_a,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
        iter_or_b,  # type: Iterable[<this type>] | tf.Tensor | np.ndarray
        *args,
        **kwargs
        ):
    # type: (...) -> Tuple[<this type>] | tf.Tensor | np.ndarray
    """
    Apply `function` to two nested iterables of `tf.Tensor | np.ndarray`s with the same shape.
    """
    if is_terminal(iter_or_a):
        assert is_terminal(iter_or_b)
        return function(iter_or_a, iter_or_b, *args, **kwargs)
    l = []
    assert len(iter_or_a) == len(iter_or_b), "Cowardly refusing to zip iters of different length."
    for i1, i2 in zip(iter_or_a, iter_or_b):
        l.append(map_over_two_iterables(function, i1, i2, *args, **kwargs))
    return tuple(l)


def map_over_namedtuple(
        function,  # type: Callable[tf.Tensor | np.ndarray, tf.Tensor]
        namedtuple_,
        ):
    template = namedtuple_to_template(namedtuple_)
    mapped_tuple = map(function, flatten(namedtuple_))
    return namedtuple_from_template(template, mapped_tuple)


def namedtuple_to_template(namedtuple_):
    """
    Create a template from a (nested) `namedtuple`.
    """
    template = collections.deque()
    template.append(namedtuple_.__class__)
    contiguous_terminal_count = 0
    for elem in namedtuple_:
        if is_terminal(elem):
            contiguous_terminal_count += 1
        else:
            if contiguous_terminal_count > 0:
                template.append(contiguous_terminal_count)
            template.append(namedtuple_to_template(elem))
            contiguous_terminal_count = 0
    if contiguous_terminal_count > 0:
        template.append(contiguous_terminal_count)
    return template


def namedtuple_from_template(template, tuple_):
    return _namedtuple_from_template(template, tuple_, 0)[0]


def _namedtuple_from_template(template, tuple_, tuple_idx):
    cls = template.popleft()
    args = []
    for template_item in template:
        if type(template_item) == int:
            args.extend(tuple_[tuple_idx:tuple_idx+template_item])
            tuple_idx += template_item
        else:
            elem, tuple_idx = _namedtuple_from_template(template_item, tuple_, tuple_idx)
            args.append(elem)
    if cls == list or cls == tuple:
        instance = cls(args)
    else:
        instance = cls(*args)
    return instance, tuple_idx
