from __future__ import absolute_import
from __future__ import print_function

from collections import namedtuple

GameState = namedtuple("GameState", [
    #"power_history",
    "entities",
    #"is_local_player_turn",
    #"turn_state",
    "turn_number",
    #"timestamp",
])
