from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections

import numpy as np
import pytest
import tensorflow as tf

import ai.tf_util as tf_util


def fix_random_seeds():
    return (np.random.seed(0), tf.set_random_seed(0))


def assert_dicts_of_iterables_equal(a, b, compare_fn):
    """
    Asserts that two dicts are equal (according to `compare_fn`).
    """
    if tf_util.is_terminal(a) or tf_util.is_terminal(b):
        if not (tf_util.is_terminal(a) and tf_util.is_terminal(b)):
            raise Exception("Both '{}' and '{}' must be terminal to compare them.".format(a, b))
        assert compare_fn(a, b)
        return

    elif type(a) == dict or type(b) == dict:
        if not (type(a) == dict and type(b) == dict):
            raise Exception("Both '{}' and '{}' must dicts to compare them.".format(a, b))

        keys_a = set(a.keys())
        keys_b = set(b.keys())
        assert keys_a == keys_b
        for k in keys_a:
            assert_dicts_of_iterables_equal(a[k], b[k], compare_fn)

    elif isinstance(a, collections.Iterable) or isinstance(a, collections.Iterable):
        if not (isinstance(a, collections.Iterable) and isinstance(a, collections.Iterable)):
            raise Exception("Both '{}' and '{}' must iterable to compare them.".format(a, b))

        for a_item, b_item in zip(a, b):
            assert_dicts_of_iterables_equal(a_item, b_item, compare_fn)

    else:
        raise Exception("Unexpected elements '{}' and '{}' being compared".format(a, b))


def assert_ndarrays_not_close(*args):
    for i in range(len(args)):
        for j in range(len(args)):
            if i != j:
                assert not np.isclose(args[i], args[j]).all()
