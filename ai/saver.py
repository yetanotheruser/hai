from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf


class FastSaver(tf.train.Saver):
    def save(self, *args, **kwargs):
        # Default save writes without metagraph (which is useless for most cases; see
        # https://stackoverflow.com/questions/36195454/what-is-the-tensorflow-checkpoint-meta-file).
        kwargs["write_meta_graph"] = False
        super(FastSaver, self).save(*args, **kwargs)
