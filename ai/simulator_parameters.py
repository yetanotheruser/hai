"""
A bunch of constants about the simulator.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags

from spellsource.proto.entity_location_pb2 import (
    EntityLocation,
)
import spellsource.proto.action_pb2 as action_pb2
import spellsource.proto.discover_action_pb2 as discover_action_pb2
import spellsource.proto.physical_attack_action_pb2 as physical_attack_action_pb2
import spellsource.proto.spell_action_pb2 as spell_action_pb2
import spellsource.proto.summon_action_pb2 as summon_action_pb2

FLAGS = flags.FLAGS
flags.DEFINE_bool("sanity_check",
                  False,
                  "Set to make this run a 'sanity check' (and drastically reduce compute costs).")
flags.DEFINE_integer("max_n_actions",
                     125,
                     "Maximum number of actions considerable at any given time.")

## Addressable entities.
MAX_BOARD_SIZE = 7
MAX_DISCOVERS = 4
MAX_HAND_SIZE = 10
# NOTE: For now I assume this will only be populated during mulligans. This may be false.
MAX_SET_ASIDE_ZONE_SIZE = 4
ADDRESSABLE_ENTITIES = {
    # NOTE: These are in the same order as the `EntityLocation.Zone` enum so it's easy to understand
    # their iteration order.
    EntityLocation.Zone.Value("HAND"): {
        "protagonist": MAX_HAND_SIZE,
    },
    EntityLocation.Zone.Value("BATTLEFIELD"): {
        "protagonist": MAX_BOARD_SIZE,
        "antagonist": MAX_BOARD_SIZE,
    },
    EntityLocation.Zone.Value("HERO_POWER"): {
        "protagonist": 1,
        "antagonist": 1,
    },
    EntityLocation.Zone.Value("HERO"): {
        "protagonist": 1,
        "antagonist": 1,
    },
    EntityLocation.Zone.Value("WEAPON"): {
        "protagonist": 1,
        "antagonist": 1
    },
    EntityLocation.Zone.Value("SET_ASIDE_ZONE"): {
        "protagonist": MAX_SET_ASIDE_ZONE_SIZE,
    },
    EntityLocation.Zone.Value("DISCOVER"): {
        "protagonist": MAX_DISCOVERS,
    },
}
SORTED_ADDRESSIBLE_ENTITIES = sorted(ADDRESSABLE_ENTITIES.items())
N_ADDRESSABLE_ENTITIES = sum(sum(counts.values()) for _, counts in ADDRESSABLE_ENTITIES.items())

## Nonaddressable entities.
# Maximum number of secrets.
MAX_N_SECRETS = 5

# Number of positions to place a minion on the board.
N_BOARD_POSITIONS = MAX_BOARD_SIZE + 1
# Maximum number of cards in starting hand supported by the simulator.
MAX_STARTING_HAND_SIZE = 4

# Maximum number of valid actions that agents need support.
def GET_MAX_N_ACTIONS():
    return 2 if FLAGS.sanity_check else FLAGS.max_n_actions


# 7 + 7 (boards)
# 10 (hand)
# 3 (discover)
# 1 + 1 (hero)
# 1 + 1 (hero power)
# 1 + 1 (weapon)
# Total addressable: 33
#
# 5 + 5 (secret)
# 1 + 1 (quest)
# 60 (deck)
# Total player: 105

# replace opponent deck via card count
# replace opponent hand via card count
# ? - 60 (other)
# Total: 175

# The number of players.
N_PLAYERS = 2
# Distinct types of action.
ACTION_TYPE_BATTLECRY = "battlecry"
ACTION_TYPE_DISCOVER = "discover"
ACTION_TYPE_END_TURN = "end_turn"
ACTION_TYPE_HERO = "hero"
ACTION_TYPE_HERO_POWER = "hero_power"
ACTION_TYPE_MULLIGAN = "mulligan"
ACTION_TYPE_PHYSICAL_ATTACK = "physical_attack"
ACTION_TYPE_SPELL = "spell"
ACTION_TYPE_SUMMON = "summon"
ACTION_TYPE_WEAPON = "weapon"
ACTION_TYPES = {
    ACTION_TYPE_BATTLECRY: spell_action_pb2.SpellAction,
    ACTION_TYPE_DISCOVER: discover_action_pb2.DiscoverAction,
    ACTION_TYPE_END_TURN: None,
    ACTION_TYPE_HERO: spell_action_pb2.SpellAction,
    ACTION_TYPE_HERO_POWER: spell_action_pb2.SpellAction,
    ACTION_TYPE_MULLIGAN: spell_action_pb2.SpellAction,
    ACTION_TYPE_PHYSICAL_ATTACK: spell_action_pb2.SpellAction,
    ACTION_TYPE_SPELL: spell_action_pb2.SpellAction,
    ACTION_TYPE_SUMMON: spell_action_pb2.SpellAction,
    ACTION_TYPE_WEAPON: spell_action_pb2.SpellAction,
}
# NOTE: These keys MUST be in the same order as the `action_pb2.Action.action_oneof`.
ACTION_TYPE_KEYS = [
    ACTION_TYPE_BATTLECRY,
    ACTION_TYPE_DISCOVER,
    ACTION_TYPE_END_TURN,
    ACTION_TYPE_HERO,
    ACTION_TYPE_HERO_POWER,
    ACTION_TYPE_PHYSICAL_ATTACK,
    ACTION_TYPE_SPELL,
    ACTION_TYPE_SUMMON,
    ACTION_TYPE_WEAPON,
    ACTION_TYPE_MULLIGAN,
]
assert set(ACTION_TYPE_KEYS) == set(ACTION_TYPES.keys())
NUM_ACTION_TYPES = len(ACTION_TYPE_KEYS)
