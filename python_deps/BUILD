# This file manually maintains lists of transitive dependencies for each Python package that we care
# about (ie the entries in `//python_deps/requirements.txt`).
load("@py_deps//:requirements.bzl", "requirement")

py_library(
    name = "absl_py",
    deps = [
        requirement("absl_py"),

        requirement("enum34"),
        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "futures",
    deps = [
        requirement("futures"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "grpcio",
    deps = [
        requirement("grpcio"),

        requirement("enum34"),
        requirement("futures"),
        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "ipython",
    deps = [
        requirement("ipython"),

        requirement("backports.shutil_get_terminal_size"),
        requirement("decorator"),
        requirement("enum34"),
        requirement("ipython_genutils"),
        requirement("pathlib2"),
        requirement("pexpect"),
        requirement("pickleshare"),
        requirement("prompt_toolkit"),
        requirement("ptyprocess"),
        requirement("pygments"),
        requirement("scandir"),
        requirement("simplegeneric"),
        requirement("six"),
        requirement("traitlets"),
        requirement("wcwidth"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "jinja2",
    deps = [
        requirement("jinja2"),

        requirement("markupsafe"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "mlflow",
    deps = [
        requirement("mlflow"),

        # NOTE: We comment out some larger requirements to save space; we don't use their plugins
        # anyways.
        requirement("boto3"),
        requirement("botocore"),
        requirement("certifi"),
        requirement("chardet"),
        requirement("click"),
        requirement("configparser"),
        requirement("databricks-cli"),
        requirement("docutils"),
        requirement("flask"),
        requirement("futures"),
        requirement("gitdb2"),
        requirement("gitpython"),
        requirement("gunicorn"),
        requirement("idna"),
        requirement("itsdangerous"),
        requirement("jinja2"),
        requirement("jmespath"),
        requirement("markupsafe"),
        requirement("mleap"),
        #requirement("nose"),
        #requirement("nose_exclude"),
        #requirement("pandas"),
        requirement("protobuf"),
        requirement("python_dateutil"),
        requirement("pytz"),
        requirement("pyyaml"),
        requirement("querystring_parser"),
        requirement("requests"),
        requirement("s3transfer"),
        #requirement("scikit_learn"),
        requirement("simplejson"),
        requirement("six"),
        requirement("smmap2"),
        requirement("tabulate"),
        requirement("urllib3"),
        requirement("werkzeug"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "paramiko",
    deps = [
        requirement("paramiko"),

        requirement("asn1crypto"),
        requirement("bcrypt"),
        requirement("cffi"),
        requirement("cryptography"),
        requirement("enum34"),
        requirement("idna"),
        requirement("ipaddress"),
        requirement("pyasn1"),
        requirement("pycparser"),
        requirement("pynacl"),
        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "pdbpp",
    deps = [
        requirement("pdbpp"),

        requirement("fancycompleter"),
        requirement("pygments"),
        requirement("pyrepl"),
        requirement("wmctrl"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "protobuf",
    deps = [
        requirement("protobuf"),

        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "pympler",
    deps = [
        requirement("pympler"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "pytest",
    deps = [
        requirement("pytest"),

        requirement("attrs"),
        requirement("funcsigs"),
        requirement("pluggy"),
        requirement("py"),
        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)

py_library(
    name = "scp",
    deps = [
        requirement("scp"),

        requirement("asn1crypto"),
        requirement("bcrypt"),
        requirement("cffi"),
        requirement("cryptography"),
        requirement("enum34"),
        requirement("idna"),
        requirement("ipaddress"),
        requirement("paramiko"),
        requirement("pyasn1"),
        requirement("pycparser"),
        requirement("pynacl"),
        requirement("six"),
    ],
    visibility = ["//visibility:public"],
)
