from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

RELATIVE_DECKLISTS_DIR = "generated/decklists"


def load(deck_names):
    decks = {}
    for deck_name in deck_names:
        deck_path = os.path.join(RELATIVE_DECKLISTS_DIR, "{}.txt".format(deck_name))
        with open(deck_path) as f:
            decks[deck_name] = f.read().strip()
    return decks


def get_sanitized_name(deck):
    name = deck.split("\n", 1)[0][len("Name: "):]
    sanitized_name = "".join([c for c in name if c.isalnum()])
    return sanitized_name
