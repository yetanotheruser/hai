from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import random

import deck.decklists as decklists

DECKLIST_NAMES = [
    #"Aggro_Mage",
    "APM_Priest",
    #"Big_Druid",
    "Combo_Priest",
    #"Combo_Shudderwock_Shaman",
    #"Control_Mage",
    #"Control_Warlock",
    #"Deathrattle_Hunter",
    "Deathrattle_Rogue",
    "Even_Paladin",
    #"Even_Shaman",
    #"Even_Warlock",
    #"Kingsbane_Rogue",
    #"Malygos_Druid",
    #"Mecha'thun_Druid",
    "Mecha'thun_Priest",
    #"Mecha'thun_Warlock",
    #"Mecha'thun_Warrior",
    "Miracle_Rogue",
    #"Odd_Fatigue_Warrior",
    "Odd_Paladin",
    "Odd_Rogue",
    #"Odd_Warrior",
    #"Pocket_Mage",
    #"Pogo_Rogue",
    #"Quest_Rogue",
    #"Recruit_Warrior",
    "Resurrect_Priest",
    #"Secret_Hunter",
    #"Subject_9_Hunter",
    #"Taunt_Druid",
    #"Tempo_Shudderwock_Shaman",
    #"Togwaggle_Druid",
    #"Token_Druid",
    "Zoolock",
]
DECKS = decklists.load(DECKLIST_NAMES)


def create_get_decks_eval(
        deck,  # type: Optional[str]
        opponent_deck,  # type: Optional[str]
):
    deck1 = deck2 = DECKS["Odd_Paladin"]
    if deck != None:
        deck1 = DECKS[deck]
    if opponent_deck != None:
        deck2 = DECKS[opponent_deck]
    def get_decks():
        return deck1, deck2
    return get_decks


def create_get_decks_train(
        deck,  # type: Optional[str]
        opponent_deck,  # type: Optional[str]
        odd_paladin_only,  # type: bool
):
    if odd_paladin_only:
        def get_decks():
            return DECKS["Odd_Paladin"], DECKS["Odd_Paladin"]
    else:
        def get_decks():
            if deck != None:
                deck1 = DECKS[deck]
            else:
                deck1 = random.choice(DECKS.values())
            if opponent_deck != None:
                deck2 = DECKS[opponent_deck]
            else:
                deck2 = random.choice(DECKS.values())
            return deck1, deck2
    return get_decks
