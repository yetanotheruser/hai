from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import Queue
import random


class ShuffleQueue(Queue.Queue):
    def shuffle(self):
        with self.mutex:
            random.shuffle(self.queue)
