from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import flags

FLAGS = flags.FLAGS
EXCLUDED_FLAGS = set([
    "nohelp",
    "nohelpfull",
    "nohelpshort",
    "nohelpxml",
])


def get_flags_string():
    flags = FLAGS.flags_into_string().split("\n")
    sanitized_flags = set()
    for f in flags:
        if f and f.lstrip("-") not in EXCLUDED_FLAGS:
            sanitized_flags.add(f)
    return "\n".join(sanitized_flags)
