from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from absl import app as absl_app
from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string("out_file", "", "File path to generate buildinfo into.")


def main(argv):
    del argv

    with open(FLAGS.out_file, "w") as of:
        status_files = ["bazel-out/stable-status.txt", "bazel-out/volatile-status.txt"]
        for status_file in status_files:
            with open(status_file) as f:
                for l in f:
                    k_maybe_v = l.strip().split(" ", 1)
                    if len(k_maybe_v) > 1:
                        k, v = k_maybe_v
                        of.write("{}={}\n".format(k, repr(v)))


if __name__ == "__main__":
    absl_app.run(main)
