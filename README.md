A (WIP) Hearthstone AI.

**NOTE**: This project is currently released as a companion to a [series of blog posts](https://www.yetanotherweb.site) about practical experience writing a Hearthstone AI. **To actually run it requires several disgusting patches to Spellsource (that have not been upstreamed at this time)**.

## TLDR: How do I X

First get [set up](#setup).

### Run the tests

```bash
mkvirtualenv hai
pip install -r python_deps/requirements.txt.lock
pip install "tensorflow==1.11.0" "numpy==1.15.2" "scipy==1.1.0"
bazel test //...
```

### Train against Spellsource GSVB

This assumes you have a Spellsource instance running locally with the required gRPC interface.

```bash
export T=ai:main_image
bazel run //$T -- --norun
docker run -v $HOME/tmp/output_dir:/tmp/output_dir -it --rm --net=host bazel/$T --train --deck=Odd_Paladin --opponent_deck=Odd_Paladin
```


### General

* You can generate the protobufs manually for examination / non-Bazel tooling using (you'll have to install `protoc` here or reach into Bazel to get it):
```bash
$ protoc --python_out=plugins=grpc:. --go_out=plugins=grpc:. -I. simulator/proto/*.proto
```

### Protos

To update the Spellsource protos copy them into `//spellsource/proto` and then run `script/rewrite_spellsource_proto_imports.sh`.

### Tensorboard

Tensorboard will help you visualize running (and completed) experiments' progress. It's easy to run as a Docker container:
```bash
docker run --net host -v $HOME/tmp/log_dir:/log_dir -it tensorflow_base tensorboard --logdir /log_dir
```

## Setup

### Bazel

* Install Bazel version 0.18.1. This repo may work with other versions but let's be real probably not (Bazel is not known for backwards compatability).
* If you are unfamiliar with Bazel you may want to create a `~/.bazelrc` with contents:
```
build --jobs 10 --verbose_failures
test --test_verbose_timeout_warnings --test_output=errors
```

### Docker

* First install [Nvidia Drivers](nvidia-drivers).
* Install docker.
* Install `nvidia-docker2`.
* You should make the `nvidia` runtime the default by adding `"default-runtime": "nvidia",` to `/etc/docker/daemon.json` and restarting the Docker service.
* Build the base Tensorflow image manually once:
```bash
docker build -t tensorflow_base -f ai/Dockerfile.dev ./
docker save tensorflow_base -o tensorflow_base.tar
```

### Nvidia Drivers

* This is notorious for messing up your computer, and depending on OS the best way to install can be very different.
* On Ubuntu 18.04 I had tons of success with:
```
sudo apt-add-repository ppa:graphics-drivers/ppa
sudo apt update
sudo apt install nvidia-390
```
* Note that we currently best support running everything inside of
  nvidia-docker, so there's no need to install more than the Nvidia drivers (no
  need for CUDA / CUDnn in particular).


### Python

* This repo uses only Python2, because Bazel has [very poor support for Python3](https://github.com/bazelbuild/bazel/issues/1580).


### Spellsource

We use the [Spellsource](https://github.com/hiddenswitch/Spellsource-Server) simulator.

The contents of the `//spellsource/proto` are directly copied from (a disgustingly patched) Spellsource. DO NOT EDIT THEM IN THIS REPO.

It's outside the scope of this readme to explain how to use Spellsource; please see its documentation.
