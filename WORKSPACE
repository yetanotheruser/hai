load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
# NOTE: At this time the native `http_archive` does not support `patch_cmds` (which
# we require to make `rules_docker` and Python protobufs play together), so we import the one from
# `bazel_tools`.
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

####################################################################################################
# Set up Python rules & support for PIP dependencies.
####################################################################################################
git_repository(
    name = "io_bazel_rules_python",
    # LICENSE: Apache 2.0
    remote = "https://github.com/bazelbuild/rules_python.git",
    # Tip of `master` circa 2018-05-12.
    commit = "8b5d0683a7d878b28fffe464779c8a53659fc645",
)

# For PIP support:
load("@io_bazel_rules_python//python:pip.bzl", "pip_repositories")
pip_repositories()

load("@io_bazel_rules_python//python:pip.bzl", "pip_import")
# This rule translates the specified requirements.txt into
# @py_deps//:requirements.bzl, which itself exposes a pip_install method.
pip_import(
   name = "py_deps",
   requirements = "//python_deps:requirements.txt.lock",
)
# Load the pip_install symbol for my_deps, and create the dependencies'
# repositories.
load("@py_deps//:requirements.bzl", "pip_install")
pip_install()


####################################################################################################
# Set up Docker rules.
####################################################################################################

# NOTE: This disgusting `sed` patching is to allow use of both Python protobufs and rules_docker.
# NOTE: depending on your version of sed, you need either `-i ''` or just `-i`.
SED_CMD = "sed -i " + " ".join(["-e '%s'" % e for e in [
    's~name = "six"~name = "six_hacked"~',
    's~"@six//:six"~"@six_hacked//:six_hacked"~',
    's~\"@six\"~\"@six_hacked\"~',
    's~if "six" not in excludes~if "six_hacked" not in excludes~',]])
http_archive(
    name = "containerregistry",
    # LICENSE: Apache 2.0
    urls = ["https://github.com/google/containerregistry/archive/v0.0.32.tar.gz"],
    sha256 = "48408e0d1861c47aac88d06efda089c46bfb3265bf3a51081853963460fbcb49",
    strip_prefix = "containerregistry-0.0.32",
    patch_cmds = [SED_CMD + " def.bzl BUILD.bazel"],
)
git_repository(
    name = "io_bazel_rules_docker",
    # LICENSE: Apache 2.0
    #urls = ["https://github.com/bazelbuild/rules_docker/archive/v0.5.1.tar.gz"],
    #sha256 = "29d109605e0d6f9c892584f07275b8c9260803bf0c6fcb7de2623b2bedc910bd",
    #strip_prefix = "rules_docker-0.5.1",

    # `master` circa 2018-11-16. Updated to fix `container_push` to be usable from within another
    # rule. When
    # https://github.com/bazelbuild/rules_docker/commit/7f61e98d7df54be9b2e0e4d46ec6ddf7f1b3ac0d is
    # released we should go back to using the tarball of an official release.
    remote = "https://github.com/bazelbuild/rules_docker/",
    commit = "9527234ef0b5a57bce93be524cb56d7ab1a85ea3",
    patch_cmds = [SED_CMD+ " container/container.bzl"],
)

# Python.
load("@io_bazel_rules_docker//python:image.bzl", py_image_repos = "repositories")
py_image_repos()

# Set up Tensorflow baseimage.
load(
    "@io_bazel_rules_docker//container:container.bzl",
    "container_load",
    "container_pull",
    container_repositories = "repositories",
)
# This is NOT needed when going through the language lang_image
# "repositories" function(s).
container_repositories()

# Load custom built Tensorflow image (see README for how to build).
container_load(
    name = "tensorflow_base",
    file = "//:tensorflow_base.tar",
)

# Pull
container_pull(
    name = "cc_debug_image_base",
    registry = "gcr.io",
    repository = "distroless/cc",
    # "gcr.io/distroless/cc:debug" circa 2017-12-09 23:07 +0000. Easiest to take this from
    # https://github.com/bazelbuild/rules_docker/blob/master/cc/cc.bzl
    digest = "sha256:d3fe07be221200e6a7c9981545da4394cd6177ab80012da65a858069bd374bb9",
)


####################################################################################################
# Import protobuf rules and toolchain.
####################################################################################################
# NOTE: This `six` archive is purely for use by google protobuf. Everyone else should use
# `requirement("six")`.
new_http_archive(
    name = "six_archive",
    build_file = "@com_google_protobuf//:six.BUILD",
    sha256 = "105f8d68616f8248e24bf0e9372ef04d3cc10104f1980f54d57b2ce73a5ad56a",
    url = "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz#md5=34eed507548117b2ab523ab14b2f8b55",
)
bind(
    name = "six",
    actual = "@six_archive//:six",
)

# NOTE: We must also `bind` the GRPC to `//external:grpc_python_plugin` because `protobuf` expects
# it to be there. If this ever changes we may be able to improve this. See
# https://github.com/protocolbuffers/protobuf/blob/master/protobuf.bzl#L404-L409
bind(
    name = "grpc_python_plugin",
    actual = "@com_google_grpc//:grpc_python_plugin",
)

git_repository(
    name = "org_pubref_rules_protobuf",
    # LICENSE: Apache 2.0
    # NOTE: The upstream provides v3.5.1. We need v3.6.1 because otherwise
    # the `py_proto_library` does not include GRPC generated files in its outputs. If
    # https://github.com/pubref/rules_protobuf ever updates to v3.6.1 then we can stop using a fork.
    remote = "https://gitlab.com/yetanotheruser/rules_protobuf.git",
    commit = "be14c5cf56116996bf526ae6a2d00fcc6e91a11b",
)

# Load language-specific dependencies
load("@org_pubref_rules_protobuf//python:rules.bzl", "py_proto_repositories")
py_proto_repositories()


# vim: set filetype=python:
