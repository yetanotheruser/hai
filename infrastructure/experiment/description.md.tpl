# SHAs

HAI SCM Revision: {{ hai_git_sha }}
HAI Docker Digest: {{ hai_digest }}
Spellsource SCM Revision: {{ spellsource_git_sha }}
Spellsource Docker Digest: {{ spellsource_digest }}

# Params

{{ train_args }}
