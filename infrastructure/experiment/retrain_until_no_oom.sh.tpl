#!/usr/bin/env bash
set -eou pipefail
IFS=$'\n\t'

set +e
false

while [[ $? != 0 ]]
do
  docker run \
    --rm \
    --net=host \
    -v `pwd`/output_dir:`pwd`/output_dir \
    --memory=10g \
    -it \
    "{{ hai_image }}" \
    --output_dir=`pwd`/output_dir \
    "$@"
done
set -e
