#!/usr/bin/env bash
set -eou pipefail
IFS=$'\n\t'

mkdir -p output_dir

tmux new-session -d -s "{{ tmux_session }}" -c "{{ experiment_dir }}" -n spellsource bash
tmux send-keys -t "spellsource" "docker run -it --memory=5g --net=host {{ spellsource_image }}" Enter

tmux new-window -c "{{ experiment_dir }}" -n train bash
tmux send-keys -t "train" "./retrain_until_no_oom.sh --mlflow_experiment={{ experiment_name }} --mlflow_run={{ experiment_uuid }} --mlflow_tracking_uri="http://mlflow.local" {{ train_args }}" Enter

tmux new-window -c "{{ experiment_dir }}" -n tensorboard bash
tmux send-keys -t "tensorboard" "docker run -it --memory=500m --net=host -v {{ experiment_dir }}/output_dir:/logdir tensorflow/tensorflow:latest-gpu tensorboard --logdir /logdir" Enter
